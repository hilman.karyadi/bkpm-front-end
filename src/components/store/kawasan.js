export default {
    "KawasanBlok": [
        {
        "id": 13,
        "name": "Delma Industrial Park",
        "details": [
            {
            "label": "Alamat",
            "value": "The Energy 17th Floor SCBD Lot 11 A \r\nJl. Jenderal Sudirman Kav. 52-53"
            },
            {
            "label": "Kabupaten / Provinsi",
            "value": "Jakarta Selatan,DKI Jakarta"
            },
            {
            "label": "Luas",
            "value": "400  HA"
            },
            {
            "label": "Website",
            "value": ""
            },
            {
            "label": "Bandara terdekat",
            "value": null
            },
            {
            "label": "Pelabuhan terdekat",
            "value": null
            },
            {
            "label": "Longitude",
            "value": "107.16861897"
            },
            {
            "label": "Latitude",
            "value": "-6.39322521499997"
            }
        ]
        },
        {
        "id": 12,
        "name": "Cilandak Commercial Estate",
        "details": [
            {
            "label": "Alamat",
            "value": "Jl. Raya Cilandak KKO - Jakarta Selatan"
            },
            {
            "label": "Kabupaten / Provinsi",
            "value": "Jakarta Selatan,DKI Jakarta"
            },
            {
            "label": "Luas",
            "value": "11.1999998092651  HA"
            },
            {
            "label": "Website",
            "value": "-"
            },
            {
            "label": "Bandara terdekat",
            "value": null
            },
            {
            "label": "Pelabuhan terdekat",
            "value": null
            },
            {
            "label": "Longitude",
            "value": "107.27748815"
            },
            {
            "label": "Latitude",
            "value": "-6.37118500000003"
            }
        ]
        },
        {
        "id": 3,
        "name": "Padang Industrial Park",
        "details": [
            {
            "label": "Alamat",
            "value": "Jl. H. Agus Salim No.17, Sawahan Tim., Padang Tim., Kota Padang, Sumatera Barat"
            },
            {
            "label": "Kabupaten / Provinsi",
            "value": "Padang,Sumatera Barat"
            },
            {
            "label": "Luas",
            "value": "616  HA"
            },
            {
            "label": "Website",
            "value": "-"
            },
            {
            "label": "Bandara terdekat",
            "value": null
            },
            {
            "label": "Pelabuhan terdekat",
            "value": null
            },
            {
            "label": "Longitude",
            "value": "119.869392215"
            },
            {
            "label": "Latitude",
            "value": "-0.711664734999999"
            }
        ]
        },
        {
        "id": 51,
        "name": "Krakatau Industrial Estate - Cilegon",
        "details": [
            {
            "label": "Alamat",
            "value": "Jl. KH. Yasin Beji, No. 6, Kec. Cilegon, Banten"
            },
            {
            "label": "Kabupaten / Provinsi",
            "value": "Cilegon,Banten"
            },
            {
            "label": "Luas",
            "value": "705  HA"
            },
            {
            "label": "Website",
            "value": "www.kiec.co.id"
            },
            {
            "label": "Bandara terdekat",
            "value": null
            },
            {
            "label": "Pelabuhan terdekat",
            "value": null
            },
            {
            "label": "Longitude",
            "value": "106.00501914292"
            },
            {
            "label": "Latitude",
            "value": "-5.97837938300131"
            }
        ]
        },
        {
        "id": 50,
        "name": "Jababeka Industrial Estate - Cilegon",
        "details": [
            {
            "label": "Alamat",
            "value": "JL. Raya Bojonegara Kav 162, Cilegon, 42454, Terate, Kramatwatu, Serang, Banten"
            },
            {
            "label": "Kabupaten / Provinsi",
            "value": "Cilegon,Banten"
            },
            {
            "label": "Luas",
            "value": "1000  HA"
            },
            {
            "label": "Website",
            "value": "www.jababeka.com"
            },
            {
            "label": "Bandara terdekat",
            "value": null
            },
            {
            "label": "Pelabuhan terdekat",
            "value": null
            },
            {
            "label": "Longitude",
            "value": "106.00501914292"
            },
            {
            "label": "Latitude",
            "value": "-5.97837938300131"
            }
        ]
        },
        {
        "id": 53,
        "name": "Kawasan Industri Makassar",
        "details": [
            {
            "label": "Alamat",
            "value": "Jl. Perintis Kemerdekaan KM. 15, Daya, Biring Kanaya, Kec. Makassar, Sulawesi Selatan"
            },
            {
            "label": "Kabupaten / Provinsi",
            "value": "Makassar,Sulawesi Selatan"
            },
            {
            "label": "Luas",
            "value": "703  HA"
            },
            {
            "label": "Website",
            "value": "-"
            },
            {
            "label": "Bandara terdekat",
            "value": null
            },
            {
            "label": "Pelabuhan terdekat",
            "value": null
            },
            {
            "label": "Longitude",
            "value": "119.402643207119"
            },
            {
            "label": "Latitude",
            "value": "-5.11949659559059"
            }
        ]
        },
        {
        "id": 17,
        "name": "Kawasan Berikat Nusantara",
        "details": [
            {
            "label": "Alamat",
            "value": "Jl. Raya Cakung Cilincing No.1, Tg. Priok, Daerah Khusus Ibukota Jakarta"
            },
            {
            "label": "Kabupaten / Provinsi",
            "value": "Jakarta Utara,DKI Jakarta"
            },
            {
            "label": "Luas",
            "value": "800  HA"
            },
            {
            "label": "Website",
            "value": "www.kbn.co.id"
            },
            {
            "label": "Bandara terdekat",
            "value": null
            },
            {
            "label": "Pelabuhan terdekat",
            "value": null
            },
            {
            "label": "Longitude",
            "value": "106.905094322646"
            },
            {
            "label": "Latitude",
            "value": "-6.26111907208673"
            }
        ]
        },
        {
        "id": 86,
        "name": "Kawasan Industri Jelitik",
        "details": [
            {
            "label": "Alamat",
            "value": "Dusun Jelitik, Kelurahan Sungailiat,Kecamatan Sungailiat Kabupaten Bangka"
            },
            {
            "label": "Kabupaten / Provinsi",
            "value": "Bangka,Bangka-Belitung"
            },
            {
            "label": "Luas",
            "value": "9000  HA"
            },
            {
            "label": "Website",
            "value": ""
            },
            {
            "label": "Bandara terdekat",
            "value": null
            },
            {
            "label": "Pelabuhan terdekat",
            "value": null
            },
            {
            "label": "Longitude",
            "value": "105.941647824089"
            },
            {
            "label": "Latitude",
            "value": "-1.93599876852755"
            }
        ]
        },
        {
        "id": 85,
        "name": "Kawasan Industri Sadai",
        "details": [
            {
            "label": "Alamat",
            "value": "Kecamatan Tukak Sadai Kabupaten Bangka Selatan"
            },
            {
            "label": "Kabupaten / Provinsi",
            "value": "Bangka Selatan,Bangka-Belitung"
            },
            {
            "label": "Luas",
            "value": "3086  HA"
            },
            {
            "label": "Website",
            "value": ""
            },
            {
            "label": "Bandara terdekat",
            "value": null
            },
            {
            "label": "Pelabuhan terdekat",
            "value": null
            },
            {
            "label": "Longitude",
            "value": "105.446940103304"
            },
            {
            "label": "Latitude",
            "value": "-1.84019763681067"
            }
        ]
        },
        {
        "id": 89,
        "name": "Kawasan Industri Tanjung Berikat",
        "details": [
            {
            "label": "Alamat",
            "value": "Kabupaten Bangka Tengah"
            },
            {
            "label": "Kabupaten / Provinsi",
            "value": "Bangka Tengah,Bangka-Belitung"
            },
            {
            "label": "Luas",
            "value": "7311  HA"
            },
            {
            "label": "Website",
            "value": ""
            },
            {
            "label": "Bandara terdekat",
            "value": null
            },
            {
            "label": "Pelabuhan terdekat",
            "value": null
            },
            {
            "label": "Longitude",
            "value": "106.29928976661"
            },
            {
            "label": "Latitude",
            "value": "-2.43284810875241"
            }
        ]
        },
        {
        "id": 14,
        "name": "Jakarta Industrial Estate - Pulogadung",
        "details": [
            {
            "label": "Alamat",
            "value": "Jl. Pulo Gadung SFB-1%2F3 Pulo Gadung Pulo Gadung Jakarta Timur DKI Jakarta, Pulo Gadung, Kota Jakarta Timur, Daerah Khusus Ibukota Jakarta"
            },
            {
            "label": "Kabupaten / Provinsi",
            "value": "Jakarta Timur,DKI Jakarta"
            },
            {
            "label": "Luas",
            "value": "500  HA"
            },
            {
            "label": "Website",
            "value": "www.jiep.co.id"
            },
            {
            "label": "Bandara terdekat",
            "value": null
            },
            {
            "label": "Pelabuhan terdekat",
            "value": null
            },
            {
            "label": "Longitude",
            "value": "107.34363707"
            },
            {
            "label": "Latitude",
            "value": "-6.40283640000001"
            }
        ]
        },
        {
        "id": 87,
        "name": "Kawasan Industri Mentok",
        "details": [
            {
            "label": "Alamat",
            "value": "Desa Air Putih, Kecamtan Muntok Kabupaten Bangka Barat"
            },
            {
            "label": "Kabupaten / Provinsi",
            "value": "Bangka Barat,Bangka-Belitung"
            },
            {
            "label": "Luas",
            "value": "1414  HA"
            },
            {
            "label": "Website",
            "value": ""
            },
            {
            "label": "Bandara terdekat",
            "value": null
            },
            {
            "label": "Pelabuhan terdekat",
            "value": null
            },
            {
            "label": "Longitude",
            "value": "106.49063954538"
            },
            {
            "label": "Latitude",
            "value": "-2.78609681991151"
            }
        ]
        },
        {
        "id": 11,
        "name": "Union Industrial Park",
        "details": [
            {
            "label": "Alamat",
            "value": "JL Yos Sudarso, Batu Ampar, Blok AA No.7- 8 Union Industrial Park, Bengkong Laut, Bengkong, Kota Batam, Kepulauan Riau"
            },
            {
            "label": "Kabupaten / Provinsi",
            "value": "Batam,Kepulauan Riau"
            },
            {
            "label": "Luas",
            "value": "2.29999995231628  HA"
            },
            {
            "label": "Website",
            "value": "-"
            },
            {
            "label": "Bandara terdekat",
            "value": null
            },
            {
            "label": "Pelabuhan terdekat",
            "value": null
            },
            {
            "label": "Longitude",
            "value": "107.18584879"
            },
            {
            "label": "Latitude",
            "value": "-6.39121514999999"
            }
        ]
        },
        {
        "id": 10,
        "name": "Tunas Industrial Estate",
        "details": [
            {
            "label": "Alamat",
            "value": "Nagoya, Komplek Bumi Indah Blok III No. 15-17, Lubuk Baja Kota, Lubuk Baja, Kota Batam, Kepulauan Riau"
            },
            {
            "label": "Kabupaten / Provinsi",
            "value": "Batam,Kepulauan Riau"
            },
            {
            "label": "Luas",
            "value": "64  HA"
            },
            {
            "label": "Website",
            "value": "www.tritunas.com"
            },
            {
            "label": "Bandara terdekat",
            "value": null
            },
            {
            "label": "Pelabuhan terdekat",
            "value": null
            },
            {
            "label": "Longitude",
            "value": "107.14774191"
            },
            {
            "label": "Latitude",
            "value": "-6.35772385500002"
            }
        ]
        },
        {
        "id": 9,
        "name": "Puri Industrial Park 2000",
        "details": [
            {
            "label": "Alamat",
            "value": "Baloi Permai, Batam City, Riau Islands"
            },
            {
            "label": "Kabupaten / Provinsi",
            "value": "Batam,Kepulauan Riau"
            },
            {
            "label": "Luas",
            "value": "24  HA"
            },
            {
            "label": "Website",
            "value": "www.tokoutama.com"
            },
            {
            "label": "Bandara terdekat",
            "value": null
            },
            {
            "label": "Pelabuhan terdekat",
            "value": null
            },
            {
            "label": "Longitude",
            "value": "104.25938567"
            },
            {
            "label": "Latitude",
            "value": "1.00897936500002"
            }
        ]
        },
        {
        "id": 8,
        "name": "Panbil Industrial Estate",
        "details": [
            {
            "label": "Alamat",
            "value": "Panbil Plaza, Jalan Ahmad Yani Muka Kuning, Kepulauan Riau"
            },
            {
            "label": "Kabupaten / Provinsi",
            "value": "Batam,Kepulauan Riau"
            },
            {
            "label": "Luas",
            "value": "200  HA"
            },
            {
            "label": "Website",
            "value": "www.panbil.co.id"
            },
            {
            "label": "Bandara terdekat",
            "value": null
            },
            {
            "label": "Pelabuhan terdekat",
            "value": null
            },
            {
            "label": "Longitude",
            "value": "104.028182435"
            },
            {
            "label": "Latitude",
            "value": "1.05899091499998"
            }
        ]
        },
        {
        "id": 7,
        "name": "Kabil Integrated Industrial Estate",
        "details": [
            {
            "label": "Alamat",
            "value": "Integrated Industrial Estate, Kabil, Nongsa, Kota Batam, Kepulauan Riau"
            },
            {
            "label": "Kabupaten / Provinsi",
            "value": "Batam,Kepulauan Riau"
            },
            {
            "label": "Luas",
            "value": "520  HA"
            },
            {
            "label": "Website",
            "value": "-"
            },
            {
            "label": "Bandara terdekat",
            "value": null
            },
            {
            "label": "Pelabuhan terdekat",
            "value": null
            },
            {
            "label": "Longitude",
            "value": "106.325223665"
            },
            {
            "label": "Latitude",
            "value": "-6.190590165"
            }
        ]
        },
        {
        "id": 6,
        "name": "Bintang Industrial Park",
        "details": [
            {
            "label": "Alamat",
            "value": "Jl. Majapahit Kav. II Batu Ampar, Batam"
            },
            {
            "label": "Kabupaten / Provinsi",
            "value": "Batam,Kepulauan Riau"
            },
            {
            "label": "Luas",
            "value": "70  HA"
            },
            {
            "label": "Website",
            "value": "www.bjsgroup.com"
            },
            {
            "label": "Bandara terdekat",
            "value": null
            },
            {
            "label": "Pelabuhan terdekat",
            "value": null
            },
            {
            "label": "Longitude",
            "value": "106.49884334"
            },
            {
            "label": "Latitude",
            "value": "-6.25739747"
            }
        ]
        },
        {
        "id": 5,
        "name": "Batamindo Industrial Park",
        "details": [
            {
            "label": "Alamat",
            "value": "Wisma Batamindo\nJl. Rasamala No. 01 Mukakuning, Batam 29433"
            },
            {
            "label": "Kabupaten / Provinsi",
            "value": "Batam,Kepulauan Riau"
            },
            {
            "label": "Luas",
            "value": "500  HA"
            },
            {
            "label": "Website",
            "value": "www.batamindoindustrial.com"
            },
            {
            "label": "Bandara terdekat",
            "value": null
            },
            {
            "label": "Pelabuhan terdekat",
            "value": null
            },
            {
            "label": "Longitude",
            "value": "110.26635613"
            },
            {
            "label": "Latitude",
            "value": "-6.92699238500001"
            }
        ]
        },
        {
        "id": 16,
        "name": "Podomoro Industrial Park",
        "details": [
            {
            "label": "Alamat",
            "value": "Central Park Mall, Lt.3 - Unit 112, Jalan Letjen. S. Parman No.28, Tanjung Duren Selatan, Grogol petamburan, Central Park Podomoro City, Daerah Khusus Ibukota Jakarta"
            },
            {
            "label": "Kabupaten / Provinsi",
            "value": "Jakarta Barat,DKI Jakarta"
            },
            {
            "label": "Luas",
            "value": "542  HA"
            },
            {
            "label": "Website",
            "value": "www.podomoroindustrialpark.com"
            },
            {
            "label": "Bandara terdekat",
            "value": null
            },
            {
            "label": "Pelabuhan terdekat",
            "value": null
            },
            {
            "label": "Longitude",
            "value": "106.802091270697"
            },
            {
            "label": "Latitude",
            "value": "-6.28409817578938"
            }
        ]
        },
        {
        "id": 18,
        "name": "Cibinong Center Industrial Estate",
        "details": [
            {
            "label": "Alamat",
            "value": "Kp. Pasir Tangkil, Rt. 13/Rw. 005, Ds. Bantarjati, Kec. Klapanunggal, Kab. Bogor 16820"
            },
            {
            "label": "Kabupaten / Provinsi",
            "value": "Bogor,Jawa Barat"
            },
            {
            "label": "Luas",
            "value": "140  HA"
            },
            {
            "label": "Website",
            "value": "-"
            },
            {
            "label": "Bandara terdekat",
            "value": null
            },
            {
            "label": "Pelabuhan terdekat",
            "value": null
            },
            {
            "label": "Longitude",
            "value": "106.814124962298"
            },
            {
            "label": "Latitude",
            "value": "-6.54524622713086"
            }
        ]
        },
        {
        "id": 2,
        "name": "Kawasan Industrial Medan",
        "details": [
            {
            "label": "Alamat",
            "value": "Wisma Kawasan Industri Medan, Jalan Pulau Batam No.1 Kompleks KIM Tahap 2, Medan, Sumatera Utara"
            },
            {
            "label": "Kabupaten / Provinsi",
            "value": "Medan,Sumatera Utara"
            },
            {
            "label": "Luas",
            "value": "960  HA"
            },
            {
            "label": "Website",
            "value": "www.kim.co.id"
            },
            {
            "label": "Bandara terdekat",
            "value": null
            },
            {
            "label": "Pelabuhan terdekat",
            "value": null
            },
            {
            "label": "Longitude",
            "value": "112.61936214"
            },
            {
            "label": "Latitude",
            "value": "-7.07139195000002"
            }
        ]
        },
        {
        "id": 33,
        "name": "MM2100 Industrial Town",
        "details": [
            {
            "label": "Alamat",
            "value": "MM2100 Industrial Town, Jl. Sumatera Blok C No.1, Cikarang Barat, Gandasari, Bekasi, Jawa Barat"
            },
            {
            "label": "Kabupaten / Provinsi",
            "value": "Bekasi,Jawa Barat"
            },
            {
            "label": "Luas",
            "value": "300  HA"
            },
            {
            "label": "Website",
            "value": "mm2100.co.id"
            },
            {
            "label": "Bandara terdekat",
            "value": null
            },
            {
            "label": "Pelabuhan terdekat",
            "value": null
            },
            {
            "label": "Longitude",
            "value": "106.970363782963"
            },
            {
            "label": "Latitude",
            "value": "-6.28473104220302"
            }
        ]
        },
        {
        "id": 32,
        "name": "Marunda Center",
        "details": [
            {
            "label": "Alamat",
            "value": "Kawasan Industri & Pergudangan Marunda Center Blok G No. 1, Jl. Marunda Makmur, Bekasi, Jawa Barat"
            },
            {
            "label": "Kabupaten / Provinsi",
            "value": "Bekasi,Jawa Barat"
            },
            {
            "label": "Luas",
            "value": "540  HA"
            },
            {
            "label": "Website",
            "value": "www.marundacenter.com"
            },
            {
            "label": "Bandara terdekat",
            "value": null
            },
            {
            "label": "Pelabuhan terdekat",
            "value": null
            },
            {
            "label": "Longitude",
            "value": "106.970363782963"
            },
            {
            "label": "Latitude",
            "value": "-6.28473104220302"
            }
        ]
        },
        {
        "id": 39,
        "name": "Taman Industri BSB",
        "details": [
            {
            "label": "Alamat",
            "value": "Jl. Raya Semarang-Boja, Jatibarang, Mijen, Kota Semarang, Jawa Tengah"
            },
            {
            "label": "Kabupaten / Provinsi",
            "value": "Semarang,Jawa Tengah"
            },
            {
            "label": "Luas",
            "value": "120  HA"
            },
            {
            "label": "Website",
            "value": "-"
            },
            {
            "label": "Bandara terdekat",
            "value": null
            },
            {
            "label": "Pelabuhan terdekat",
            "value": null
            },
            {
            "label": "Longitude",
            "value": "110.387249115056"
            },
            {
            "label": "Latitude",
            "value": "-7.0230518321755"
            }
        ]
        },
        {
        "id": 38,
        "name": "Candi Industrial Estate",
        "details": [
            {
            "label": "Alamat",
            "value": "Jl. Marina No. 3 Semarang 50144"
            },
            {
            "label": "Kabupaten / Provinsi",
            "value": "Semarang,Jawa Tengah"
            },
            {
            "label": "Luas",
            "value": "500  HA"
            },
            {
            "label": "Website",
            "value": "-"
            },
            {
            "label": "Bandara terdekat",
            "value": null
            },
            {
            "label": "Pelabuhan terdekat",
            "value": null
            },
            {
            "label": "Longitude",
            "value": "110.387249115056"
            },
            {
            "label": "Latitude",
            "value": "-7.0230518321755"
            }
        ]
        },
        {
        "id": 47,
        "name": "Tanjung Emas Export Processing Zone",
        "details": [
            {
            "label": "Alamat",
            "value": "Jembatan Merah Plasa, Jl. Taman Jayengrono, Krembangan Sel., Krembangan, Kota SBY, Jawa Timur"
            },
            {
            "label": "Kabupaten / Provinsi",
            "value": "Surabaya,Jawa Timur"
            },
            {
            "label": "Luas",
            "value": "101  HA"
            },
            {
            "label": "Website",
            "value": "-"
            },
            {
            "label": "Bandara terdekat",
            "value": null
            },
            {
            "label": "Pelabuhan terdekat",
            "value": null
            },
            {
            "label": "Longitude",
            "value": "112.719272115017"
            },
            {
            "label": "Latitude",
            "value": "-7.27455289909619"
            }
        ]
        },
        {
        "id": 46,
        "name": "Surabaya Industrial Estate Rungkut",
        "details": [
            {
            "label": "Alamat",
            "value": "JL. Rungkut Industri Raya, No. 10, 60401"
            },
            {
            "label": "Kabupaten / Provinsi",
            "value": "Surabaya,Jawa Timur"
            },
            {
            "label": "Luas",
            "value": "332  HA"
            },
            {
            "label": "Website",
            "value": "www.sier-pier.com"
            },
            {
            "label": "Bandara terdekat",
            "value": null
            },
            {
            "label": "Pelabuhan terdekat",
            "value": null
            },
            {
            "label": "Longitude",
            "value": "112.719272115017"
            },
            {
            "label": "Latitude",
            "value": "-7.27455289909619"
            }
        ]
        },
        {
        "id": 15,
        "name": "Milennium Industrial Estate",
        "details": [
            {
            "label": "Alamat",
            "value": "Jl. Millennium 4 Blok A25 No.2, Budi Mulya, Cikupa, Tangerang, Banten 15710, Indonesia"
            },
            {
            "label": "Kabupaten / Provinsi",
            "value": "Tangerang,Banten"
            },
            {
            "label": "Luas",
            "value": "1800  HA"
            },
            {
            "label": "Website",
            "value": "www.bumicitrapermai.com"
            },
            {
            "label": "Bandara terdekat",
            "value": null
            },
            {
            "label": "Pelabuhan terdekat",
            "value": null
            },
            {
            "label": "Longitude",
            "value": "107.437400305"
            },
            {
            "label": "Latitude",
            "value": "-6.43927358500002"
            }
        ]
        },
        {
        "id": 1,
        "name": "Medan Star Industrial Estate",
        "details": [
            {
            "label": "Alamat",
            "value": "Jl. Raya Medan-Lubuk Pakam, Tj. Morawa B, Tj. Morawa, Kabupaten Deli Serdang, Sumatera Utara"
            },
            {
            "label": "Kabupaten / Provinsi",
            "value": "Deliserdang,Sumatera Utara"
            },
            {
            "label": "Luas",
            "value": "103  HA"
            },
            {
            "label": "Website",
            "value": "-"
            },
            {
            "label": "Bandara terdekat",
            "value": null
            },
            {
            "label": "Pelabuhan terdekat",
            "value": null
            },
            {
            "label": "Longitude",
            "value": "110.551571235"
            },
            {
            "label": "Latitude",
            "value": "-6.92153883999998"
            }
        ]
        },
        {
        "id": 36,
        "name": "Kawasan Industri Terboyo Semarang",
        "details": [
            {
            "label": "Alamat",
            "value": "Jl. Kaligawe/Semarang-Demak, Sayung, Kabupaten Demak, Jawa Tengah"
            },
            {
            "label": "Kabupaten / Provinsi",
            "value": "Demak,Jawa Tengah"
            },
            {
            "label": "Luas",
            "value": "182  HA"
            },
            {
            "label": "Website",
            "value": ""
            },
            {
            "label": "Bandara terdekat",
            "value": null
            },
            {
            "label": "Pelabuhan terdekat",
            "value": null
            },
            {
            "label": "Longitude",
            "value": "110.651079549982"
            },
            {
            "label": "Latitude",
            "value": "-6.92289344917253"
            }
        ]
        },
        {
        "id": 37,
        "name": "Kawasan Industri Wijayakusuma",
        "details": [
            {
            "label": "Alamat",
            "value": "Jl. Raya Semarang - Kendal Km. 12, Mangkangwetan, Tugu, Jawa Tengah"
            },
            {
            "label": "Kabupaten / Provinsi",
            "value": "Kendal,Jawa Tengah"
            },
            {
            "label": "Luas",
            "value": "250  HA"
            },
            {
            "label": "Website",
            "value": "www.kiw.co.id"
            },
            {
            "label": "Bandara terdekat",
            "value": null
            },
            {
            "label": "Pelabuhan terdekat",
            "value": null
            },
            {
            "label": "Longitude",
            "value": "110.139401605101"
            },
            {
            "label": "Latitude",
            "value": "-7.02345338036838"
            }
        ]
        },
        {
        "id": 41,
        "name": "Ngoro Industrial Park",
        "details": [
            {
            "label": "Alamat",
            "value": "Jl. Raya Ngoro, Kec. Ngoro. Mojokerto - Indonesia"
            },
            {
            "label": "Kabupaten / Provinsi",
            "value": "Mojokerto,Jawa Timur"
            },
            {
            "label": "Luas",
            "value": "550  HA"
            },
            {
            "label": "Website",
            "value": "www.ngoroindustrialpark.com"
            },
            {
            "label": "Bandara terdekat",
            "value": null
            },
            {
            "label": "Pelabuhan terdekat",
            "value": null
            },
            {
            "label": "Longitude",
            "value": "112.437384324928"
            },
            {
            "label": "Latitude",
            "value": "-7.47071944674428"
            }
        ]
        },
        {
        "id": 81,
        "name": "GT Tech Park",
        "details": [
            {
            "label": "Alamat",
            "value": "Jl. Trans Heksa Karawang, Kabupaten Karawang"
            },
            {
            "label": "Kabupaten / Provinsi",
            "value": "Karawang,Jawa Barat"
            },
            {
            "label": "Luas",
            "value": "400  HA"
            },
            {
            "label": "Website",
            "value": ""
            },
            {
            "label": "Bandara terdekat",
            "value": null
            },
            {
            "label": "Pelabuhan terdekat",
            "value": null
            },
            {
            "label": "Longitude",
            "value": "107.363978345625"
            },
            {
            "label": "Latitude",
            "value": "-6.26348359928789"
            }
        ]
        },
        {
        "id": 26,
        "name": "Suryacipta City of Industry",
        "details": [
            {
            "label": "Alamat",
            "value": "Jl. Peruri,Sukatani, Kutamekar, Ciampel, Kabupaten Karawang, Jawa Barat"
            },
            {
            "label": "Kabupaten / Provinsi",
            "value": "Karawang,Jawa Barat"
            },
            {
            "label": "Luas",
            "value": "1400  HA"
            },
            {
            "label": "Website",
            "value": "www.suryacipta.com"
            },
            {
            "label": "Bandara terdekat",
            "value": null
            },
            {
            "label": "Pelabuhan terdekat",
            "value": null
            },
            {
            "label": "Longitude",
            "value": "107.363978345625"
            },
            {
            "label": "Latitude",
            "value": "-6.26348359928789"
            }
        ]
        },
        {
        "id": 25,
        "name": "Karawang International Industrial City",
        "details": [
            {
            "label": "Alamat",
            "value": "Margakaya, Telukjambe Barat, Karawang , Jawa Barat"
            },
            {
            "label": "Kabupaten / Provinsi",
            "value": "Karawang,Jawa Barat"
            },
            {
            "label": "Luas",
            "value": "1200  HA"
            },
            {
            "label": "Website",
            "value": "-"
            },
            {
            "label": "Bandara terdekat",
            "value": null
            },
            {
            "label": "Pelabuhan terdekat",
            "value": null
            },
            {
            "label": "Longitude",
            "value": "107.363978345625"
            },
            {
            "label": "Latitude",
            "value": "-6.26348359928789"
            }
        ]
        },
        {
        "id": 24,
        "name": "Kujang Industrial Estate",
        "details": [
            {
            "label": "Alamat",
            "value": "Jalan A. Yani No. 39, Cikampek Kota, Cikampek, Karawang, Jawa Barat"
            },
            {
            "label": "Kabupaten / Provinsi",
            "value": "Karawang,Jawa Barat"
            },
            {
            "label": "Luas",
            "value": "110  HA"
            },
            {
            "label": "Website",
            "value": "www.kikc.co.id"
            },
            {
            "label": "Bandara terdekat",
            "value": null
            },
            {
            "label": "Pelabuhan terdekat",
            "value": null
            },
            {
            "label": "Longitude",
            "value": "107.363978345625"
            },
            {
            "label": "Latitude",
            "value": "-6.26348359928789"
            }
        ]
        },
        {
        "id": 22,
        "name": "Kawasan Industri Mitra Karawang",
        "details": [
            {
            "label": "Alamat",
            "value": "Desa Parungmulya, Kecamatan Ciampel, Parungmulya, Ciampel, Kabupaten Karawang, Jawa Barat"
            },
            {
            "label": "Kabupaten / Provinsi",
            "value": "Karawang,Jawa Barat"
            },
            {
            "label": "Luas",
            "value": "500  HA"
            },
            {
            "label": "Website",
            "value": "-"
            },
            {
            "label": "Bandara terdekat",
            "value": null
            },
            {
            "label": "Pelabuhan terdekat",
            "value": null
            },
            {
            "label": "Longitude",
            "value": "107.363978345625"
            },
            {
            "label": "Latitude",
            "value": "-6.26348359928789"
            }
        ]
        },
        {
        "id": 90,
        "name": "name",
        "details": [
            {
            "label": "Alamat",
            "value": "Karawang"
            },
            {
            "label": "Kabupaten / Provinsi",
            "value": "Karawang,Jawa Barat"
            },
            {
            "label": "Luas",
            "value": "10  HA"
            },
            {
            "label": "Website",
            "value": "htt://eri2"
            },
            {
            "label": "Bandara terdekat",
            "value": "ALAS LAUSER update"
            },
            {
            "label": "Pelabuhan terdekat",
            "value": "Tapaktuan"
            },
            {
            "label": "Longitude",
            "value": "103.402055841364"
            },
            {
            "label": "Latitude",
            "value": "-0.908983888744258"
            }
        ]
        },
        {
        "id": 19,
        "name": "Kawasan Industri Rancaekek",
        "details": [
            {
            "label": "Alamat",
            "value": "JL. Raya Rancaekek, Km. 245, M-8, Cipacing, Rancaekek, Kabupaten Sumedang, Jawa Barat"
            },
            {
            "label": "Kabupaten / Provinsi",
            "value": "Sumedang,Jawa Barat"
            },
            {
            "label": "Luas",
            "value": "200  HA"
            },
            {
            "label": "Website",
            "value": "www.dwipapuri-abadi.com"
            },
            {
            "label": "Bandara terdekat",
            "value": null
            },
            {
            "label": "Pelabuhan terdekat",
            "value": null
            },
            {
            "label": "Longitude",
            "value": "107.980400168234"
            },
            {
            "label": "Latitude",
            "value": "-6.81017775959515"
            }
        ]
        },
        {
        "id": 82,
        "name": "Jababeka Industrial Estate",
        "details": [
            {
            "label": "Alamat",
            "value": "Kota Jababeka, Cikarang, Kabupaten Bekasi"
            },
            {
            "label": "Kabupaten / Provinsi",
            "value": "Bekasi,Jawa Barat"
            },
            {
            "label": "Luas",
            "value": "1840  HA"
            },
            {
            "label": "Website",
            "value": ""
            },
            {
            "label": "Bandara terdekat",
            "value": null
            },
            {
            "label": "Pelabuhan terdekat",
            "value": null
            },
            {
            "label": "Longitude",
            "value": "107.13269439713"
            },
            {
            "label": "Latitude",
            "value": "-6.19789470787367"
            }
        ]
        },
        {
        "id": 80,
        "name": "MM2100 Megalopolis Manunggal Industrial Development",
        "details": [
            {
            "label": "Alamat",
            "value": "Jl. Sumatera Blok C, Cikarang Barat, Gandasari, Cikarang Baru., Bekasi, Jawa Barat "
            },
            {
            "label": "Kabupaten / Provinsi",
            "value": "Bekasi,Jawa Barat"
            },
            {
            "label": "Luas",
            "value": "805  HA"
            },
            {
            "label": "Website",
            "value": ""
            },
            {
            "label": "Bandara terdekat",
            "value": null
            },
            {
            "label": "Pelabuhan terdekat",
            "value": null
            },
            {
            "label": "Longitude",
            "value": "107.13269439713"
            },
            {
            "label": "Latitude",
            "value": "-6.19789470787367"
            }
        ]
        },
        {
        "id": 77,
        "name": "Lippo Cikarang",
        "details": [
            {
            "label": "Alamat",
            "value": "Easton Commercial Centre, Jl. Gunung Panderman Kav. 05, Lippo Cikarang, Cibatu, Cikarang Sel., Bekasi, Jawa Barat"
            },
            {
            "label": "Kabupaten / Provinsi",
            "value": "Bekasi,Jawa Barat"
            },
            {
            "label": "Luas",
            "value": "1.22699999809265  HA"
            },
            {
            "label": "Website",
            "value": "www.lippo-cikarang.com"
            },
            {
            "label": "Bandara terdekat",
            "value": null
            },
            {
            "label": "Pelabuhan terdekat",
            "value": null
            },
            {
            "label": "Longitude",
            "value": "107.13269439713"
            },
            {
            "label": "Latitude",
            "value": "-6.19789470787367"
            }
        ]
        },
        {
        "id": 76,
        "name": "Kawasan Industri Terpadu Indonesia Cina",
        "details": [
            {
            "label": "Alamat",
            "value": "GIIC - Kota Deltamas, Jl. Kawasan Industri Terpadu Indonesia China, Kecamatan Cikarang Pusat, Jawa Barat"
            },
            {
            "label": "Kabupaten / Provinsi",
            "value": "Bekasi,Jawa Barat"
            },
            {
            "label": "Luas",
            "value": "200  HA"
            },
            {
            "label": "Website",
            "value": ""
            },
            {
            "label": "Bandara terdekat",
            "value": null
            },
            {
            "label": "Pelabuhan terdekat",
            "value": null
            },
            {
            "label": "Longitude",
            "value": "107.13269439713"
            },
            {
            "label": "Latitude",
            "value": "-6.19789470787367"
            }
        ]
        },
        {
        "id": 75,
        "name": "Kawasan Industri Gobel",
        "details": [
            {
            "label": "Alamat",
            "value": "Jl. Kw. Industri Gobel, Telaga Asih, Cikarang Bar., Bekasi, Jawa Barat 17530"
            },
            {
            "label": "Kabupaten / Provinsi",
            "value": "Bekasi,Jawa Barat"
            },
            {
            "label": "Luas",
            "value": "54  HA"
            },
            {
            "label": "Website",
            "value": ""
            },
            {
            "label": "Bandara terdekat",
            "value": null
            },
            {
            "label": "Pelabuhan terdekat",
            "value": null
            },
            {
            "label": "Longitude",
            "value": "107.13269439713"
            },
            {
            "label": "Latitude",
            "value": "-6.19789470787367"
            }
        ]
        },
        {
        "id": 74,
        "name": "Jakarta Timur Industrial Park",
        "details": [
            {
            "label": "Alamat",
            "value": "EJIP Industrial Park Plot 3A, Cikarang Selatan, Bekasi 17550"
            },
            {
            "label": "Kabupaten / Provinsi",
            "value": "Bekasi,Jawa Barat"
            },
            {
            "label": "Luas",
            "value": "320  HA"
            },
            {
            "label": "Website",
            "value": ""
            },
            {
            "label": "Bandara terdekat",
            "value": null
            },
            {
            "label": "Pelabuhan terdekat",
            "value": null
            },
            {
            "label": "Longitude",
            "value": "107.13269439713"
            },
            {
            "label": "Latitude",
            "value": "-6.19789470787367"
            }
        ]
        },
        {
        "id": 72,
        "name": "Greenland International Industrial Centre",
        "details": [
            {
            "label": "Alamat",
            "value": "Jl. Tol Jakarta-Cikampek KM 37, Cikarang Pusat"
            },
            {
            "label": "Kabupaten / Provinsi",
            "value": "Bekasi,Jawa Barat"
            },
            {
            "label": "Luas",
            "value": "1.42999994754791  HA"
            },
            {
            "label": "Website",
            "value": "http://www.kota-deltamas.com"
            },
            {
            "label": "Bandara terdekat",
            "value": null
            },
            {
            "label": "Pelabuhan terdekat",
            "value": null
            },
            {
            "label": "Longitude",
            "value": "107.13269439713"
            },
            {
            "label": "Latitude",
            "value": "-6.19789470787367"
            }
        ]
        },
        {
        "id": 21,
        "name": "Kawasan Industri Lion",
        "details": [
            {
            "label": "Alamat",
            "value": "Cilandak, Cibatu, Kab. Purwakarta, Jawa Barat"
            },
            {
            "label": "Kabupaten / Provinsi",
            "value": "Purwakarta,Jawa Barat"
            },
            {
            "label": "Luas",
            "value": "50  HA"
            },
            {
            "label": "Website",
            "value": ""
            },
            {
            "label": "Bandara terdekat",
            "value": null
            },
            {
            "label": "Pelabuhan terdekat",
            "value": null
            },
            {
            "label": "Longitude",
            "value": "107.411312588606"
            },
            {
            "label": "Latitude",
            "value": "-6.59235286159446"
            }
        ]
        },
        {
        "id": 20,
        "name": "Kota Bukit Indah Industrial City",
        "details": [
            {
            "label": "Alamat",
            "value": "Jl. Veteran, Wanakerta, Bungursari, Kabupaten Purwakarta, Jawa Barat"
            },
            {
            "label": "Kabupaten / Provinsi",
            "value": "Purwakarta,Jawa Barat"
            },
            {
            "label": "Luas",
            "value": "1425  HA"
            },
            {
            "label": "Website",
            "value": "www.kotabukitindah.com"
            },
            {
            "label": "Bandara terdekat",
            "value": null
            },
            {
            "label": "Pelabuhan terdekat",
            "value": null
            },
            {
            "label": "Longitude",
            "value": "107.411312588606"
            },
            {
            "label": "Latitude",
            "value": "-6.59235286159446"
            }
        ]
        },
        {
        "id": 40,
        "name": "Pergudangan & Industri Safe N Lock",
        "details": [
            {
            "label": "Alamat",
            "value": "Jalan Lingkar Timur, KM 5.5, Sidoarjo Sub-Distrcit, East Java"
            },
            {
            "label": "Kabupaten / Provinsi",
            "value": "Sidoarjo,Jawa Timur"
            },
            {
            "label": "Luas",
            "value": "197  HA"
            },
            {
            "label": "Website",
            "value": "www.safenlock.com"
            },
            {
            "label": "Bandara terdekat",
            "value": null
            },
            {
            "label": "Pelabuhan terdekat",
            "value": null
            },
            {
            "label": "Longitude",
            "value": "112.664602682169"
            },
            {
            "label": "Latitude",
            "value": "-7.45340309697457"
            }
        ]
        },
        {
        "id": 42,
        "name": "Kawasan Industri Tuban",
        "details": [
            {
            "label": "Alamat",
            "value": "Socorejo, Jenu, Tuban Regency, East Java"
            },
            {
            "label": "Kabupaten / Provinsi",
            "value": "Tuban,Jawa Timur"
            },
            {
            "label": "Luas",
            "value": "227  HA"
            },
            {
            "label": "Website",
            "value": "-"
            },
            {
            "label": "Bandara terdekat",
            "value": null
            },
            {
            "label": "Pelabuhan terdekat",
            "value": null
            },
            {
            "label": "Longitude",
            "value": "111.896037811397"
            },
            {
            "label": "Latitude",
            "value": "-6.95945297857128"
            }
        ]
        },
        {
        "id": 45,
        "name": "Kawasan Industri Maspion",
        "details": [
            {
            "label": "Alamat",
            "value": "Jalan Raya Sukomulyo, Manyar, Kec. Gresik, Jawa Timur"
            },
            {
            "label": "Kabupaten / Provinsi",
            "value": "Gresik,Jawa Timur"
            },
            {
            "label": "Luas",
            "value": "1000  HA"
            },
            {
            "label": "Website",
            "value": "www.maspionindustrialestate.com"
            },
            {
            "label": "Bandara terdekat",
            "value": null
            },
            {
            "label": "Pelabuhan terdekat",
            "value": null
            },
            {
            "label": "Longitude",
            "value": "112.62907832"
            },
            {
            "label": "Latitude",
            "value": "-7.11882687000002"
            }
        ]
        },
        {
        "id": 44,
        "name": "Kawasan Industri Gresik",
        "details": [
            {
            "label": "Alamat",
            "value": "Jl. Tri Dharma, Tlogopojok, Kec. Gresik, Kabupaten Gresik, Jawa Timur"
            },
            {
            "label": "Kabupaten / Provinsi",
            "value": "Gresik,Jawa Timur"
            },
            {
            "label": "Luas",
            "value": "140  HA"
            },
            {
            "label": "Website",
            "value": "www.kig.co.id"
            },
            {
            "label": "Bandara terdekat",
            "value": null
            },
            {
            "label": "Pelabuhan terdekat",
            "value": null
            },
            {
            "label": "Longitude",
            "value": "112.571472394836"
            },
            {
            "label": "Latitude",
            "value": "-6.56161407183112"
            }
        ]
        },
        {
        "id": 43,
        "name": "Java Integrated Industrial and Port Estate",
        "details": [
            {
            "label": "Alamat",
            "value": "Manyarsidorukun, Manyar, Manyar Sido Rukun, Kec. Gresik, Kabupaten Gresik, Jawa Timur"
            },
            {
            "label": "Kabupaten / Provinsi",
            "value": "Gresik,Jawa Timur"
            },
            {
            "label": "Luas",
            "value": "3000  HA"
            },
            {
            "label": "Website",
            "value": "www.jipe.com"
            },
            {
            "label": "Bandara terdekat",
            "value": null
            },
            {
            "label": "Pelabuhan terdekat",
            "value": null
            },
            {
            "label": "Longitude",
            "value": "112.571472394836"
            },
            {
            "label": "Latitude",
            "value": "-6.56161407183112"
            }
        ]
        },
        {
        "id": 52,
        "name": "Modern Cikande Industrial Estate",
        "details": [
            {
            "label": "Alamat",
            "value": "Jl. Raya Jakarta-Serang KM. 68, Cikande, Serang, Banten"
            },
            {
            "label": "Kabupaten / Provinsi",
            "value": "Serang,Banten"
            },
            {
            "label": "Luas",
            "value": "2175  HA"
            },
            {
            "label": "Website",
            "value": "www.modern-cikande.co.id"
            },
            {
            "label": "Bandara terdekat",
            "value": null
            },
            {
            "label": "Pelabuhan terdekat",
            "value": null
            },
            {
            "label": "Longitude",
            "value": "106.118592827098"
            },
            {
            "label": "Latitude",
            "value": "-6.07209732243268"
            }
        ]
        },
        {
        "id": 49,
        "name": "Taman Tekno Bumi Serpong Damai",
        "details": [
            {
            "label": "Alamat",
            "value": "Jl. Raya Serpong - Tangerang 15322"
            },
            {
            "label": "Kabupaten / Provinsi",
            "value": "Tangerang,Banten"
            },
            {
            "label": "Luas",
            "value": "200  HA"
            },
            {
            "label": "Website",
            "value": "www.bsdcity.com"
            },
            {
            "label": "Bandara terdekat",
            "value": null
            },
            {
            "label": "Pelabuhan terdekat",
            "value": null
            },
            {
            "label": "Longitude",
            "value": "106.650023219697"
            },
            {
            "label": "Latitude",
            "value": "-6.17669032899465"
            }
        ]
        },
        {
        "id": 48,
        "name": "Kawasan Industri &amp;amp; Perdagangan Cikupamas",
        "details": [
            {
            "label": "Alamat",
            "value": "JL. Cikupamas Raya, No. 8, Cikupa, Kawasan Industri &amp;amp; Pergudangan Cikupamas, Talaga, Cikupa, Tangerang, Banten"
            },
            {
            "label": "Kabupaten / Provinsi",
            "value": "Tangerang,Banten"
            },
            {
            "label": "Luas",
            "value": "250  HA"
            },
            {
            "label": "Website",
            "value": ""
            },
            {
            "label": "Bandara terdekat",
            "value": null
            },
            {
            "label": "Pelabuhan terdekat",
            "value": null
            },
            {
            "label": "Longitude",
            "value": "106.650023219697"
            },
            {
            "label": "Latitude",
            "value": "-6.17669032899465"
            }
        ]
        },
        {
        "id": 4,
        "name": "Kawasan Industri Dumai",
        "details": [
            {
            "label": "Alamat",
            "value": "Jl. Pulau Sumatera, Pelintung, Medang Kampai, Kota Dumai, Riau"
            },
            {
            "label": "Kabupaten / Provinsi",
            "value": "Dumai,Riau"
            },
            {
            "label": "Luas",
            "value": "0  HA"
            },
            {
            "label": "Website",
            "value": "-"
            },
            {
            "label": "Bandara terdekat",
            "value": null
            },
            {
            "label": "Pelabuhan terdekat",
            "value": null
            },
            {
            "label": "Longitude",
            "value": "110.33423336"
            },
            {
            "label": "Latitude",
            "value": "-6.96644285999997"
            }
        ]
        }
    ],
    "KawasanIndustri": [
        {
        "id": 88,
        "name": "Kawasan Industri Ketapang",
        "details": [
            {
            "label": "Alamat",
            "value": "Kelurahan Bacang dan Air Itam Kecamatan Bukit Intan Kota Pangkalpinang"
            },
            {
            "label": "Kabupaten / Provinsi",
            "value": "Pangkalpinang,Bangka-Belitung"
            },
            {
            "label": "Luas",
            "value": "422  HA"
            },
            {
            "label": "Website",
            "value": ""
            },
            {
            "label": "Bandara terdekat",
            "value": null
            },
            {
            "label": "Pelabuhan terdekat",
            "value": null
            },
            {
            "label": "Longitude",
            "value": "106.111112383231"
            },
            {
            "label": "Latitude",
            "value": "-2.10929706021034"
            }
        ]
        },
        {
        "id": 97,
        "name": "Taman Tekno Bumi Serpong Damai",
        "details": [
            {
            "label": "Alamat",
            "value": "coba case 4"
            },
            {
            "label": "Kabupaten / Provinsi",
            "value": "Tobasamosir,Sumatera Utara"
            },
            {
            "label": "Luas",
            "value": "192891  -"
            },
            {
            "label": "Website",
            "value": "http://udemy.com"
            },
            {
            "label": "Bandara terdekat",
            "value": "KUALA BATU"
            },
            {
            "label": "Pelabuhan terdekat",
            "value": "Sinabang"
            },
            {
            "label": "Longitude",
            "value": "112.742042229004"
            },
            {
            "label": "Latitude",
            "value": "-2.15006863568097"
            }
        ]
        },
        {
        "id": 98,
        "name": "Karawang Jabar Industrial Estate",
        "details": [
            {
            "label": "Alamat",
            "value": "tes"
            },
            {
            "label": "Kabupaten / Provinsi",
            "value": "Bireuen,Aceh"
            },
            {
            "label": "Luas",
            "value": "12  -"
            },
            {
            "label": "Website",
            "value": "-"
            },
            {
            "label": "Bandara terdekat",
            "value": "KUALA BATU"
            },
            {
            "label": "Pelabuhan terdekat",
            "value": "Singkil"
            },
            {
            "label": "Longitude",
            "value": "107.633979380486"
            },
            {
            "label": "Latitude",
            "value": "-7.25126109311359"
            }
        ]
        },
        {
        "id": 99,
        "name": "Pertiwi Lestari Industrial Estate",
        "details": [
            {
            "label": "Alamat",
            "value": "tes"
            },
            {
            "label": "Kabupaten / Provinsi",
            "value": "Bireuen,Aceh"
            },
            {
            "label": "Luas",
            "value": "12  -"
            },
            {
            "label": "Website",
            "value": "-"
            },
            {
            "label": "Bandara terdekat",
            "value": "KUALA BATU"
            },
            {
            "label": "Pelabuhan terdekat",
            "value": "Singkil"
            },
            {
            "label": "Longitude",
            "value": "107.633979380486"
            },
            {
            "label": "Latitude",
            "value": "-7.25126109311359"
            }
        ]
        },
        {
        "id": 100,
        "name": "Artha Industrial Hill",
        "details": [
            {
            "label": "Alamat",
            "value": "tes"
            },
            {
            "label": "Kabupaten / Provinsi",
            "value": "Bireuen,Aceh"
            },
            {
            "label": "Luas",
            "value": "12  -"
            },
            {
            "label": "Website",
            "value": "-"
            },
            {
            "label": "Bandara terdekat",
            "value": "KUALA BATU"
            },
            {
            "label": "Pelabuhan terdekat",
            "value": "Singkil"
            },
            {
            "label": "Longitude",
            "value": "107.633979380486"
            },
            {
            "label": "Latitude",
            "value": "-7.25126109311359"
            }
        ]
        },
        {
        "id": 101,
        "name": "Jambi Integrated City",
        "details": [
            {
            "label": "Alamat",
            "value": "tes"
            },
            {
            "label": "Kabupaten / Provinsi",
            "value": "Bireuen,Aceh"
            },
            {
            "label": "Luas",
            "value": "12  -"
            },
            {
            "label": "Website",
            "value": "-"
            },
            {
            "label": "Bandara terdekat",
            "value": "KUALA BATU"
            },
            {
            "label": "Pelabuhan terdekat",
            "value": "Singkil"
            },
            {
            "label": "Longitude",
            "value": "107.633979380486"
            },
            {
            "label": "Latitude",
            "value": "-7.25126109311359"
            }
        ]
        }
    ],
    "KawasanIndustriKek": [
        {
        "id": 84,
        "name": "Kawasan Ekonomi Khusus Palu",
        "details": [
            {
            "label": "Alamat",
            "value": ""
            },
            {
            "label": "Kabupaten / Provinsi",
            "value": "Palu,Sulawesi Tengah"
            },
            {
            "label": "Luas",
            "value": "0  HA"
            },
            {
            "label": "Website",
            "value": "-"
            },
            {
            "label": "Bandara terdekat",
            "value": null
            },
            {
            "label": "Pelabuhan terdekat",
            "value": null
            },
            {
            "label": "Longitude",
            "value": "119.899274191951"
            },
            {
            "label": "Latitude",
            "value": "-0.789562043053024"
            }
        ]
        },
        {
        "id": 83,
        "name": "Kawasan Ekonomi Khusus (KEK) Arun Lhokseumawe",
        "details": [
            {
            "label": "Alamat",
            "value": "Kabupaten Aceh Utara dan Kota Lhokseumawe"
            },
            {
            "label": "Kabupaten / Provinsi",
            "value": "Aceh Utara,Aceh"
            },
            {
            "label": "Luas",
            "value": "2623  HA"
            },
            {
            "label": "Website",
            "value": ""
            },
            {
            "label": "Bandara terdekat",
            "value": null
            },
            {
            "label": "Pelabuhan terdekat",
            "value": null
            },
            {
            "label": "Longitude",
            "value": "96.6243453820348"
            },
            {
            "label": "Latitude",
            "value": "5.08347361271791"
            }
        ]
        },
        {
        "id": 82,
        "name": "KEK Arun Lhokseumawe",
        "details": [
            {
            "label": "Alamat",
            "value": "Kabupaten Aceh Utara dan Kota Lhokseumawe"
            },
            {
            "label": "Kabupaten / Provinsi",
            "value": "Aceh Utara,Aceh"
            },
            {
            "label": "Luas",
            "value": "2623  HA"
            },
            {
            "label": "Website",
            "value": ""
            },
            {
            "label": "Bandara terdekat",
            "value": null
            },
            {
            "label": "Pelabuhan terdekat",
            "value": null
            },
            {
            "label": "Longitude",
            "value": "96.6243453820348"
            },
            {
            "label": "Latitude",
            "value": "5.08347361271791"
            }
        ]
        },
        {
        "id": 81,
        "name": "Kawasan Ekonomi Khusus Singhasari",
        "details": [
            {
            "label": "Alamat",
            "value": "Kabupaten Aceh Utara dan Kota Lhokseumawe"
            },
            {
            "label": "Kabupaten / Provinsi",
            "value": "Aceh Utara,Aceh"
            },
            {
            "label": "Luas",
            "value": "2623  HA"
            },
            {
            "label": "Website",
            "value": ""
            },
            {
            "label": "Bandara terdekat",
            "value": null
            },
            {
            "label": "Pelabuhan terdekat",
            "value": null
            },
            {
            "label": "Longitude",
            "value": "96.6243453820348"
            },
            {
            "label": "Latitude",
            "value": "5.08347361271791"
            }
        ]
        },
        {
        "id": 80,
        "name": "Kawasan Ekonomi Khusus (KEK) Likupang",
        "details": [
            {
            "label": "Alamat",
            "value": "Kabupaten Aceh Utara dan Kota Lhokseumawe"
            },
            {
            "label": "Kabupaten / Provinsi",
            "value": "Aceh Utara,Aceh"
            },
            {
            "label": "Luas",
            "value": "2623  HA"
            },
            {
            "label": "Website",
            "value": ""
            },
            {
            "label": "Bandara terdekat",
            "value": null
            },
            {
            "label": "Pelabuhan terdekat",
            "value": null
            },
            {
            "label": "Longitude",
            "value": "96.6243453820348"
            },
            {
            "label": "Latitude",
            "value": "5.08347361271791"
            }
        ]
        },
        {
        "id": 79,
        "name": "Kawasan Ekonomi Khusus (KEK) Morotai",
        "details": [
            {
            "label": "Alamat",
            "value": "Kabupaten Aceh Utara dan Kota Lhokseumawe"
            },
            {
            "label": "Kabupaten / Provinsi",
            "value": "Aceh Utara,Aceh"
            },
            {
            "label": "Luas",
            "value": "2623  HA"
            },
            {
            "label": "Website",
            "value": ""
            },
            {
            "label": "Bandara terdekat",
            "value": null
            },
            {
            "label": "Pelabuhan terdekat",
            "value": null
            },
            {
            "label": "Longitude",
            "value": "96.6243453820348"
            },
            {
            "label": "Latitude",
            "value": "5.08347361271791"
            }
        ]
        },
        {
        "id": 78,
        "name": "Kawasan Ekonomi Khusus (KEK) Nongsa",
        "details": [
            {
            "label": "Alamat",
            "value": "Kabupaten Aceh Utara dan Kota Lhokseumawe"
            },
            {
            "label": "Kabupaten / Provinsi",
            "value": "Aceh Utara,Aceh"
            },
            {
            "label": "Luas",
            "value": "2623  HA"
            },
            {
            "label": "Website",
            "value": ""
            },
            {
            "label": "Bandara terdekat",
            "value": null
            },
            {
            "label": "Pelabuhan terdekat",
            "value": null
            },
            {
            "label": "Longitude",
            "value": "96.6243453820348"
            },
            {
            "label": "Latitude",
            "value": "5.08347361271791"
            }
        ]
        }
    ]
}