export default [
    {
       "id":4,
       "name":"CAHAYA MAS MAKMUR",
       "location":{
          "lat":null,
          "lon":null
       },
       "propinsi":{
          "id":31,
          "name":"DKI Jakarta"
       },
       "kotaKabupaten":{
          "id":3173,
          "name":"Jakarta Pusat"
       },
       "bidangUsaha":{
          "code":"22220",
          "description":"Industri Barang Dari Plastik Untuk Pengemasan"
       },
       "image":"https://admin.regionalinvestment.bkpm.go.id:81/backend/public/upload/umkm/kayu-jati.jpg"
    },
    {
       "id":6,
       "name":"DUTA KEKAR PLASINDO",
       "location":{
          "lat":null,
          "lon":null
       },
       "propinsi":{
          "id":31,
          "name":"DKI Jakarta"
       },
       "kotaKabupaten":{
          "id":3173,
          "name":"Jakarta Pusat"
       },
       "bidangUsaha":{
          "code":"22291",
          "description":"Industri Barang Plastik Lembaran"
       },
       "image":"https://admin.regionalinvestment.bkpm.go.id:81/backend/public/upload/umkm/kayu-jati.jpg"
    },
    {
       "id":10,
       "name":"INDESSO SANAVIA INTERNASIONAL",
       "location":{
          "lat":null,
          "lon":null
       },
       "propinsi":{
          "id":31,
          "name":"DKI Jakarta"
       },
       "kotaKabupaten":{
          "id":3173,
          "name":"Jakarta Pusat"
       },
       "bidangUsaha":{
          "code":"20115",
          "description":"Industri Kimia Dasar Organik Yang Bersumber Dari Hasil Pertanian"
       },
       "image":"https://admin.regionalinvestment.bkpm.go.id:81/backend/public/upload/umkm/kayu-jati.jpg"
    },
    {
       "id":11,
       "name":"INTI INDOSAWIT SUBUR",
       "location":{
          "lat":null,
          "lon":null
       },
       "propinsi":{
          "id":31,
          "name":"DKI Jakarta"
       },
       "kotaKabupaten":{
          "id":3173,
          "name":"Jakarta Pusat"
       },
       "bidangUsaha":{
          "code":"10431",
          "description":"Industri Minyak Mentah Kelapa Sawit (Crude Palm Oil)"
       },
       "image":"https://admin.regionalinvestment.bkpm.go.id:81/backend/public/upload/umkm/batik.jpg"
    },
    {
       "id":12,
       "name":"KALTIM AMONIUM NITRAT",
       "location":{
          "lat":null,
          "lon":null
       },
       "propinsi":{
          "id":31,
          "name":"DKI Jakarta"
       },
       "kotaKabupaten":{
          "id":3173,
          "name":"Jakarta Pusat"
       },
       "bidangUsaha":{
          "code":"20114",
          "description":"Industri Kimia Dasar Anorganik Lainnya"
       },
       "image":"https://admin.regionalinvestment.bkpm.go.id:81/backend/public/upload/umkm/kayu-jati.jpg"
    },
    {
       "id":18,
       "name":"MITRA ALAM SEGAR",
       "location":{
          "lat":null,
          "lon":null
       },
       "propinsi":{
          "id":31,
          "name":"DKI Jakarta"
       },
       "kotaKabupaten":{
          "id":3173,
          "name":"Jakarta Pusat"
       },
       "bidangUsaha":{
          "code":"22299",
          "description":"Industri Barang Plastik Lainnya Ytdl"
       },
       "image":"https://admin.regionalinvestment.bkpm.go.id:81/backend/public/upload/umkm/kayu-jati.jpg"
    },
    {
       "id":19,
       "name":"MITRA ALAM SEGAR",
       "location":{
          "lat":null,
          "lon":null
       },
       "propinsi":{
          "id":31,
          "name":"DKI Jakarta"
       },
       "kotaKabupaten":{
          "id":3173,
          "name":"Jakarta Pusat"
       },
       "bidangUsaha":{
          "code":"11040",
          "description":"Industri Minuman Ringan"
       },
       "image":"https://admin.regionalinvestment.bkpm.go.id:81/backend/public/upload/umkm/batik.jpg"
    },
    {
       "id":20,
       "name":"MITRA ALAM SEGAR",
       "location":{
          "lat":null,
          "lon":null
       },
       "propinsi":{
          "id":31,
          "name":"DKI Jakarta"
       },
       "kotaKabupaten":{
          "id":3173,
          "name":"Jakarta Pusat"
       },
       "bidangUsaha":{
          "code":"22220",
          "description":"Industri Barang Dari Plastik Untuk Pengemasan"
       },
       "image":"https://admin.regionalinvestment.bkpm.go.id:81/backend/public/upload/umkm/kayu-jati.jpg"
    },
    {
       "id":21,
       "name":"MITRA ALAM SEGAR",
       "location":{
          "lat":null,
          "lon":null
       },
       "propinsi":{
          "id":31,
          "name":"DKI Jakarta"
       },
       "kotaKabupaten":{
          "id":3173,
          "name":"Jakarta Pusat"
       },
       "bidangUsaha":{
          "code":"10763",
          "description":"Industri Pengolahan Teh"
       },
       "image":"https://admin.regionalinvestment.bkpm.go.id:81/backend/public/upload/umkm/batik.jpg"
    },
    {
       "id":23,
       "name":"MULTI INDOMANDIRI",
       "location":{
          "lat":null,
          "lon":null
       },
       "propinsi":{
          "id":31,
          "name":"DKI Jakarta"
       },
       "kotaKabupaten":{
          "id":3173,
          "name":"Jakarta Pusat"
       },
       "bidangUsaha":{
          "code":"20231",
          "description":"Industri Sabun Dan Bahan Pembersih Keperluan Rumah Tangga"
       },
       "image":"https://admin.regionalinvestment.bkpm.go.id:81/backend/public/upload/umkm/batik.jpg"
    },
    {
       "id":24,
       "name":"MULTI MANAO INDONESIA",
       "location":{
          "lat":null,
          "lon":null
       },
       "propinsi":{
          "id":31,
          "name":"DKI Jakarta"
       },
       "kotaKabupaten":{
          "id":3173,
          "name":"Jakarta Pusat"
       },
       "bidangUsaha":{
          "code":"31001",
          "description":"Industri Furnitur Dari Kayu"
       },
       "image":"https://admin.regionalinvestment.bkpm.go.id:81/backend/public/upload/umkm/kayu-jati.jpg"
    },
    {
       "id":39,
       "name":"SUGGO METAL CEMERLANG",
       "location":{
          "lat":null,
          "lon":null
       },
       "propinsi":{
          "id":31,
          "name":"DKI Jakarta"
       },
       "kotaKabupaten":{
          "id":3173,
          "name":"Jakarta Pusat"
       },
       "bidangUsaha":{
          "code":"25992",
          "description":"Industri Peralatan Dapur Dan Peralatan Meja Dari Logam"
       },
       "image":"https://admin.regionalinvestment.bkpm.go.id:81/backend/public/upload/umkm/batik.jpg"
    },
    {
       "id":40,
       "name":"SURABAYA MEKABOX",
       "location":{
          "lat":null,
          "lon":null
       },
       "propinsi":{
          "id":31,
          "name":"DKI Jakarta"
       },
       "kotaKabupaten":{
          "id":3173,
          "name":"Jakarta Pusat"
       },
       "bidangUsaha":{
          "code":"17022",
          "description":"Industri Kemasan Dan Kotak Dari Kertas Dan Karton"
       },
       "image":"https://admin.regionalinvestment.bkpm.go.id:81/backend/public/upload/umkm/kayu-jati.jpg"
    },
    {
       "id":42,
       "name":"TIRTA FRESINDO JAYA",
       "location":{
          "lat":null,
          "lon":null
       },
       "propinsi":{
          "id":31,
          "name":"DKI Jakarta"
       },
       "kotaKabupaten":{
          "id":3173,
          "name":"Jakarta Pusat"
       },
       "bidangUsaha":{
          "code":"22220",
          "description":"Industri Barang Dari Plastik Untuk Pengemasan"
       },
       "image":"https://admin.regionalinvestment.bkpm.go.id:81/backend/public/upload/umkm/kayu-jati.jpg"
    },
    {
       "id":45,
       "name":"TRIMITRA MEBELINDO",
       "location":{
          "lat":null,
          "lon":null
       },
       "propinsi":{
          "id":31,
          "name":"DKI Jakarta"
       },
       "kotaKabupaten":{
          "id":3173,
          "name":"Jakarta Pusat"
       },
       "bidangUsaha":{
          "code":"31001",
          "description":"Industri Furnitur Dari Kayu"
       },
       "image":"https://admin.regionalinvestment.bkpm.go.id:81/backend/public/upload/umkm/batik.jpg"
    },
    {
       "id":46,
       "name":"UNO POLYMER INDONESIA",
       "location":{
          "lat":null,
          "lon":null
       },
       "propinsi":{
          "id":31,
          "name":"DKI Jakarta"
       },
       "kotaKabupaten":{
          "id":3173,
          "name":"Jakarta Pusat"
       },
       "bidangUsaha":{
          "code":"22210",
          "description":"Industri Barang dari Plastik Untuk Bangunan"
       },
       "image":"https://admin.regionalinvestment.bkpm.go.id:81/backend/public/upload/umkm/kayu-jati.jpg"
    },
    {
       "id":48,
       "name":"YANASURYA BHAKTIPERSADA",
       "location":{
          "lat":null,
          "lon":null
       },
       "propinsi":{
          "id":31,
          "name":"DKI Jakarta"
       },
       "kotaKabupaten":{
          "id":3173,
          "name":"Jakarta Pusat"
       },
       "bidangUsaha":{
          "code":"22299",
          "description":"Industri Barang Plastik Lainnya Ytdl"
       },
       "image":"https://admin.regionalinvestment.bkpm.go.id:81/backend/public/upload/umkm/kayu-jati.jpg"
    },
    {
       "id":54,
       "name":"ANEKA GAS INDUSTRI",
       "location":{
          "lat":null,
          "lon":null
       },
       "propinsi":{
          "id":31,
          "name":"DKI Jakarta"
       },
       "kotaKabupaten":{
          "id":3173,
          "name":"Jakarta Pusat"
       },
       "bidangUsaha":{
          "code":"20112",
          "description":"Industri Kimia Dasar Anorganik Gas Industri"
       },
       "image":"https://admin.regionalinvestment.bkpm.go.id:81/backend/public/upload/umkm/kayu-jati.jpg"
    },
    {
       "id":55,
       "name":"BALINA INDUSTRIAL PERKASA",
       "location":{
          "lat":null,
          "lon":null
       },
       "propinsi":{
          "id":31,
          "name":"DKI Jakarta"
       },
       "kotaKabupaten":{
          "id":3173,
          "name":"Jakarta Pusat"
       },
       "bidangUsaha":{
          "code":"25940",
          "description":"Industri Ember, Kaleng, Drum Dan Wadah Sejenis Dari Logam"
       },
       "image":"https://admin.regionalinvestment.bkpm.go.id:81/backend/public/upload/umkm/batik.jpg"
    },
    {
       "id":58,
       "name":"CHAROEN POKPHAND INDONESIA TBK",
       "location":{
          "lat":null,
          "lon":null
       },
       "propinsi":{
          "id":31,
          "name":"DKI Jakarta"
       },
       "kotaKabupaten":{
          "id":3173,
          "name":"Jakarta Pusat"
       },
       "bidangUsaha":{
          "code":"22292",
          "description":"Industri Perlengkapan Dan Peralatan Rumah Tangga (tidak Termasuk Furnitur)"
       },
       "image":"https://admin.regionalinvestment.bkpm.go.id:81/backend/public/upload/umkm/kayu-jati.jpg"
    },
    {
       "id":59,
       "name":"CENTRAL SAHABAT BARU",
       "location":{
          "lat":null,
          "lon":null
       },
       "propinsi":{
          "id":31,
          "name":"DKI Jakarta"
       },
       "kotaKabupaten":{
          "id":3173,
          "name":"Jakarta Pusat"
       },
       "bidangUsaha":{
          "code":"25940",
          "description":"Industri Ember, Kaleng, Drum Dan Wadah Sejenis Dari Logam"
       },
       "image":"https://admin.regionalinvestment.bkpm.go.id:81/backend/public/upload/umkm/batik.jpg"
    },
    {
       "id":62,
       "name":"DIAMOND COLD STORAGE",
       "location":{
          "lat":null,
          "lon":null
       },
       "propinsi":{
          "id":31,
          "name":"DKI Jakarta"
       },
       "kotaKabupaten":{
          "id":3173,
          "name":"Jakarta Pusat"
       },
       "bidangUsaha":{
          "code":"10510",
          "description":"Industri Pengolahan Susu Segar dan Krim"
       },
       "image":"https://admin.regionalinvestment.bkpm.go.id:81/backend/public/upload/umkm/kayu-jati.jpg"
    },
    {
       "id":63,
       "name":"DIAMOND COLD STORAGE",
       "location":{
          "lat":null,
          "lon":null
       },
       "propinsi":{
          "id":31,
          "name":"DKI Jakarta"
       },
       "kotaKabupaten":{
          "id":3173,
          "name":"Jakarta Pusat"
       },
       "bidangUsaha":{
          "code":"10732",
          "description":"Industri Makanan Dari Cokelat Dan Kembang Gula dari Coklat"
       },
       "image":"https://admin.regionalinvestment.bkpm.go.id:81/backend/public/upload/umkm/batik.jpg"
    },
    {
       "id":64,
       "name":"DIAMOND COLD STORAGE",
       "location":{
          "lat":null,
          "lon":null
       },
       "propinsi":{
          "id":31,
          "name":"DKI Jakarta"
       },
       "kotaKabupaten":{
          "id":3173,
          "name":"Jakarta Pusat"
       },
       "bidangUsaha":{
          "code":"10312",
          "description":"Industri Pelumatan Buah-buahan Dan Sayuran"
       },
       "image":"https://admin.regionalinvestment.bkpm.go.id:81/backend/public/upload/umkm/kayu-jati.jpg"
    },
    {
       "id":65,
       "name":"DIAMOND COLD STORAGE",
       "location":{
          "lat":null,
          "lon":null
       },
       "propinsi":{
          "id":31,
          "name":"DKI Jakarta"
       },
       "kotaKabupaten":{
          "id":3173,
          "name":"Jakarta Pusat"
       },
       "bidangUsaha":{
          "code":"10531",
          "description":"Industri Pengolahan Es Krim"
       },
       "image":"https://admin.regionalinvestment.bkpm.go.id:81/backend/public/upload/umkm/batik.jpg"
    },
    {
       "id":66,
       "name":"DIAMOND COLD STORAGE",
       "location":{
          "lat":null,
          "lon":null
       },
       "propinsi":{
          "id":31,
          "name":"DKI Jakarta"
       },
       "kotaKabupaten":{
          "id":3173,
          "name":"Jakarta Pusat"
       },
       "bidangUsaha":{
          "code":"10710",
          "description":"Industri Produk Roti Dan Kue"
       },
       "image":"https://admin.regionalinvestment.bkpm.go.id:81/backend/public/upload/umkm/kayu-jati.jpg"
    },
    {
       "id":75,
       "name":"MULTI SPUNINDO JAYA",
       "location":{
          "lat":null,
          "lon":null
       },
       "propinsi":{
          "id":31,
          "name":"DKI Jakarta"
       },
       "kotaKabupaten":{
          "id":3173,
          "name":"Jakarta Pusat"
       },
       "bidangUsaha":{
          "code":"13993",
          "description":"Industri Non Woven (bukan Tenunan)"
       },
       "image":"https://admin.regionalinvestment.bkpm.go.id:81/backend/public/upload/umkm/batik.jpg"
    },
    {
       "id":76,
       "name":"HERLINA INDAH",
       "location":{
          "lat":null,
          "lon":null
       },
       "propinsi":{
          "id":31,
          "name":"DKI Jakarta"
       },
       "kotaKabupaten":{
          "id":3173,
          "name":"Jakarta Pusat"
       },
       "bidangUsaha":{
          "code":"20231",
          "description":"Industri Sabun Dan Bahan Pembersih Keperluan Rumah Tangga"
       },
       "image":"https://admin.regionalinvestment.bkpm.go.id:81/backend/public/upload/umkm/kayu-jati.jpg"
    },
    {
       "id":77,
       "name":"GANDUM",
       "location":{
          "lat":null,
          "lon":null
       },
       "propinsi":{
          "id":31,
          "name":"DKI Jakarta"
       },
       "kotaKabupaten":{
          "id":3173,
          "name":"Jakarta Pusat"
       },
       "bidangUsaha":{
          "code":"12011",
          "description":"Industri Sigaret Kretek Tangan"
       },
       "image":"https://admin.regionalinvestment.bkpm.go.id:81/backend/public/upload/umkm/batik.jpg"
    },
    {
       "id":78,
       "name":"PUTRA NAGA INDOTAMA",
       "location":{
          "lat":null,
          "lon":null
       },
       "propinsi":{
          "id":31,
          "name":"DKI Jakarta"
       },
       "kotaKabupaten":{
          "id":3173,
          "name":"Jakarta Pusat"
       },
       "bidangUsaha":{
          "code":"22220",
          "description":"Industri Barang Dari Plastik Untuk Pengemasan"
       },
       "image":"https://admin.regionalinvestment.bkpm.go.id:81/backend/public/upload/umkm/kayu-jati.jpg"
    },
    {
       "id":131,
       "name":"MUTU GADING TEKSTIL",
       "location":{
          "lat":null,
          "lon":null
       },
       "propinsi":{
          "id":31,
          "name":"DKI Jakarta"
       },
       "kotaKabupaten":{
          "id":3173,
          "name":"Jakarta Pusat"
       },
       "bidangUsaha":{
          "code":"20301",
          "description":"Industri Serat/Benang/Strip Filamen Buatan"
       },
       "image":"https://admin.regionalinvestment.bkpm.go.id:81/backend/public/upload/umkm/batik.jpg"
    },
    {
       "id":83,
       "name":"INDO-RAMA SYNTHETICS TBK",
       "location":{
          "lat":null,
          "lon":null
       },
       "propinsi":{
          "id":31,
          "name":"DKI Jakarta"
       },
       "kotaKabupaten":{
          "id":3173,
          "name":"Jakarta Pusat"
       },
       "bidangUsaha":{
          "code":"13112",
          "description":"Industri Pemintalan Benang"
       },
       "image":"https://admin.regionalinvestment.bkpm.go.id:81/backend/public/upload/umkm/batik.jpg"
    },
    {
       "id":84,
       "name":"SOHO INDUSTRI PHARMASI",
       "location":{
          "lat":null,
          "lon":null
       },
       "propinsi":{
          "id":31,
          "name":"DKI Jakarta"
       },
       "kotaKabupaten":{
          "id":3173,
          "name":"Jakarta Pusat"
       },
       "bidangUsaha":{
          "code":"21012",
          "description":"Industri Produk Farmasi Untuk Manusia"
       },
       "image":"https://admin.regionalinvestment.bkpm.go.id:81/backend/public/upload/umkm/kayu-jati.jpg"
    },
    {
       "id":85,
       "name":"PT GEMAH MAKMUR SEJAHTERA",
       "location":{
          "lat":null,
          "lon":null
       },
       "propinsi":{
          "id":31,
          "name":"DKI Jakarta"
       },
       "kotaKabupaten":{
          "id":3173,
          "name":"Jakarta Pusat"
       },
       "bidangUsaha":{
          "code":"22220",
          "description":"Industri Barang Dari Plastik Untuk Pengemasan"
       },
       "image":"https://admin.regionalinvestment.bkpm.go.id:81/backend/public/upload/umkm/batik.jpg"
    },
    {
       "id":87,
       "name":"SUNRISE STEEL",
       "location":{
          "lat":null,
          "lon":null
       },
       "propinsi":{
          "id":31,
          "name":"DKI Jakarta"
       },
       "kotaKabupaten":{
          "id":3173,
          "name":"Jakarta Pusat"
       },
       "bidangUsaha":{
          "code":"24102",
          "description":"Industri Penggilingan Baja (Steel Rolling)"
       },
       "image":"https://admin.regionalinvestment.bkpm.go.id:81/backend/public/upload/umkm/batik.jpg"
    },
    {
       "id":93,
       "name":"LAS TRANSPORT INDONESIA",
       "location":{
          "lat":null,
          "lon":null
       },
       "propinsi":{
          "id":31,
          "name":"DKI Jakarta"
       },
       "kotaKabupaten":{
          "id":3173,
          "name":"Jakarta Pusat"
       },
       "bidangUsaha":{
          "code":"49431",
          "description":"Angkutan Bermotor untuk Barang Umum"
       },
       "image":"https://admin.regionalinvestment.bkpm.go.id:81/backend/public/upload/umkm/batik.jpg"
    },
    {
       "id":94,
       "name":"MAYORA INDAH",
       "location":{
          "lat":null,
          "lon":null
       },
       "propinsi":{
          "id":31,
          "name":"DKI Jakarta"
       },
       "kotaKabupaten":{
          "id":3173,
          "name":"Jakarta Pusat"
       },
       "bidangUsaha":{
          "code":"10710",
          "description":"Industri Produk Roti Dan Kue"
       },
       "image":"https://admin.regionalinvestment.bkpm.go.id:81/backend/public/upload/umkm/kayu-jati.jpg"
    },
    {
       "id":96,
       "name":"AGUNG PELITA INDUSTRINDO",
       "location":{
          "lat":null,
          "lon":null
       },
       "propinsi":{
          "id":31,
          "name":"DKI Jakarta"
       },
       "kotaKabupaten":{
          "id":3173,
          "name":"Jakarta Pusat"
       },
       "bidangUsaha":{
          "code":"15202",
          "description":"Industri Sepatu Olahraga"
       },
       "image":"https://admin.regionalinvestment.bkpm.go.id:81/backend/public/upload/umkm/kayu-jati.jpg"
    },
    {
       "id":97,
       "name":"GANDUM MAS KENCANA",
       "location":{
          "lat":null,
          "lon":null
       },
       "propinsi":{
          "id":31,
          "name":"DKI Jakarta"
       },
       "kotaKabupaten":{
          "id":3173,
          "name":"Jakarta Pusat"
       },
       "bidangUsaha":{
          "code":"10732",
          "description":"Industri Makanan Dari Cokelat Dan Kembang Gula dari Coklat"
       },
       "image":"https://admin.regionalinvestment.bkpm.go.id:81/backend/public/upload/umkm/batik.jpg"
    },
    {
       "id":105,
       "name":"PT. SAAT INDUSTRI INDONESIA",
       "location":{
          "lat":null,
          "lon":null
       },
       "propinsi":{
          "id":31,
          "name":"DKI Jakarta"
       },
       "kotaKabupaten":{
          "id":3173,
          "name":"Jakarta Pusat"
       },
       "bidangUsaha":{
          "code":"12011",
          "description":"Industri Sigaret Kretek Tangan"
       },
       "image":"https://admin.regionalinvestment.bkpm.go.id:81/backend/public/upload/umkm/batik.jpg"
    },
    {
       "id":106,
       "name":"PT. AGINCOURT RESOURCES",
       "location":{
          "lat":null,
          "lon":null
       },
       "propinsi":{
          "id":31,
          "name":"DKI Jakarta"
       },
       "kotaKabupaten":{
          "id":3173,
          "name":"Jakarta Pusat"
       },
       "bidangUsaha":{
          "code":"07301",
          "description":"Pertambangan Emas Dan Perak"
       },
       "image":"https://admin.regionalinvestment.bkpm.go.id:81/backend/public/upload/umkm/kayu-jati.jpg"
    },
    {
       "id":109,
       "name":"PT. ANUGERAH REJEKI PLASTINDO ABADI",
       "location":{
          "lat":null,
          "lon":null
       },
       "propinsi":{
          "id":31,
          "name":"DKI Jakarta"
       },
       "kotaKabupaten":{
          "id":3173,
          "name":"Jakarta Pusat"
       },
       "bidangUsaha":{
          "code":"38302",
          "description":"Pemulihan Material Barang Bukan Logam"
       },
       "image":"https://admin.regionalinvestment.bkpm.go.id:81/backend/public/upload/umkm/batik.jpg"
    },
    {
       "id":110,
       "name":"PT. GEBE INDUSTRY NICKEL",
       "location":{
          "lat":null,
          "lon":null
       },
       "propinsi":{
          "id":31,
          "name":"DKI Jakarta"
       },
       "kotaKabupaten":{
          "id":3173,
          "name":"Jakarta Pusat"
       },
       "bidangUsaha":{
          "code":"24202",
          "description":"Industri Pembuatan Logam Dasar Bukan Besi"
       },
       "image":"https://admin.regionalinvestment.bkpm.go.id:81/backend/public/upload/umkm/kayu-jati.jpg"
    },
    {
       "id":111,
       "name":"PT. SONIA INDUSTRIES",
       "location":{
          "lat":null,
          "lon":null
       },
       "propinsi":{
          "id":31,
          "name":"DKI Jakarta"
       },
       "kotaKabupaten":{
          "id":3173,
          "name":"Jakarta Pusat"
       },
       "bidangUsaha":{
          "code":"13930",
          "description":"Industri Karpet dan Permadani"
       },
       "image":"https://admin.regionalinvestment.bkpm.go.id:81/backend/public/upload/umkm/batik.jpg"
    },
    {
       "id":113,
       "name":"TEKNOMEDIKA MANUFAKTUR INOVASI",
       "location":{
          "lat":null,
          "lon":null
       },
       "propinsi":{
          "id":31,
          "name":"DKI Jakarta"
       },
       "kotaKabupaten":{
          "id":3173,
          "name":"Jakarta Pusat"
       },
       "bidangUsaha":{
          "code":"32509",
          "description":"Industri Peralatan Kedokteran Dan Kedokteran Gigi Serta Perlengkapan Lainnya"
       },
       "image":"https://admin.regionalinvestment.bkpm.go.id:81/backend/public/upload/umkm/batik.jpg"
    },
    {
       "id":122,
       "name":"INDOFOOD CBP SUKSES MAKMUR TBK",
       "location":{
          "lat":null,
          "lon":null
       },
       "propinsi":{
          "id":31,
          "name":"DKI Jakarta"
       },
       "kotaKabupaten":{
          "id":3173,
          "name":"Jakarta Pusat"
       },
       "bidangUsaha":{
          "code":"22220",
          "description":"Industri Barang Dari Plastik Untuk Pengemasan"
       },
       "image":"https://admin.regionalinvestment.bkpm.go.id:81/backend/public/upload/umkm/kayu-jati.jpg"
    },
    {
       "id":123,
       "name":"KENCANA HIJAU BINA LESTARI",
       "location":{
          "lat":null,
          "lon":null
       },
       "propinsi":{
          "id":31,
          "name":"DKI Jakarta"
       },
       "kotaKabupaten":{
          "id":3173,
          "name":"Jakarta Pusat"
       },
       "bidangUsaha":{
          "code":"20115",
          "description":"Industri Kimia Dasar Organik Yang Bersumber Dari Hasil Pertanian"
       },
       "image":"https://admin.regionalinvestment.bkpm.go.id:81/backend/public/upload/umkm/batik.jpg"
    },
    {
       "id":128,
       "name":"MULIA GRAND MANUFACTURE",
       "location":{
          "lat":null,
          "lon":null
       },
       "propinsi":{
          "id":31,
          "name":"DKI Jakarta"
       },
       "kotaKabupaten":{
          "id":3173,
          "name":"Jakarta Pusat"
       },
       "bidangUsaha":{
          "code":"17099",
          "description":"Industri Barang Dari Kertas Dan Papan Kertas Lainnya YTDL"
       },
       "image":"https://admin.regionalinvestment.bkpm.go.id:81/backend/public/upload/umkm/kayu-jati.jpg"
    },
    {
       "id":132,
       "name":"ULTRA PRIMA ABADI",
       "location":{
          "lat":null,
          "lon":null
       },
       "propinsi":{
          "id":31,
          "name":"DKI Jakarta"
       },
       "kotaKabupaten":{
          "id":3173,
          "name":"Jakarta Pusat"
       },
       "bidangUsaha":{
          "code":"10710",
          "description":"Industri Produk Roti Dan Kue"
       },
       "image":"https://admin.regionalinvestment.bkpm.go.id:81/backend/public/upload/umkm/kayu-jati.jpg"
    }
 ]