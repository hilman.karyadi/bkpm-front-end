import React, { useState, useEffect } from 'react'
import styled from 'styled-components'
import Pagination from 'rc-pagination'
import { FiChevronLeft, FiChevronRight } from 'react-icons/fi'

const PagedDisplay = ({ list, itemsPerPage, itemsPerRow, currentPage, setCurrentPage}) => {

    //const [currentPage, setCurrentPage] = useState(1)
    const [visibleItems, setVisibleItems] = useState([])
    
    useEffect(() => {
        setVisibleItems([...list.slice( (currentPage - 1) * itemsPerPage, currentPage * itemsPerPage )])
    },[currentPage, setCurrentPage])

    useEffect(() => {
        setVisibleItems([...list.slice( (currentPage - 1) * itemsPerPage, currentPage * itemsPerPage )])
    })

    if (list.length) {
        const totalPage = Math.ceil(list.length / itemsPerPage)

        const paginationRender = (current, type, element) => {
            if (type === 'prev') {
                return (
                    <>
                        { currentPage > 1 && <PageItem className="inline-block  border-gray-300 border-solid border text-sm p-2 disabled:opacity-20"><FiChevronLeft className="leading-none" /></PageItem> }
                    </>
                )
            }
            if (type === 'next') {
                return (
                    <>
                        { currentPage < totalPage && <PageItem className="inline-block  border-gray-300 border-solid border text-sm p-2 disabled:opacity-20"><FiChevronRight className="leading-none" /></PageItem> }
                    </>
                )
            }
            if (type === 'page') {
            return <PageItem href={`#${current}`} className={`block min-w-12 border-solid border text-sm p-2 disabled:opacity-20 ${ current === currentPage ? 'bg-bkpm-dark-blue-01 text-white border-bkpm-dark-blue-01' : 'border-gray-300 ' }`}>{current}</PageItem>
            }
            return element
        }

        return (
            <>
                <DisplayContainer className="grid gap-6 mb-12" $itemsPerRow={ itemsPerRow }>
                    { visibleItems }
                </DisplayContainer>
                <div className="flex justify-center">
                    <Pagination
                        className="inline-flex space-x-2 my-4"
                        onChange={setCurrentPage}
                        current={currentPage}
                        pageSize={itemsPerPage}
                        total={list.length}
                        itemRender={ paginationRender }
                        locale={{
                            // Options.jsx
                            items_per_page: '/ page',
                            jump_to: 'Go to',
                            jump_to_confirm: 'confirm',
                            page: '',
                            
                            // Pagination.jsx
                            prev_page: 'Previous Page',
                            next_page: 'Next Page',
                            prev_5: 'Previous 5 Pages',
                            next_5: 'Next 5 Pages',
                            prev_3: 'Previous 3 Pages',
                            next_3: 'Next 3 Pages',                      
                        }}
                    />
                </div>
            </>
        )
    } else {
        return <h5 className="text-center">Maaf data tidak tersedia</h5>
    }
}

const DisplayContainer = styled.div`
    grid-template-columns: repeat(${ el => el.$itemsPerRow }, minmax(0, 1fr));
`

const PageItem = styled.button`
    min-width: 2rem;
    line-height: 1;
`

export default PagedDisplay
