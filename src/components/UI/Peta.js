import React, { useState, useEffect } from "react"
import { setDefaultOptions, loadModules } from "esri-loader";

import Layout from "components/Layout/Layout"
import axios from "axios";

const Peta = ({dataLayers}) => {
 
  const [state, setState] = useState({status:"loading"}); 


  const options = {
    url: "https://js.arcgis.com/4.6/",
  };

  useEffect(async() => {
    setDefaultOptions({ css: true });
    loadModules(
        [
          "esri/Map",
          "esri/views/MapView",
          "esri/layers/FeatureLayer",
          "esri/widgets/BasemapGallery",
          "esri/widgets/LayerList",
          "esri/widgets/Legend",
        ],
        options
      ).then(([Map, MapView, FeatureLayer, BasemapGallery, LayerList, Legend]) => {
        const map = new Map({ basemap: "streets" });
  
        const view = new MapView({
          container: "viewDiv",
          map,
          zoom: 5,
          center: [117.62527, -1.4509444],
        });
  
        let basemapGallery = new BasemapGallery({
          view: view,
          showArcGISBasemaps: true,
        });
  
        basemapGallery.headingLevel = 3;
        const layerList = new LayerList({
          view,
        });
  
        view.ui.add(layerList, "top-right");
        // Add widget to the top right corner of the view
        view.ui.add(basemapGallery, {
          position: "top-right",
        });
        
        const featureLayers = 
        [
            new FeatureLayer("https://regionalinvestment.bkpm.go.id/gis/rest/services/bkpm_pir/bandara_id/MapServer/0",{outFields: ["*"]}),
            new FeatureLayer("https://regionalinvestment.bkpm.go.id/gis/rest/services/bkpm_pir/hotel_id/MapServer/0",{outFields: ["*"]}),
            new FeatureLayer("https://regionalinvestment.bkpm.go.id/gis/rest/services/bkpm_pir/kawasan_industri_id/MapServer/0",{outFields: ["*"]}),
            new FeatureLayer("https://regionalinvestment.bkpm.go.id/gis/rest/services/bkpm_pir/pelabuhan_id/MapServer/0",{outFields: ["*"]}),
            new FeatureLayer("https://regionalinvestment.bkpm.go.id/gis/rest/services/bkpm_pir/pendidikan_id/MapServer/0",{outFields: ["*"]}),
            new FeatureLayer("https://regionalinvestment.bkpm.go.id/gis/rest/services/bkpm_pir/rumah_sakit_id/MapServer/0",{outFields: ["*"]}),
        ];

        map.addMany(featureLayers);
        console.log("featureLayers",featureLayers)
        
        var layerLegend = [];
        featureLayers.map((item,index) => {
            layerLegend = {
                layer: item,
                title: "Legend",
              };
        });
        let legend = new Legend({
          view: view,
          layerInfos: layerLegend,
        });
        view.ui.add(legend, "bottom-left");

        view.then(() => {
          this.setState({
            map,
            view,
            status: "loaded",
          });
        });
  
        view.then(() => {
          setState({
            map,
            view,
            status: "loaded",
          });
        });
      });
  }, [])

  const renderMap = () => {
    if (state.status === "loading") {
        return <div>loading</div>;
    }
  }
  
  return (
      <div style={styles.container}>
        <div id="viewDiv" style={styles.mapDiv}>
          {renderMap()}
        </div>
      </div>
  )
}

const styles = {
  container: {
    height: "100vh",
    width: "100vw",
  },
  mapDiv: {
    padding: 0,
    margin: 0,
    height: "100%",
    width: "100%",
  },
};

export default Peta;