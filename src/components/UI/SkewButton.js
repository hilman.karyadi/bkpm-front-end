import React, { useState } from 'react'
import { BiRightArrowAlt } from "react-icons/bi";

const SkewButton = (props) => {
    const [isButtonHover, setIsButtonHover] = useState(false)
    return (
        <div onMouseEnter={ () => setIsButtonHover(true) } onMouseLeave={ () => setIsButtonHover(false) } className="rounded-lg w-full bg-bkpm-light-blue-01 text-white inline-block transform transition-all duration-300 inline-flex items-center" style={ isButtonHover ? { '--tw-translate-x': '-15px', 'background-color': 'rgb(var(--red))'} : {} }>
            <button className="rounded-lg bg-bkpm-light-blue-01 text-white px-8 py-2 w-full font-bold transform transition-all duration-300 origin-top-right whitespace-nowrap" style={ isButtonHover ? { '--tw-skew-x': '-15deg', 'background-color': 'rgb(var(--red))'} : {} }>
                <span className="inline-block transform transition-all duration-300" style={ isButtonHover ? { '--tw-skew-x': '15deg'} : {} }>{ props.children }</span>
            </button>
            <BiRightArrowAlt className="mr-2 text-2xl"/>
        </div>
    )
}

export default SkewButton