import React, { useState } from 'react'
import dataUmkm from 'components/store/dataUmkm'

const UmkmContext = React.createContext({
    listUmkm: [],
    setListUmkm: () => {}
})

const UmkmContextProvider = ({ children }) => {
    const [listUmkm, setListUmkm] = useState([...dataUmkm])
    return (
        <UmkmContext.Provider value={{listUmkm: [...listUmkm], setListUmkm: setListUmkm }}>
            {children}
        </UmkmContext.Provider>
    )
}

export default UmkmContext
export { UmkmContextProvider }