import React from 'react'

import ResponsiveContainer from 'components/UI/ResponsiveContainer'

import UMKMItem from './UMKMItem'

const DaftarUMKM = () => {
    return (
        <section className="pb-12">
            <ResponsiveContainer >
                <div className="grid gap-6 grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4">
                    <UMKMItem title="Jerawood Craft" area="Kabupaten Bogor" category="Kerajinan Kayu" image="/images/kayu-jati.jpg" className="h-full" >
                        <p>Pemilik usaha Jerawood Craft. UMKM ini berdiri sejak 2019. Jerawood Craft merupakan usaha aneka kerajinan limbah kayu yang disulap menjadi produk bernilai jual tinggi.</p>
                    </UMKMItem>
                    <UMKMItem title="Puspita Batik Indigo Natural" area="DIY Yogyakarta" category="Tas, Dompet, Jaket, Kemeja, Dress, Kain" image="/images/batik.jpg" className="h-full" className="h-full" >
                        <p>Yogyakarta merupakan daerah yang kuat dengan industri kreatifnya. Kegiatan ini merupakan salah bentuk dukungan kami untuk terus menumbuhkan Industri Kreatif yang inovatif.z</p>
                    </UMKMItem>
                    <UMKMItem title="CV. Jepara Abadi" area="Kabupaten Bogor" category="Kerajinan Furnitur Kayu" image="/images/kayu-jati.jpg" className="h-full" >
                        <p>CV. Jepara Abadi tumbuh meramaikan industri lokal dalam bidang furnitur dan kerajinan kayu lainnya. Berdiri sejak tahun 1989.</p>
                    </UMKMItem>
                    <UMKMItem title="Batik Yogyakarta" area="DIY Yogyakarta" category="Tas, Dompet, Jaket, Kemeja" image="/images/batik.jpg">
                        <p>Yogyakarta merupakan daerah yang kuat dengan industri kreatifnya. Kegiatan ini merupakan salah bentuk dukungan kami untuk terus menumbuhkan Industri Kreatif </p>
                    </UMKMItem>
                    <UMKMItem title="Jerawood Craft" area="Kabupaten Bogor" category="Kerajinan Kayu" image="/images/kayu-jati.jpg" className="h-full" >
                        <p>Pemilik usaha Jerawood Craft. UMKM ini berdiri sejak 2019. Jerawood Craft merupakan usaha aneka kerajinan limbah kayu yang disulap menjadi produk bernilai jual tinggi.</p>
                    </UMKMItem>
                    <UMKMItem title="Puspita Batik Indigo Natural" area="DIY Yogyakarta" category="Tas, Dompet, Jaket, Kemeja, Dress, Kain" image="/images/batik.jpg" className="h-full" className="h-full" >
                        <p>Yogyakarta merupakan daerah yang kuat dengan industri kreatifnya. Kegiatan ini merupakan salah bentuk dukungan kami untuk terus menumbuhkan Industri Kreatif yang inovatif.z</p>
                    </UMKMItem>
                    <UMKMItem title="CV. Jepara Abadi" area="Kabupaten Bogor" category="Kerajinan Furnitur Kayu" image="/images/kayu-jati.jpg" className="h-full" >
                        <p>CV. Jepara Abadi tumbuh meramaikan industri lokal dalam bidang furnitur dan kerajinan kayu lainnya. Berdiri sejak tahun 1989.</p>
                    </UMKMItem>
                    <UMKMItem title="Batik Yogyakarta" area="DIY Yogyakarta" category="Tas, Dompet, Jaket, Kemeja" image="/images/batik.jpg">
                        <p>Yogyakarta merupakan daerah yang kuat dengan industri kreatifnya. Kegiatan ini merupakan salah bentuk dukungan kami untuk terus menumbuhkan Industri Kreatif </p>
                    </UMKMItem>
                    <UMKMItem title="Jerawood Craft" area="Kabupaten Bogor" category="Kerajinan Kayu" image="/images/kayu-jati.jpg" className="h-full" >
                        <p>Pemilik usaha Jerawood Craft. UMKM ini berdiri sejak 2019. Jerawood Craft merupakan usaha aneka kerajinan limbah kayu yang disulap menjadi produk bernilai jual tinggi.</p>
                    </UMKMItem>
                    <UMKMItem title="Puspita Batik Indigo Natural" area="DIY Yogyakarta" category="Tas, Dompet, Jaket, Kemeja, Dress, Kain" image="/images/batik.jpg" className="h-full" className="h-full" >
                        <p>Yogyakarta merupakan daerah yang kuat dengan industri kreatifnya. Kegiatan ini merupakan salah bentuk dukungan kami untuk terus menumbuhkan Industri Kreatif yang inovatif.z</p>
                    </UMKMItem>
                    <UMKMItem title="CV. Jepara Abadi" area="Kabupaten Bogor" category="Kerajinan Furnitur Kayu" image="/images/kayu-jati.jpg" className="h-full" >
                        <p>CV. Jepara Abadi tumbuh meramaikan industri lokal dalam bidang furnitur dan kerajinan kayu lainnya. Berdiri sejak tahun 1989.</p>
                    </UMKMItem>
                    <UMKMItem title="Batik Yogyakarta" area="DIY Yogyakarta" category="Tas, Dompet, Jaket, Kemeja" image="/images/batik.jpg">
                        <p>Yogyakarta merupakan daerah yang kuat dengan industri kreatifnya. Kegiatan ini merupakan salah bentuk dukungan kami untuk terus menumbuhkan Industri Kreatif </p>
                    </UMKMItem>
                    <UMKMItem title="Jerawood Craft" area="Kabupaten Bogor" category="Kerajinan Kayu" image="/images/kayu-jati.jpg" className="h-full" >
                        <p>Pemilik usaha Jerawood Craft. UMKM ini berdiri sejak 2019. Jerawood Craft merupakan usaha aneka kerajinan limbah kayu yang disulap menjadi produk bernilai jual tinggi.</p>
                    </UMKMItem>
                    <UMKMItem title="Puspita Batik Indigo Natural" area="DIY Yogyakarta" category="Tas, Dompet, Jaket, Kemeja, Dress, Kain" image="/images/batik.jpg" className="h-full" className="h-full" >
                        <p>Yogyakarta merupakan daerah yang kuat dengan industri kreatifnya. Kegiatan ini merupakan salah bentuk dukungan kami untuk terus menumbuhkan Industri Kreatif yang inovatif.z</p>
                    </UMKMItem>
                    <UMKMItem title="CV. Jepara Abadi" area="Kabupaten Bogor" category="Kerajinan Furnitur Kayu" image="/images/kayu-jati.jpg" className="h-full" >
                        <p>CV. Jepara Abadi tumbuh meramaikan industri lokal dalam bidang furnitur dan kerajinan kayu lainnya. Berdiri sejak tahun 1989.</p>
                    </UMKMItem>
                    <UMKMItem title="Batik Yogyakarta" area="DIY Yogyakarta" category="Tas, Dompet, Jaket, Kemeja" image="/images/batik.jpg">
                        <p>Yogyakarta merupakan daerah yang kuat dengan industri kreatifnya. Kegiatan ini merupakan salah bentuk dukungan kami untuk terus menumbuhkan Industri Kreatif </p>
                    </UMKMItem>
                </div>
            </ResponsiveContainer>
        </section>
    )
}

export default DaftarUMKM
