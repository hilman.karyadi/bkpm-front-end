import React, { useState, useContext } from 'react'
import { Dropdown, DropdownItem, Badge } from '@windmill/react-ui'
import styled from 'styled-components'
import UmkmItem from 'components/Page/DaftarUMKM/UMKMItem'

import ResponsiveContainer from 'components/UI/ResponsiveContainer'
import ArrowButton from 'components/UI/ArrowButton'
import UmkmContext from './Context'
import dataUmkm from 'components/store/dataUmkm'

const Search = ({ searchTerm, setSearchTerm, searchHandler }) => {

    const ctx = useContext(UmkmContext)
    const [bidangUsahaVisible, setBidangUsahaVisible] = useState(false)
    const [lokasiUsahaVisible, setLokasiUsahaVisible] = useState(false)
    const [kabupatenUsahaVisible, setKabupatenUsahaVisible] = useState(false)
    
    const searchInputHandler = e => {
        setSearchTerm(e.target.value)
    }

    return (
        <>
        <section className="py-12">
            <ResponsiveContainer>
                <div className="flex flex-col">
                    <div className="grid grid-cols-1 gap-6 lg:grid-cols-4 ">
                        <div>
                            <input type="text" className="py-4 px-8 rounded-full bg-white w-full text-left border-gray-100 shadow-lg" placeholder="Search" value={ searchTerm } onChange={ searchInputHandler }/>
                        </div>
                        <div className="relative">
                            <button type="text" className="py-4 px-8 rounded-full bg-white w-full text-left border-gray-100 shadow-lg" onClick={ () => setLokasiUsahaVisible(prevState => !prevState ) } >Provinsi</button>
                            <Dropdown isOpen={lokasiUsahaVisible} onClose={() => {}} className="left-1/2 transform -translate-x-1/2">
                                <DropdownItem>Jawa Barat</DropdownItem>
                                <DropdownItem>Jawa Tengah</DropdownItem>
                                <DropdownItem>Jawa Timur</DropdownItem>
                            </Dropdown>    
                        </div>
                        <div className="relative">
                            <button type="text" className="py-4 px-8 rounded-full bg-white w-full text-left border-gray-100 shadow-lg" onClick={ () => setKabupatenUsahaVisible(prevState => !prevState ) } >Kabupaten</button>
                            <Dropdown isOpen={kabupatenUsahaVisible} onClose={() => {}} className="left-1/2 transform -translate-x-1/2">
                                <DropdownItem>Bandung Barat</DropdownItem>
                                <DropdownItem>Bandung</DropdownItem>
                                <DropdownItem>Bekasi</DropdownItem>
                            </Dropdown>    
                        </div>
                        <div className="relative">
                            <button type="text" className="py-4 px-8 rounded-full bg-white w-full text-left border-gray-100 shadow-lg" onClick={ () => setBidangUsahaVisible(prevState => !prevState ) } >Bidang Usaha</button>
                            <Dropdown isOpen={bidangUsahaVisible} onClose={() => {}} className="left-1/2 transform -translate-x-1/2">
                                <DropdownItem>15111 : Industri Pengawetan Kulit</DropdownItem>
                                <DropdownItem>15112 : Industri Penyamakan Kulit</DropdownItem>
                                <DropdownItem>15113 : Industri Pencelupan Kulit Bulu</DropdownItem>
                                <DropdownItem>15114 : Industri Kulit Komposisi</DropdownItem>
                            </Dropdown>
                        </div>
                        <div className="lg:col-start-2 lg:col-end-4 lg:text-center">
                            <ArrowButton buttonType="expanded" buttonClick={ searchHandler }>Cari</ArrowButton>
                        </div>
                    </div>
                </div>
            </ResponsiveContainer>
        </section>
        </>
    )}

export default Search

