import React from 'react'
import styled from 'styled-components'

const DataItem = ({ label, value}) => {
    return (
        <div className="mb-4">
            <h6 className="mb-0 text-bkpm-orange-02">{ label }</h6>
            <p className="text-sm">{ value }</p>
        </div>
    )
}

const DataUmkm = () => {
    return (
        <DataContainer>
            <DataItem label="Alamat" value="Jl. Jeruk Tim. III, Sanden, Kramat Sel., Kec. Magelang Utara, Kota Magelang, Jawa Tengah 56115" />
            <DataItem label="Kabupaten/Provinsi" value="Jawa Tengah" />
            <DataItem label="Kontak Person" value="Tubagus Ismail" />
            <DataItem label="KBLI" value={ <>16105: Industri Partikel Kayu &amp; Sejenisnya<br />16104: Industri Pengolahan Rotan</> } />
            <DataItem label="No Telepon" value="021 - 8901666" />
            <DataItem label="No Fax" value="021 - 8901777" />
            <DataItem label="E-mail" value="jerawood.craft@gmail.com" />
        </DataContainer>
    )
}

const DataContainer = styled.div`
    @media (min-width: 768px) {
        column-count: 2;
        column-gap: 2rem;
        & > * {
            break-inside: avoid;
        }
    }
`

export default DataUmkm
