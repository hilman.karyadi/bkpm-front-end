import React, { useContext } from 'react'
import GoogleMapReact from 'google-map-react'

import UmkmContext from './Context'

const Map = () => {

    const ctx = useContext(UmkmContext)

    return (
        <div className="w-full h-96">
            <GoogleMapReact
                bootstrapURLKeys={{ key: 'AIzaSyD-U9NkKKhshvKgZRAM4MzlPk45HeLGIZ8' }}
                defaultCenter={{
                    lat: ctx.map.location.lat,
                    lng: ctx.map.location.lng
                }}
                defaultZoom={12}
            >
                <div className="w-3 h-3 rounded-full bg-red-700"  lat={ ctx.map.location.lat } lng={ ctx.map.location.lng }></div>
            </GoogleMapReact>
        </div>
    )
}

export default Map
