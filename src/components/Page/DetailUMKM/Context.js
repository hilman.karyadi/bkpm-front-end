import React, { useState } from 'react'

const UmkmContext = React.createContext({
    currentTab: '',
    setCurrentTab: () => {},
    map: {}
})

const UmkmContextProvider = ({ children }) => {

    const [currentTab, setCurrentTab] = useState('galeri')
    
    return (
        <UmkmContext.Provider value={{
            currentTab: currentTab,
            setCurrentTab: setCurrentTab,
            map: {
                location: {
                    lat: -7.4521266,
                    lng: 110.2219053
                }
            }
        }} >
            { children }
        </UmkmContext.Provider>
    )
}

export default UmkmContext

export { UmkmContextProvider }