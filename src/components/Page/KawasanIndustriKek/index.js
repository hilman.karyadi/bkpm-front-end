import React from 'react'
import { StaticImage } from 'gatsby-plugin-image'

const KawasanIndustriKek = () => {
    return (
        <div className="py-12"><StaticImage src="../../images/page-kek-2.jpg" layout="constrained" /></div>
    )}

export default KawasanIndustriKek
