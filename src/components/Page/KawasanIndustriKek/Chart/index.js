import React from 'react'
import { ResponsivePie } from '@nivo/pie'

import ResponsiveContainer from 'components/UI/ResponsiveContainer'

const data = [
    {
        id: 'total-sold',
        label: 'Total Sold',
        value: 918,
        color: '#C70007'
    },
    {
        id: 'total-available',
        label: 'Total Available',
        value: 1400,
        color: '#00C850'
    },
    {
        id: 'total-not-for-sale',
        label: 'Total Not for Sale',
        value: 1100,
        color: '#BDBDBD'
    },
    {
        id: 'total-future-development',
        label: 'Total Future Development',
        value: 922,
        color: '#0094C9'
    },
]

const totalLoc = data.reduce( (prev, curr) => prev + curr.value, 0)

const Chart = () => {
    return (
        <section className="py-12">
            <ResponsiveContainer>
                <hr className="h-0.5 bg-bkpm-dark-blue-01 w-12 rounded-full border-0 mb-2" />
                <h4 className="text-bkpm-dark-blue-01 mb-12">Kawasan Industri &amp; KEK</h4>
                <div className="h-96 w-full">
                    <ResponsivePie
                        data={data}
                        colors={{ datum: 'data.color' }}
                        margin={{ top: 40, right: 80, bottom: 80, left: 80 }}
                        sortByValue={false}
                        cornerRadius={3}
                        activeOuterRadiusOffset={8}
                        borderWidth={1}
                        borderColor={{ from: 'color', modifiers: [ [ 'darker', 0.2 ] ] }}
                        arcLinkLabel={ e => `${e.label}: ${ ( parseFloat(e.value / totalLoc ) * 100).toFixed(2) }%`}
                        arcLinkLabelsSkipAngle={10}
                        arcLinkLabelsTextColor={{ from: 'color' }}
                        arcLinkLabelsThickness={2}
                        arcLinkLabelsColor={{ from: 'color' }}
                        enableArcLabels={false}
                        arcLabelsSkipAngle={10}
                        arcLabelsTextColor={{ from: 'color', modifiers: [ [ 'darker', 2 ] ] }}
                        legends={[]}
                    />
                </div>
                <div className="flex justify-center flex-wrap text-xs">
                    <div className="flex mx-2">
                        <div className="mr-2 w-4 h-4 rounded-full" style={ {background: '#C70007'} }></div>
                        <div>Total Sold</div>
                    </div>
                    <div className="flex mx-2">
                        <div className="mr-2 w-4 h-4 rounded-full" style={ {background: '#00C850'} }></div>
                        <div>Total Available</div>
                    </div>
                    <div className="flex mx-2">
                        <div className="mr-2 w-4 h-4 rounded-full" style={ {background: '#BDBDBD'} }></div>
                        <div>Total Not for Sale</div>
                    </div>
                    <div className="flex mx-2">
                        <div className="mr-2 w-4 h-4 rounded-full" style={ {background: '#0094C9'} }></div>
                        <div>Total Future Development</div>
                    </div>
                </div>
            </ResponsiveContainer>
        </section>
    )
}

export default Chart
