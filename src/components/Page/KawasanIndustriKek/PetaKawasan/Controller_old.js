import React, { useState } from 'react'
import styled from 'styled-components'
import { FaCaretDown } from 'react-icons/fa'
import { Dropdown, DropdownItem } from '@windmill/react-ui'

const Controller = () => {
    const [selectorOpen, setSelectorOpen] = useState(false)
    return (
        <div className="border-b-2 border-solid border-gray-200 mb-4 relative">
            <Button className="py-2 px-4 border-b-2 border-solid border-bkpm-dark-blue-01 text-bkpm-dark-blue-01 font-bold" onClick={() => setSelectorOpen( prevState => !prevState )}>Kawasan Industri dan Blok Kawasan&emsp;<FaCaretDown className="inline" /></Button>
            <Dropdown isOpen={selectorOpen} onClose={() => {}} className="z-30 left-6 whitespace-nowrap w-auto">
                <DropdownItem>Kawasan Industri dan Blok Kawasan</DropdownItem>
                <DropdownItem>Kawasan Industri</DropdownItem>
                <DropdownItem>Kawasan Ekonomi Khusus</DropdownItem>
            </Dropdown>
        </div>
    )
}

const Button = styled.button`
    margin-bottom: -2px;
`

export default Controller
