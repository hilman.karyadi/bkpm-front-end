import React, { useState, useEffect, useLayoutEffect } from 'react'
import DataTable from 'react-data-table-component'
import axios from "axios";
import { navigate } from 'gatsby'
import GoogleMapReact from 'google-map-react';
import Fade from 'react-reveal/Fade'

import ResponsiveContainer from 'components/UI/ResponsiveContainer'
//import kawasanIndustriKek from 'components/store/kawasan'

import Controller from './Controller'
import Accordion from './Accordion'

const content = (title,details) => {
    //console.log("data details",details);
    return <Accordion title={title} details={details} />
}

const customStyles = {
    headCells: {
        style: {
            fontWeight: 'bold',
            justifyContent: 'center',
        },
    },
}

const PetaKawasan = ({ keyword, setKeyword}) => {
    const [kawasanIndustriKek, setkawasanIndustriKek] = useState({KawasanBlok:[],KawasanIndustri:[],KawasanIndustriKek:[]});
    const [kawasanId, setKawasanId] = useState(2);
    useEffect(() => {
        axios({
            method: 'GET',
            url: "https://admin.regionalinvestment.bkpm.go.id:81/backend/public/index.php/api/FrontEnd/GetInfrastrukturKek",
            responseType: 'json'
        })
        .then(function (response) {
            setkawasanIndustriKek(response.data.Data.kawasan);
        }).catch((response_error) => {
        
        })
        
        
    },[kawasanIndustriKek]);



    const [columnName, setColumnName] = useState('Kawasan Industri dan Blok Kawasan')
    const [dataBlok, setDataBlok] = useState(kawasanIndustriKek.KawasanBlok.map( item => {
        return { ...item,
            content: content( item.name, item.details )
        }
    }))
    const [dataKawasan, setDataKawasan] = useState(kawasanIndustriKek.KawasanIndustri.map( item => {
        return { ...item,
            content: content( item.name, item.details )
        }
    }))
    const [dataKek, setDataKek] = useState(kawasanIndustriKek.KawasanIndustriKek.map( item => {
        return { ...item,
            content: content( item.name, item.details )
        }
    }))

    const columns = [
        {
            name: columnName,
            selector: row => row.content,
            sortable: true
        }
    ];
    useEffect(() => {
        setKeyword('')
        switch (kawasanId) {
            case 3:
                setColumnName('Kawasan Ekonomi Khusus')
                setDataKek(kawasanIndustriKek.KawasanIndustriKek.map( item => {
                    return { ...item,
                        content: content( item.name, item.details )
                    }
                }))
                break
            case 2:
                setColumnName('Kawasan Industri')
                setDataKawasan(kawasanIndustriKek.KawasanIndustri.map( item => {
                    return { ...item,
                        content: content( item.name, item.details )
                    }
                }))
                break
            case 1:
            default:
                setColumnName('Kawasan Industri dan Blok Kawasan')
                setDataBlok(kawasanIndustriKek.KawasanBlok.map( item => {
                    return { ...item,
                        content: content( item.name, item.details )
                    }
                }))
                break
        }
    }, [kawasanId])

    useEffect(() => {        
        switch (kawasanId) {
            case 3:
                setDataKek(kawasanIndustriKek.KawasanIndustriKek.map( item => {
                    return { ...item,
                        content: content( item.name, item.details )
                    }
                }).filter( item => item.name.toLowerCase().includes(keyword.toLowerCase()) ))
                break
            case 2:
                setDataKawasan(kawasanIndustriKek.KawasanIndustri.map( item => {
                    return { ...item,
                        content: content( item.name, item.details )
                    }
                }).filter( item => item.name.toLowerCase().includes(keyword.toLowerCase()) ))
                break
            case 1:
            default:
                setDataBlok(kawasanIndustriKek.KawasanBlok.map( item => {
                    return { ...item,
                        content: content( item.name, item.details )
                    }
                }).filter( item => item.name.toLowerCase().includes(keyword.toLowerCase()) ))
                break
        }
    }, [keyword, setKeyword])

    const searchInputHandler = e => {
        setKeyword(e.target.value)
    }

    return (
        <section className="py-12">
            <ResponsiveContainer>
                <div className="grid gap-6 grid-cols-1 lg:grid-cols-2">
                    <div>
                        <Controller kawasanId={ kawasanId } setKawasanId={ setKawasanId }/>
                        <div className="flex justify-end space-x-4 items-center">
                            <label>Cari</label><input type="text" placeholder="Kata kunci" className="border-gray-200 border-solid border rounded-md px-2 py-1 placeholder-gray-200" onChange={ searchInputHandler } value={ keyword } />
                        </div>

                        <Fade spy={ kawasanId } >
                            <>
                                { kawasanId === 1 &&
                                    <DataTable
                                    columns={columns}
                                    data={dataBlok}
                                    pagination
                                    customStyles={customStyles}
                                    />
                                }
                                { kawasanId === 2 &&
                                    <DataTable
                                    columns={columns}
                                    data={dataKawasan}
                                    pagination
                                    customStyles={customStyles}
                                    />
                                }
                                { kawasanId === 3 &&
                                    <DataTable
                                    columns={columns}
                                    data={dataKek}
                                    pagination
                                    customStyles={customStyles}
                                    />
                                }
                            </>
                        </Fade>
                    </div>
                    <div className="h-80 lg:h-96">
                        <GoogleMapReact
                            bootstrapURLKeys={{ key: 'AIzaSyD-U9NkKKhshvKgZRAM4MzlPk45HeLGIZ8' }}
                            defaultCenter={{
                                lat: 0.7893,
                                lng: 113.9213
                            }}
                            defaultZoom={5}
                        >
                        </GoogleMapReact>
                    </div>
                </div>
            </ResponsiveContainer>
        </section>
    )
}

export default PetaKawasan
