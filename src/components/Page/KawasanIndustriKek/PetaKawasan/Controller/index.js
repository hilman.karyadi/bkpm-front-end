import React from 'react'

import Item from './Item'

const Controller = ({ kawasanId, setKawasanId}) => {
    return (
        <div className="mb-8">
            <div className="border-b-2 border-gray-200 border-solid flex">
                <Item className="lg:w-1/3 lg:flex-none" label="Kawasan Industri dan Blok Kawasan" active={ kawasanId === 1} setKawasanId={ () => setKawasanId(1) } />
                <Item className="lg:w-1/3 lg:flex-none" label="Kawasan Industri" active={ kawasanId === 2} setKawasanId={ () => setKawasanId(2) } />
                <Item className="lg:w-1/3 lg:flex-none" label="Kawasan Ekonomi Khusus" active={ kawasanId === 3} setKawasanId={ () => setKawasanId(3) } />
            </div>
        </div>
    )
}

export default Controller
