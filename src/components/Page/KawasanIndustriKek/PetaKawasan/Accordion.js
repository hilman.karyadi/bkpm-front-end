import React, { useRef, useState, useEffect } from 'react'
import { Link } from 'gatsby'
import { FaChevronDown } from 'react-icons/fa'
import styled from 'styled-components'

import KawasanDetailItem from './KawasanDetailItem'

const Accordion = (props) => {
    const accordionContent = useRef()
    const [accordionHeight, setAccordionHeight] = useState(null)
    const [isOpen, setIsOpen] = useState(false)

    useEffect(() => {
        if ( isOpen ) {
            setAccordionHeight( accordionContent.current.scrollHeight)
        } else {
            setAccordionHeight(0)
        }
    })

    const imageStyle = {
        backgroundImage: props.bgImage
    }
    
    return (
        
        <div className={ `overflow-hidden ` }>
            <div className="relative pr-12">
                <button onClick={ () => setIsOpen( prevState => !prevState) } className={ `py-4 pl-6 pr-0 w-full text-left ${ isOpen ? 'font-bold' : '' }` }>{ props.title }</button>
                <div className="absolute right-3 top-1/2 transform -translate-y-1/2"><FaChevronDown className="transition-transform text-2xl text-bkpm-green" style={ accordionHeight ? { transform: 'rotate(180deg)' } :'' } /></div>
            </div>
            <div
                ref={ accordionContent }
                style={ { height: accordionHeight } }
                className="overflow-hidden transition-all"
            >
                <div className="p-6">
                    <div className="grid grid-cols-1 gap-12 md:grid-cols-2">
                        <div>
                            <div className="bg-cover w-full aspect-w-5 aspect-h-3 rounded-2xl bg-gray-100" style={ imageStyle }></div>
                        </div>
                        <div>
                            {
                                props.details?.map((item,index) => {
                                    return(<KawasanDetailItem key={index} label={item.label}>{item.value}</KawasanDetailItem>)
                                })
                            }
                            <Link to="/kawasan-industri" className="uppercase underline text-bkpm-dark-blue-01">Selengkapnya</Link>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Accordion