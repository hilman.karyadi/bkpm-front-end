import React from 'react'

import Item from './Item'

const Controller = ({ infrastrukturId, setInfrastrukturId}) => {
    return (
        <div className="overflow-x-auto overflow-y-hidden mb-8">
            <div className="border-b-2 border-gray-200 border-solid flex">
                <Item label="Bandara" active={ infrastrukturId === 1} clickHandler={ () => setInfrastrukturId(1)} />
                <Item label="Pelabuhan" active={ infrastrukturId === 2} clickHandler={ () => setInfrastrukturId(2)} />
                <Item label="Hotel" active={ infrastrukturId === 3} clickHandler={ () => setInfrastrukturId(3)} />
                <Item label="Pendidikan" active={ infrastrukturId === 4} clickHandler={ () => setInfrastrukturId(4)} />
                <Item label="Rumah Sakit" active={ infrastrukturId === 5} clickHandler={ () => setInfrastrukturId(5)} />
            </div>
        </div>
    )
}

export default Controller
