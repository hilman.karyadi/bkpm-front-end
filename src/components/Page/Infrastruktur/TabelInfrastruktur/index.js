import React, { useState, useEffect } from "react"
import DataTable from "react-data-table-component"
import { navigate } from "gatsby"
import Fade from "react-reveal/Fade"

import ResponsiveContainer from "components/UI/ResponsiveContainer"

import Controller from "./Controller"
import Bandara from "./Content/Bandara"
import Pelabuhan from "./Content/Pelabuhan"
import Hotel from "./Content/Hotel"
import Pendidikan from "./Content/Pendidikan"
import RumahSakit from "./Content/RumahSakit"

//import infrastruktur from "components/store/infrastruktur"
import Accordion from "./Content/Accordion"
import ApiInfr from "./Content/ApiInfr"
import axios from "axios"

const content = details => <Accordion details={details} />

const columns = [
  {
    name: "Nama",
    selector: row => row.content,
    grow: true,
    width: "100%",
    style: {
      width: "100%",
    },
    sortable: true,
  },
]

const customStyles = {
  headCells: {
    style: {
      fontWeight: "bold",
      justifyContent: "center",
    },
  },
}



const TabelInfrastruktur = ({
  infrastrukturId,
  setInfrastrukturId,
  dataBandara,
}) => {

  const [displayedInfrastruktur, setDisplayedInfrastruktur] = useState([])

  const [keyword, setKeyword] = useState("")
  //const [data, setDatas] = useState([])
  const { data, loading } = ApiInfr()

  // await for results
  
  const [infrastruktur, setInfrastruktur] = useState(
    {
      bandaras:[],
      hotel:[],
      pelabuhans:[],
      pendidikans:[],
      rumahsakits:[]
    }
  );
  useEffect(() => {
      axios({
          method: 'GET',
          url: "https://admin.regionalinvestment.bkpm.go.id:81/backend/public/index.php/api/FrontEnd/GetInfrastrukturs",
          responseType: 'json'
      })
      .then(function (response) {
        setInfrastruktur(response.data.Data);
      }).catch((response_error) => {
      
      })
  },[infrastruktur]);

  const bandara = [
    ...infrastruktur?.bandaras.map(item => {
      return {
        id: item.idBandara,
        name: item.namaBandara,
        lng: item.longitude,
        ltd: item.latitude,
        details: [...item.details],
        content: content({
          name: item.namaBandara,
          details: [...item.details],
        }),
      }
    }),
  ]
  
  const pelabuhan = [
    ...infrastruktur?.pelabuhans.map(item => {
      return {
        id: item.idPelabuhan,
        name: item.namaPelabuhan,
        lng: item.longitude,
        ltd: item.latitude,
        details: [...item.details],
        content: content({
          name: item.namaPelabuhan,
          details: [...item.details],
        }),
      }
    }),
  ]
  const hotel = [
    ...infrastruktur?.hotel.map(item => {
      return {
        id: item.idHotel,
        name: item.namaHotel,
        lng: item.longitude,
        ltd: item.latitude,
        details: [...item.details],
        content: content({
          name: item.namaHotel,
          details: [...item.details],
        }),
      }
    }),
  ]
  const pendidikan = [
    ...infrastruktur?.pendidikans.map(item => {
      return {
        id: item.idPendidikan,
        name: item.namaPendidikan,
        lng: item.longitude,
        ltd: item.latitude,
        details: [...item.details],
        content: content({
          name: item.namaPendidikan,
          details: [...item.details],
        }),
      }
    }),
  ]
  const rumahsakit = [
    ...infrastruktur?.rumahsakits.map(item => {
      return {
        id: item.idRumahSakit,
        name: item.namaRumahSakit,
        lng: item.longitude,
        ltd: item.latitude,
        details: [...item.details],
        content: content({
          name: item.namaRumahSakit,
          details: [...item.details],
        }),
      }
    }),
  ]

  useEffect(() => {
    setKeyword("")

    switch (infrastrukturId) {
      case 5:
        setDisplayedInfrastruktur([...rumahsakit])
        break
      case 4:
        setDisplayedInfrastruktur([...pendidikan])
        break
      case 3:
        setDisplayedInfrastruktur([...hotel])
        break
      case 2:
        setDisplayedInfrastruktur([...pelabuhan])
        break
      case 1:
      default:
        setDisplayedInfrastruktur([...bandara])
        break
    }
  }, [infrastrukturId, setInfrastrukturId])

  useEffect(() => {
    switch (infrastrukturId) {
      case 5:
        if (keyword) {
          setDisplayedInfrastruktur(
            rumahsakit.filter(item =>
              item.name.toLowerCase().includes(keyword.toLowerCase())
            )
          )
          break
        }
        break
      case 4:
        if (keyword) {
          setDisplayedInfrastruktur(
            pendidikan.filter(item =>
              item.name.toLowerCase().includes(keyword.toLowerCase())
            )
          )
          break
        }
        break
      case 3:
        if (keyword) {
          setDisplayedInfrastruktur(
            hotel.filter(item =>
              item.name.toLowerCase().includes(keyword.toLowerCase())
            )
          )
          break
        }
        break
      case 2:
        if (keyword) {
          setDisplayedInfrastruktur(
            pelabuhan.filter(item =>
              item.name.toLowerCase().includes(keyword.toLowerCase())
            )
          )
          break
        }
        break
      case 1:
      default:
        if (keyword) {
          setDisplayedInfrastruktur(
            bandara.filter(item =>
              item.name.toLowerCase().includes(keyword.toLowerCase())
            )
          )
          break
        }
        break
    }
  }, [keyword, setKeyword])

  const searchInputHandler = e => {
    setKeyword(e.target.value)
  }

  return (
    <>
      <Controller
        infrastrukturId={infrastrukturId}
        setInfrastrukturId={setInfrastrukturId}
      />
      <Fade spy={infrastrukturId}>
        <div className="flex justify-end space-x-4 items-center">
          <label>Cari</label>
          <input
            type="text"
            placeholder="Kata kunci"
            className="border-gray-200 border-solid border rounded-md px-2 py-1 placeholder-gray-200"
            onChange={searchInputHandler}
            value={keyword}
          />
        </div>
        <DataTable
          columns={columns}
          data={displayedInfrastruktur}
          pagination
          responsive={false}
          customStyles={customStyles}
        />
      </Fade>
    </>
  )
}

export default TabelInfrastruktur
