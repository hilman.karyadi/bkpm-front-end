import React from "react"
import DataTable from "react-data-table-component"

import Accordion from "./Accordion"

const content = title => {
  return <Accordion title={title} type="bandara" />
}

const columns = [
  {
    name: "BandarawWx",
    selector: row => row.name,
    style: {
      textTransform: "uppercase",
    },
    sortable: true,
  },
]

const data = [
  {
    id: 1,
    name: content("Cut Nyak Dhien"),
    location: {
      lat: "",
      lng: "",
    },
  },
  {
    id: 2,
    name: content("Kuala Batu"),
    location: {
      lat: "",
      lng: "",
    },
  },
  {
    id: 3,
    name: content("Lasikin"),
    location: {
      lat: "",
      lng: "",
    },
  },
  {
    id: 4,
    name: content("Maimun Saleh"),
    location: {
      lat: "",
      lng: "",
    },
  },
  {
    id: 5,
    name: content("Malikus Saleh"),
    location: {
      lat: "",
      lng: "",
    },
  },
]

const customStyles = {
  headCells: {
    style: {
      display: "none",
    },
  },
}

const Bandara = () => {
  return (
    <div className="w-full">
      <DataTable
        columns={columns}
        data={data}
        pagination
        customStyles={customStyles}
      />
    </div>
  )
}

export default Bandara
