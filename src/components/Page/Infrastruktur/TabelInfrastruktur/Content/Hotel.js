import React from 'react'
import DataTable from 'react-data-table-component'

import Accordion from './Accordion'

const content = title => {
    return <Accordion title={ title } type="hotel"/>
}

const columns = [
    {
        name: 'Hotel',
        selector: row => row.name,
        style: {
            textTransform: 'uppercase'
        },
        sortable: true
    }
];

const data = [
    {
        id: 1,     
        name: content('Grand Aston Yogyakarta'),
        location: {
            lat: '',
            lng: ''
        }
    },
    {
        id: 2,     
        name: content('Hotel Discovery Kartika Plaza'),
        location: {
            lat: '',
            lng: ''
        },
    },
    {
        id: 3,     
        name: content('Hilton Bandung Hotel'),
        location: {
            lat: '',
            lng: ''
        },
    },
    {
        id: 4,     
        name: content('Elephant Safari Park Lodge'),
        location: {
            lat: '',
            lng: ''
        },
    },
    {
        id: 5,     
        name: content('Tugu Lombok Hotel'),
        location: {
            lat: '',
            lng: ''
        },
    }
]

const customStyles = {
    headCells: {
        style: {
            display: 'none'
        },
    },
}

const Pelabuhan = () => {
    return (
        <div className="w-full">
            <DataTable
                columns={columns}
                data={data}
                pagination
                customStyles={ customStyles}
            />
        </div>
    )
}

export default Pelabuhan
