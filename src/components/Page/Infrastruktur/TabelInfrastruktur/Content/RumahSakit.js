import React from 'react'
import DataTable from 'react-data-table-component'

import Accordion from './Accordion'

const content = title => {
    return <Accordion title={ title } type="rumahsakit"/>
}

const columns = [
    {
        name: 'Rumah Sakit',
        selector: row => row.name,
        style: {
            textTransform: 'uppercase'
        },
        sortable: true
    }
];

const data = [
    {
        id: 1,     
        name: content('RSUD H Damanhuri Barabai'),
        location: {
            lat: '',
            lng: ''
        }
    },
    {
        id: 2,     
        name: content('RS Bhayangkara Ambon'),
        location: {
            lat: '',
            lng: ''
        },
    },
    {
        id: 3,     
        name: content('RSU Bangli'),
        location: {
            lat: '',
            lng: ''
        },
    },
    {
        id: 4,     
        name: content('RS Jiwa Provinsi Bali'),
        location: {
            lat: '',
            lng: ''
        },
    },
    {
        id: 5,     
        name: content('RSU Pambalah Batung'),
        location: {
            lat: '',
            lng: ''
        },
    }
]

const customStyles = {
    headCells: {
        style: {
            display: 'none'
        },
    },
}

const RumahSakit = () => {
    return (
        <div className="w-full">
            <DataTable
                columns={columns}
                data={data}
                pagination
                customStyles={ customStyles}
            />
        </div>
    )
}

export default RumahSakit
