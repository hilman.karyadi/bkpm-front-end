import React from 'react'
import DataTable from 'react-data-table-component'

import Accordion from './Accordion'

const content = title => {
    return <Accordion title={ title } type="pendidikan"/>
}

const columns = [
    {
        name: 'Pendidikan',
        selector: row => row.name,
        style: {
            textTransform: 'uppercase'
        },
        sortable: true
    }
];

const data = [
    {
        id: 2,     
        name: content('Universitas Pembangunan Nasional Veteran Jakarta'),
        location: {
            lat: '',
            lng: ''
        },
    },
    {
        id: 1,     
        name: content('SMA N 2 Kundur'),
        location: {
            lat: '',
            lng: ''
        }
    },
    {
        id: 3,     
        name: content('SMKN Kundur'),
        location: {
            lat: '',
            lng: ''
        },
    },
    {
        id: 4,     
        name: content('SMA N 4 Kundur'),
        location: {
            lat: '',
            lng: ''
        },
    },
    {
        id: 5,     
        name: content('SMA N 3 Kundur'),
        location: {
            lat: '',
            lng: ''
        },
    }
]

const customStyles = {
    headCells: {
        style: {
            display: 'none'
        },
    },
}

const Pendidikan = () => {
    return (
        <div className="w-full">
            <DataTable
                columns={columns}
                data={data}
                pagination
                customStyles={ customStyles}
            />
        </div>
    )
}

export default Pendidikan
