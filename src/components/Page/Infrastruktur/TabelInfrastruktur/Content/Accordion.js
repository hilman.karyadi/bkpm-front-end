import React, { useRef, useState, useEffect } from 'react'
import { Link } from 'gatsby'
import { FaChevronDown } from 'react-icons/fa'

import InfrastrukturDetailItem from './InfrastrukturDetailItem'

const Accordion = ({ details }) => {

    const accordionContent = useRef()
    const [accordionHeight, setAccordionHeight] = useState(null)
    const [isOpen, setIsOpen] = useState(false)

    useEffect(() => {
        if ( isOpen ) {
            setAccordionHeight( accordionContent.current.scrollHeight)
        } else {
            setAccordionHeight(0)
        }
    })
    
    return (
        
        <div className={ `w-full overflow-hidden ` }>
            <div className="relative pr-12">
                <button onClick={ () => setIsOpen( prevState => !prevState) } className={ `py-4 pl-6 pr-0 w-full text-left whitespace-normal ${ isOpen ? 'font-bold' : '' }` }>{ details.name }</button>
                <div className="absolute right-3 top-1/2 transform -translate-y-1/2"><FaChevronDown className="transition-transform text-2xl text-bkpm-green" style={ accordionHeight ? { transform: 'rotate(180deg)' } :'' } onClick={ () => setIsOpen( prevState => !prevState) } role="button" /></div>
            </div>
            <div
                ref={ accordionContent }
                style={ { height: accordionHeight } }
                className="overflow-hidden transition-all"
            >
                <div className="p-6">
                    <div>
                        { details.details.map( item => {
                            return <InfrastrukturDetailItem title={ item.label }>{ item.value }</InfrastrukturDetailItem>
                        }) }
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Accordion