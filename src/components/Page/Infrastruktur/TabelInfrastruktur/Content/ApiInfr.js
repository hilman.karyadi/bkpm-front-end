import { useEffect, useState } from "react"
import axios from "axios"

const ApiInfr = () => {
  const [data, setData] = useState({})
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    const ApiInfr = async () => {
      try {
        const { data: response } = await axios.get(
          "https://admin.regionalinvestment.bkpm.go.id:81/backend/public/index.php/api/FrontEnd/GetInfrastrukturs"
        )
        setData(response.json())
      } catch (error) {
        console.error(error)
      }
      setLoading(false)
    }

    ApiInfr()
  }, [])

  return {
    data,
    loading,
  }
}

export default ApiInfr
