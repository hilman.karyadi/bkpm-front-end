import React from 'react'

const InfrastrukturDetailItem = (props) => {
    return (
        <p className="whitespace-normal"><span className="font-bold">{ props.title }</span>: <span className="text-sm">{ props.children }</span></p>
    )
}

const DetailBandara = () => {
    <>
        <InfrastrukturDetailItem title="Airport Name">ALAS LAUSER</InfrastrukturDetailItem>
        <InfrastrukturDetailItem title="Airport Class">Domestik Airport</InfrastrukturDetailItem>
        <InfrastrukturDetailItem title="Airport Time">00:00 - 00:00 WIB</InfrastrukturDetailItem>
        <InfrastrukturDetailItem title="Airport Airlines">Susi Air</InfrastrukturDetailItem>
        <InfrastrukturDetailItem title="Airport lata">SLY</InfrastrukturDetailItem>
        <InfrastrukturDetailItem title="Distance to the capital">593.33 KM</InfrastrukturDetailItem>
        <InfrastrukturDetailItem title="Airport Address">Aceh Tenggara, Aceh, Indonesia</InfrastrukturDetailItem>
        <InfrastrukturDetailItem title="Airport Phone"></InfrastrukturDetailItem>
    </>
}

const DetailPelabuhan = () => {
    <>
        <InfrastrukturDetailItem title="Seaport Name">Sinabang</InfrastrukturDetailItem>
        <InfrastrukturDetailItem title="Seaport Class">Kelas V</InfrastrukturDetailItem>
        <InfrastrukturDetailItem title="Seaport Function">National</InfrastrukturDetailItem>
        <InfrastrukturDetailItem title="Seaport Information">Pelabuhan Umum tidak diusahakan</InfrastrukturDetailItem>
        <InfrastrukturDetailItem title="District Name">Simeulue</InfrastrukturDetailItem>
        <InfrastrukturDetailItem title="Seaport Address">Jl. Samudra, No. 212, Suka Karya, Simeuleu Tim, Kabupaten Simeuleu, Aceh</InfrastrukturDetailItem>
        <InfrastrukturDetailItem title="Seaport Phone">0650 - 21034</InfrastrukturDetailItem>
    </>
}

const DetailHotel = () => {
    <>
        <InfrastrukturDetailItem title="Hotel Name">Grand Aston YogyakartaR</InfrastrukturDetailItem>
        <InfrastrukturDetailItem title="Hotel Class">Bintang 5</InfrastrukturDetailItem>
        <InfrastrukturDetailItem title="Hotel Information"></InfrastrukturDetailItem>
        <InfrastrukturDetailItem title="Hotel Address">Jl. Urip Sumoharjo No. 37</InfrastrukturDetailItem>
        <InfrastrukturDetailItem title="Hotel Phone">(0274) 566 999</InfrastrukturDetailItem>
    </>
}

const DetailPendidikan = () => {
    <>
        <InfrastrukturDetailItem title="Educational Name">SMA N 2 KUNDUR</InfrastrukturDetailItem>
        <InfrastrukturDetailItem title="Educational Class">Negeri</InfrastrukturDetailItem>
        <InfrastrukturDetailItem title="Education Area">1524 M</InfrastrukturDetailItem>
        <InfrastrukturDetailItem title="Educational Address">Jl. Hang TuahKM, 4 Tanjung Berlian,Tanjung Berlian Kota Kecamatan Kundur Utara Kabupaten Karimun Provinsi Kepulauan Riau</InfrastrukturDetailItem>
        <InfrastrukturDetailItem title="Educational Phone"></InfrastrukturDetailItem>
    </>
}

const DetailRumahSakit = () => {
    <>
        <InfrastrukturDetailItem title="Hospital Name">RSUD H Damanhuri Barabai</InfrastrukturDetailItem>
        <InfrastrukturDetailItem title="Hospital Category">Negeri</InfrastrukturDetailItem>
        <InfrastrukturDetailItem title="Hospital Area">1472 M</InfrastrukturDetailItem>
        <InfrastrukturDetailItem title="Hospital Address">Jl. Murakata No. 4 Barabai</InfrastrukturDetailItem>
        <InfrastrukturDetailItem title="Hospital Phone">0517-41004</InfrastrukturDetailItem>
    </>
}

export default InfrastrukturDetailItem
