import React from 'react'
import DataTable from 'react-data-table-component'

import Accordion from './Accordion'

const content = title => {
    return <Accordion title={ title } type="pelabuhan"/>
}

const columns = [
    {
        name: 'Pelabuhan',
        selector: row => row.name,
        style: {
            textTransform: 'uppercase'
        },
        sortable: true
    }
];

const data = [
    {
        id: 1,     
        name: content('Sinabang'),
        location: {
            lat: '',
            lng: ''
        }
    },
    {
        id: 2,     
        name: content('Singkil'),
        location: {
            lat: '',
            lng: ''
        },
    },
    {
        id: 3,     
        name: content('Tapaktuan'),
        location: {
            lat: '',
            lng: ''
        },
    },
    {
        id: 4,     
        name: content('Umum Meulaboh'),
        location: {
            lat: '',
            lng: ''
        },
    },
    {
        id: 5,     
        name: content('Lhokseumawe'),
        location: {
            lat: '',
            lng: ''
        },
    }
]

const customStyles = {
    headCells: {
        style: {
            display: 'none'
        },
    },
}

const Pelabuhan = () => {
    return (
        <div className="w-full">
            <DataTable
                columns={columns}
                data={data}
                pagination
                customStyles={ customStyles}
            />
        </div>
    )
}

export default Pelabuhan
