import React, { useState } from 'react'

const KawasanContext = React.createContext({
    currentTab: '',
    setCurrentTab: () => {},
    map: {}
})

const KawasanContextProvider = ({ children }) => {

    const [currentTab, setCurrentTab] = useState('galeri')
    
    return (
        <KawasanContext.Provider value={{
            currentTab: currentTab,
            setCurrentTab: setCurrentTab,
            map: {
                location: {
                    lat: -7.4521266,
                    lng: 110.2219053
                }
            }
        }} >
            { children }
        </KawasanContext.Provider>
    )
}

export default KawasanContext

export { KawasanContextProvider }