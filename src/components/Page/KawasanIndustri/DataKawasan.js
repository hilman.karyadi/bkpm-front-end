import React from 'react'
import styled from 'styled-components'

const DataItem = ({ label, value}) => {
    return (
        <div className="mb-4">
            <h6 className="mb-0 text-bkpm-orange-02">{ label }</h6>
            <p className="text-sm">{ value }</p>
        </div>
    )
}

const DataKawasan = () => {
    return (
        <DataContainer>
            <DataItem label="Alamat" value="Jl Raya Trans Heksa, Wanajaya, Kec. Telukjambe Bar., Kabupaten Karawang, Jawa Barat 41361" />
            <DataItem label="Kabupaten/Provinsi" value="Jawa Tengah" />
            <DataItem label="Luas" value="1200 hektar" />
            <DataItem label="Website" value="www.knic.co.id" />
            <DataItem label="Bandara terdekat" value="Bandara Halim Perdana Kusuma" />
            <DataItem label="Pelabuhan terdekat" value="Pelabuhan Tanjung Priuk" />
            <DataItem label="Availibility Block" value="0" />
            <DataItem label="No Telepon" value="021 - 8901666" />
            <DataItem label="No Fax" value="021 - 8901777" />
            <DataItem label="E-mail" value="marketing@kiic.co.id" />
        </DataContainer>
    )
}

const DataContainer = styled.div`
    @media (min-width: 768px) {
        column-count: 2;
        column-gap: 2rem;
        & > * {
            break-inside: avoid;
        }
    }
`

export default DataKawasan
