import React, { useContext } from 'react'
import { Swiper, SwiperSlide } from 'swiper/react'
import 'swiper/css'
import  { StaticImage } from 'gatsby-plugin-image'

import KawasanContext from './Context'

const MediaKawasan = () => {

    const ctx = useContext(KawasanContext)

    return (
        <>
            <div className="flex space-x-8 mx-auto justify-center mb-8">
                <button onClick={ () => ctx.setCurrentTab('galeri') } className={`${ ctx.currentTab === 'galeri' ? 'underline' : '' }`}>Galeri Produk</button>
                <button onClick={ () => ctx.setCurrentTab('video') } className={`${ ctx.currentTab === 'video' ? 'underline' : '' }`}>Video</button>
                <button onClick={ () => ctx.setCurrentTab('brosur') } className={`${ ctx.currentTab === 'brosur' ? 'underline' : '' }`}>Brosur</button>
            </div>
            { ctx.currentTab === 'galeri' &&
            <div className="rounded-lg p-3 bg-bkpm-dark-blue-01 w-10/12 mx-auto">
                <Swiper
                    spaceBetween={0}
                    slidesPerView={1}
                    loop={true}
                >
                    <SwiperSlide>
                        <StaticImage src='../../images/industri-01.jpg' width="700" aspectRatio="1.7" className="rounded-lg" />
                    </SwiperSlide>
                    <SwiperSlide>
                        <StaticImage src='../../images/industri-02.jpg' width="700" aspectRatio="1.7" className="rounded-lg" />
                    </SwiperSlide>
                    <SwiperSlide>
                        <StaticImage src='../../images/industri-03.jpg' width="700" aspectRatio="1.7" className="rounded-lg" />
                    </SwiperSlide>
                </Swiper>
            </div> }
            { ctx.currentTab === 'video' && <><iframe width="100%" height="315" src="https://www.youtube.com/embed/e0XUaheVCyA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></> }
            { ctx.currentTab === 'brosur' && <p className="text-center">Belum ada brosur pada kawasan ini</p> }
        </>
    )
}

export default MediaKawasan
