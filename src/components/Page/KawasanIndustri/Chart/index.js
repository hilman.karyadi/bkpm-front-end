import React from 'react'
import { ResponsivePie } from '@nivo/pie'

import ResponsiveContainer from 'components/UI/ResponsiveContainer'

const data = [
    {
        id: 'unavailable',
        label: 'Unavailable',
        value: 15,
        color: '#C70007'
    },
    {
        id: 'available',
        label: 'Available',
        value: 63,
        color: '#00C850'
    },
    {
        id: 'public-facility',
        label: 'Public Facility',
        value: 15,
        color: '#BDBDBD'
    },
    {
        id: 'for-development',
        label: 'For Development',
        value: 18,
        color: '#0094C9'
    },
]

const Chart = () => {
    return (
        <>
            <div className="h-80 w-full">
                <ResponsivePie
                    data={data}
                    colors={{ datum: 'data.color' }}
                    margin={{ top: 40, right: 80, bottom: 80, left: 80 }}
                    sortByValue={false}
                    cornerRadius={3}
                    activeOuterRadiusOffset={8}
                    borderWidth={1}
                    borderColor={{ from: 'color', modifiers: [ [ 'darker', 0.2 ] ] }}
                    arcLinkLabel={ e => `${e.label}: ${ e.value}`}
                    arcLinkLabelsSkipAngle={10}
                    arcLinkLabelsTextColor={{ from: 'color' }}
                    arcLinkLabelsThickness={2}
                    arcLinkLabelsColor={{ from: 'color' }}
                    enableArcLabels={false}
                    arcLabelsSkipAngle={10}
                    arcLabelsTextColor={{ from: 'color', modifiers: [ [ 'darker', 2 ] ] }}
                    legends={[]}
                />
            </div>
            <div className="flex justify-center flex-wrap text-xs">
                <div className="flex mx-2">
                    <div className="mr-2 w-4 h-4 rounded-full" style={ {background: '#C70007'} }></div>
                    <div>Unavailable</div>
                </div>
                <div className="flex mx-2">
                    <div className="mr-2 w-4 h-4 rounded-full" style={ {background: '#00C850'} }></div>
                    <div>Available</div>
                </div>
                <div className="flex mx-2">
                    <div className="mr-2 w-4 h-4 rounded-full" style={ {background: '#BDBDBD'} }></div>
                    <div>Public Facility</div>
                </div>
                <div className="flex mx-2">
                    <div className="mr-2 w-4 h-4 rounded-full" style={ {background: '#0094C9'} }></div>
                    <div>For Development</div>
                </div>
            </div>
        </>
    )
}

export default Chart
