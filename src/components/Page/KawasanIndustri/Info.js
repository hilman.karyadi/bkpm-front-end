import React from 'react'

const InfoItem = ({ label, value }) => {
    return (
        <div className="mb-4">
            <h6 className="mb-0 text-bkpm-orange-02">{ label }</h6>
            <p className="text-sm">{ value }</p>
        </div>
    )
}

const Info = () => {
    return (
        <>
            <InfoItem label="Alamat" value="Jl Raya Trans Heksa, Wanajaya, Kec. Telukjambe Bar., Kabupaten Karawang, Jawa Barat 41361" />
            <InfoItem label="Kabupaten/Provinsi" value="Jawa Tengah" />
            <InfoItem label="Luas" value="1200 hektar" />
            <InfoItem label="Website" value="www.knic.co.id" />
            <InfoItem label="Bandara terdekat" value="Bandara Halim Perdana Kusuma" />
            <InfoItem label="Pelabuhan terdekat" value="Pelabuhan Tanjung Priuk" />
            <InfoItem label="Availibility Block" value="0" />
        </>
    )
}

export default Info
