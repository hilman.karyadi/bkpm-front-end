import React, { useState, useRef } from 'react'
import { Dropdown, DropdownItem, Badge } from '@windmill/react-ui'
import styled from 'styled-components'

import ResponsiveContainer from 'components/UI/ResponsiveContainer'
import ArrowButton from 'components/UI/ArrowButton'

const Search = () => {
    const [bidangUsahaVisible, setBidangUsahaVisible] = useState(false)
    const [lokasiUsahaVisible, setLokasiUsahaVisible] = useState(false)
    const [kabupatenUsahaVisible, setKabupatenUsahaVisible] = useState(false)
    const [sektorVisible, setSektorVisible] = useState(false)
    const inputPencarian = useRef('')
    return (
        <>
        <div className="py-12">
            <ResponsiveContainer>
                <div className="flex flex-col ">
                    <div className="grid grid-cols-1 gap-6 lg:grid-cols-5 ">
                        <div><input type="text" className="py-4 px-8 rounded-full w-full" placeholder="Pencarian" ref={ inputPencarian } /></div>
                        <div className="relative">
                            <button type="text" className="py-4 px-8 rounded-full bg-white w-full text-left" onClick={ () => setLokasiUsahaVisible(prevState => !prevState ) } >Provinsi</button>
                            <Dropdown isOpen={lokasiUsahaVisible} onClose={() => {}} className="left-1/2 transform -translate-x-1/2">
                                <DropdownItem>Jawa Barat</DropdownItem>
                                <DropdownItem>Jawa Tengah</DropdownItem>
                                <DropdownItem>Jawa Timur</DropdownItem>
                            </Dropdown>    
                        </div>
                        <div className="relative">
                            <button type="text" className="py-4 px-8 rounded-full bg-white w-full text-left" onClick={ () => setKabupatenUsahaVisible(prevState => !prevState ) } >Kabupaten</button>
                            <Dropdown isOpen={kabupatenUsahaVisible} onClose={() => {}} className="left-1/2 transform -translate-x-1/2">
                                <DropdownItem>Bandung Barat</DropdownItem>
                                <DropdownItem>Bandung</DropdownItem>
                                <DropdownItem>Bekasi</DropdownItem>
                            </Dropdown>    
                        </div>
                        <div className="relative">
                            <button type="text" className="py-4 px-8 rounded-full bg-white w-full text-left" onClick={ () => setBidangUsahaVisible(prevState => !prevState ) } >Nilai Investasi</button>
                            <Dropdown isOpen={bidangUsahaVisible} onClose={() => {}} className="left-1/2 transform -translate-x-1/2">
                                <DropdownItem>0-20 Milyar</DropdownItem>
                                <DropdownItem>20-50 Milyar</DropdownItem>
                                <DropdownItem>50-100 Milyar</DropdownItem>
                                <DropdownItem>&gt;100 Milyar</DropdownItem>
                            </Dropdown>
                        </div>
                        <div className="relative">
                            <button type="text" className="py-4 px-8 rounded-full bg-white w-full text-left" onClick={ () => setSektorVisible(prevState => !prevState ) } >Sektor</button>
                            <Dropdown isOpen={sektorVisible} onClose={() => {}} className="left-1/2 transform -translate-x-1/2">
                                <DropdownItem>Industri</DropdownItem>
                                <DropdownItem>Infrastruktur</DropdownItem>
                                <DropdownItem>Smelter</DropdownItem>
                                <DropdownItem>Pariwisata</DropdownItem>
                            </Dropdown>
                        </div>
                        <div className="lg:col-start-2 lg:col-end-5 lg:text-center">
                            <ArrowButton buttonType="expanded" >Cari</ArrowButton>
                        </div>
                    </div>
                </div>
            </ResponsiveContainer>
        </div>
        </>
    )}

export default Search

