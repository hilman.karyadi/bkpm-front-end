import React from 'react'
import { StaticImage } from 'gatsby-plugin-image'
import styled from 'styled-components'
import { Link } from 'gatsby'

import { Card, CardBody, Button, Badge } from '@windmill/react-ui'
import { FiMapPin } from 'react-icons/fi'

const BadgeStyled = styled(Badge)`
    color: #ffffff !important;
`

const PeluangInvestasiItem = (props) => (
    <Card className="shadow-xl">
        <img src={ props.image } alt="" className="w-full rounded-xl" width="500" height="375" />
        <CardBody>
            <h5 className="text-bkpm-dark-blue-01">{ props.title }</h5>
            <p className="text-gray-400 text-xs"><FiMapPin className="inline text-bkpm-dark-blue-01" />&nbsp;&nbsp;{ props.area }</p>
            <BadgeStyled className="text-white bg-bkpm-dark-blue-01 mb-6">{ props.value }</BadgeStyled>
            <div className="text-center">
                <Link to="/peluang" className="rounded-full bg-bkpm-light-blue-01 text-white py-3 px-8 hover:bg-bkpm-light-blue-02">Lihat Detail</Link>
            </div>
        </CardBody>
    </Card>
)

export default PeluangInvestasiItem