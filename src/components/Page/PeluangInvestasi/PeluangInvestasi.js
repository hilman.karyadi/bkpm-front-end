import React from "react"

import ResponsiveContainer from "components/UI/ResponsiveContainer"

import PeluangInvestasiItem from "./PeluangInvestasiItem"

const PeluangInvestasi = () => {
  return (
    <section className="py-12">
      <ResponsiveContainer>
        <h4 className="text-bkpm-dark-blue-01">Daftar Proyek</h4>
        <div className="grid gap-6 grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4">
          <PeluangInvestasiItem
            title="Pengembangan Fasilitas Wisata Pantai Batu Karas"
            area="Kab. Pangandaran"
            value="Rp 20 Miliar"
            image="/images/pantai-batu-karas.jpg"
          />
          <PeluangInvestasiItem
            title="Pengembangan Fasilitas Wisata Geopark Ciletuh"
            area="Sukabumi"
            value="Rp 30 Miliar"
            image="/images/geopark-ciletuh.jpg"
          />
          <PeluangInvestasiItem
            title="Pengembangan Fasilitas Wisata Ciwidey"
            area="Kab. Bandung"
            value="Rp 15 Miliar"
            image="/images/ciwidey.jpg"
          />
          <PeluangInvestasiItem
            title="Pengembangan Fasilitas Wisata Pantai Batu Karas"
            area="Kab. Pangandaran"
            value="Rp 20 Miliar"
            image="/images/pantai-batu-karas.jpg"
          />
          <PeluangInvestasiItem
            title="Pengembangan Fasilitas Wisata Geopark Ciletuh"
            area="Sukabumi"
            value="Rp 30 Miliar"
            image="/images/geopark-ciletuh.jpg"
          />
          <PeluangInvestasiItem
            title="Pengembangan Fasilitas Wisata Ciwidey"
            area="Kab. Bandung"
            value="Rp 15 Miliar"
            image="/images/ciwidey.jpg"
          />
          <PeluangInvestasiItem
            title="Pengembangan Fasilitas Wisata Pantai Batu Karas"
            area="Kab. Pangandaran"
            value="Rp 20 Miliar"
            image="/images/pantai-batu-karas.jpg"
          />
          <PeluangInvestasiItem
            title="Pengembangan Fasilitas Wisata Geopark Ciletuh"
            area="Sukabumi"
            value="Rp 30 Miliar"
            image="/images/geopark-ciletuh.jpg"
          />
          <PeluangInvestasiItem
            title="Pengembangan Fasilitas Wisata Ciwidey"
            area="Kab. Bandung"
            value="Rp 15 Miliar"
            image="/images/ciwidey.jpg"
          />
          <PeluangInvestasiItem
            title="Pengembangan Fasilitas Wisata Pantai Batu Karas"
            area="Kab. Pangandaran"
            value="Rp 20 Miliar"
            image="/images/pantai-batu-karas.jpg"
          />
          <PeluangInvestasiItem
            title="Pengembangan Fasilitas Wisata Geopark Ciletuh"
            area="Sukabumi"
            value="Rp 30 Miliar"
            image="/images/geopark-ciletuh.jpg"
          />
          <PeluangInvestasiItem
            title="Pengembangan Fasilitas Wisata Ciwidey"
            area="Kab. Bandung"
            value="Rp 15 Miliar"
            image="/images/ciwidey.jpg"
          />
        </div>
      </ResponsiveContainer>
    </section>
  )
}

export default PeluangInvestasi
