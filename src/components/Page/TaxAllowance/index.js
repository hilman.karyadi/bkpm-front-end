import React from 'react'
import { StaticImage } from 'gatsby-plugin-image'

const TaxAllowance = () => {
    return (
        <div className="py-12"><StaticImage src="../../images/page-tax-allowance.jpg" layout="constrained" /></div>
    )}

export default TaxAllowance
