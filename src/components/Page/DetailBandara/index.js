import React from 'react'
import { StaticImage } from 'gatsby-plugin-image'

const DetailBandara = () => {
    return (
        <div className="py-12"><StaticImage src="../../images/page-detail-bandara.jpg" className="w-full" /></div>
    )}

export default DetailBandara
