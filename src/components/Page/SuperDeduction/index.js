import React from 'react'
import { StaticImage } from 'gatsby-plugin-image'

const SuperDeduction = () => {
    return (
        <div className="py-12"><StaticImage src="../../images/page-super-deduction.jpg" layout="constrained" /></div>
    )}

export default SuperDeduction
