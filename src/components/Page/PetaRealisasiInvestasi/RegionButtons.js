import React, { useContext } from 'react'
import { navigate } from 'gatsby'
import { DropdownItem } from '@windmill/react-ui'

import ButtonSelector from './ButtonSelector'
import dataIndonesia from 'components/store/dataIndonesia'
import MapContext from 'components/ctx/map-context'

const RegionButtons = ({ region, setRegion }) => {
    const MapCtx = useContext(MapContext)
    return (
        <div className="flex flex-wrap justify-center">
            <ButtonSelector region="Sumatera" clickHandler={ () => region === "Sumatera" ? setRegion(null) : setRegion("Sumatera") } active={ region === "Sumatera" }>
                { dataIndonesia.map(item => {
                    if ( item.region ==='Sumatera') {
                        return (
                            <DropdownItem
                                className="whitespace-nowrap text-xs"
                                onClick={ () => navigate('/daerah') }
                                onMouseEnter={ () => { MapCtx.setMap(item.id, item.name) } }
                                onMouseLeave={ () => { MapCtx.setMap() } }
                            >{ item.name }</DropdownItem>
                        )
                    }
                })}
            </ButtonSelector>
            <ButtonSelector region="Jawa" clickHandler={ () => region === "Jawa" ? setRegion(null) : setRegion("Jawa") } active={ region === "Jawa" }>
            { dataIndonesia.map(item => {
                    if ( item.region ==='Jawa') {
                        return (
                            <DropdownItem
                                className="whitespace-nowrap text-xs"
                                onClick={ () => navigate('/daerah') }
                                onMouseEnter={ () => { MapCtx.setMap(item.id, item.name) } }
                                onMouseLeave={ () => { MapCtx.setMap() } }
                            >{ item.name }</DropdownItem>
                        )
                    }
                })}
            </ButtonSelector>
            <ButtonSelector region="Bali dan Nusa Tenggara" clickHandler={ () => region === "Bali dan Nusa Tenggara" ? setRegion(null) : setRegion("Bali dan Nusa Tenggara") } active={ region === "Bali dan Nusa Tenggara" }>
            { dataIndonesia.map(item => {
                    if ( item.region ==='Bali dan Nusa Tenggara') {
                        return (
                            <DropdownItem
                                className="whitespace-nowrap text-xs"
                                onClick={ () => navigate('/daerah') }
                                onMouseEnter={ () => { MapCtx.setMap(item.id, item.name) } }
                                onMouseLeave={ () => { MapCtx.setMap() } }
                            >{ item.name }</DropdownItem>
                        )
                    }
                })}
            </ButtonSelector>
            <ButtonSelector region="Kalimantan" clickHandler={ () => region === "Kalimantan" ? setRegion(null) : setRegion("Kalimantan") } active={ region === "Kalimantan" }>
            { dataIndonesia.map(item => {
                    if ( item.region ==='Kalimantan') {
                        return (
                            <DropdownItem
                                className="whitespace-nowrap text-xs"
                                onClick={ () => navigate('/daerah') }
                                onMouseEnter={ () => { MapCtx.setMap(item.id, item.name) } }
                                onMouseLeave={ () => { MapCtx.setMap() } }
                            >{ item.name }</DropdownItem>
                        )
                    }
                })}
            </ButtonSelector>
            <ButtonSelector region="Sulawesi" clickHandler={ () => region === "Sulawesi" ? setRegion(null) : setRegion("Sulawesi") } active={ region === "Sulawesi" } >
            { dataIndonesia.map(item => {
                    if ( item.region ==='Sulawesi') {
                        return (
                            <DropdownItem
                                className="whitespace-nowrap text-xs"
                                onClick={ () => navigate('/daerah') }
                                onMouseEnter={ () => { MapCtx.setMap(item.id, item.name) } }
                                onMouseLeave={ () => { MapCtx.setMap() } }
                            >{ item.name }</DropdownItem>
                        )
                    }
                })}
            </ButtonSelector>
            <ButtonSelector region="Maluku" clickHandler={ () => region === "Maluku" ? setRegion(null) : setRegion("Maluku") } active={ region === "Maluku" } >
            { dataIndonesia.map(item => {
                    if ( item.region ==='Maluku') {
                        return (
                            <DropdownItem
                                className="whitespace-nowrap text-xs"
                                onClick={ () => navigate('/daerah') }
                                onMouseEnter={ () => { MapCtx.setMap(item.id, item.name) } }
                                onMouseLeave={ () => { MapCtx.setMap() } }
                            >{ item.name }</DropdownItem>
                        )
                    }
                })}
            </ButtonSelector>
            <ButtonSelector region="Papua" clickHandler={ () => region === "Papua" ? setRegion(null) : setRegion("Papua") } active={ region === "Papua" } >
            { dataIndonesia.map(item => {
                    if ( item.region ==='Papua') {
                        return (
                            <DropdownItem
                                className="whitespace-nowrap text-xs"
                                onClick={ () => navigate('/daerah') }
                                onMouseEnter={ () => { MapCtx.setMap(item.id, item.name) } }
                                onMouseLeave={ () => { MapCtx.setMap() } }
                            >{ item.name }</DropdownItem>
                        )
                    }
                })}
            </ButtonSelector>
        </div>
    )
}

export default RegionButtons
