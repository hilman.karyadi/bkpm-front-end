import React, { useState } from 'react'
import { Card, CardBody } from '@windmill/react-ui'
import Fade from 'react-reveal/Fade'
import Modal from 'react-modal'

import ResponsiveContainer from 'components/UI/ResponsiveContainer'
import ArrowButton from 'components/UI/ArrowButton'

import BarChart from './BarChart'

Modal.setAppElement('#___gatsby')

const Chart = (props) => {

    const [modalIsOpen, setModalIsOpen] = useState(false)
    return (
        <>
            <Modal
                isOpen={modalIsOpen}
                onRequestClose={() => setModalIsOpen(false)}
                overlayClassName="inset-0 bg-opacity-90 bg-black z-50 fixed flex items-center justify-center "
                className="p-12 rounded-xl bg-white"
                style={{content: { maxWidth: '650px'}}}
                contentLabel="Chart Data"
            >
                <table classnName="border-collapse">
                    <thead className="bg-bkpm-light-blue-01 text-white">
                        <tr>
                            <th className="text-right py-1 px-2 border border-bkpm-light-blue-02 text-xs">Provinsi</th>
                            <th className="text-right py-1 px-2 border border-bkpm-light-blue-02 text-xs">PMA (dalam ribuan)</th>
                            <th className="text-right py-1 px-2 border border-bkpm-light-blue-02 text-xs">PMDA (dalam jutaan)</th>
                        </tr>
                    </thead>
                    <tbody>
                        {props.data.map( item => {
                            return (
                                <tr>
                                    <th className="text-right py-1 px-2 border border-bkpm-light-blue-02 text-xs">{ item.name }</th>
                                    <td className="text-right py-1 px-2 border border-bkpm-light-blue-02 text-xs">US$ { item.pma.toLocaleString('id-ID') }</td>
                                    <td className="text-right py-1 px-2 border border-bkpm-light-blue-02 text-xs">Rp { item.pmdn.toLocaleString('id-ID') }</td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </Modal>
            <section className="py-12">
                <ResponsiveContainer>
                    <h2 className="text-bkpm-green text-center mb-8">Realisasi <span className="font-normal text-black">Investasi Provinsi</span></h2>
                    <Fade bottom spy={ props.data }>
                    <Card className="rounded-2xl shadow-xl">
                        <CardBody>
                            <div className="h-80 mb-8">
                                <BarChart chartData={ props.data } />
                            </div>
                            <div className="flex justify-between">
                                <div className="flex text-2xs">
                                    <div className="mr-12 flex items-center">
                                        <div className="h-4 w-4 mr-3 items-center" style={ { background: 'rgb(93, 98, 181)' } } ></div>
                                        <span>PMA (dalam ribuan)</span>
                                    </div>
                                    <div className="flex items-center">
                                        <div className="h-4 w-4 mr-3 flex items-center" style={ { background: 'rgb(41, 195, 190)' } } ></div>
                                        <span>PMDN (dalam jutaan)</span>
                                    </div>
                                </div>
                                <div>
                                    <ArrowButton buttonClick={ () => setModalIsOpen(true) }>Data</ArrowButton>
                                </div>
                            </div>
                        </CardBody>
                    </Card>
                    </Fade>
                </ResponsiveContainer>
            </section>
        </>
    )
}

export default Chart
