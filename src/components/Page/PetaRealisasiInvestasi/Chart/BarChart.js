import React from 'react'
import { ResponsiveBar } from '@nivo/bar'

const BarChart = (props) => {

    const colors = {
        pma: 'rgb(93, 98, 181)',
        pmdn: 'rgb(41, 195, 190)',
    }
    const getColor = bar => colors[bar.id]
    return (
        <ResponsiveBar
            data={ props.chartData }
            keys={['pma','pmdn']}
            indexBy="name"
            margin={{ top: 0, right: 50, bottom: 50, left: 100 }}
            layout="vertical"
            padding={0.6}
            groupMode="grouped"
            valueScale={{ type: 'linear' }}
            indexScale={{ type: 'band', round: true }}
            valueFormat={{ enabled: true }}
            colors={getColor}
            axisTop={null}
            axisRight={null}
            axisBottom={{
                tickSize: 5,
                tickPadding: 5,
                tickRotation: 0,
                format: value => value.toLocaleString('id-ID'),
                tickValues: 6,
            }}
            axisLeft={{
                tickSize: 0,
                tickPadding: 5,
                tickRotation: 0,
                tickValues: 4,
                format: value => value.toLocaleString('id-ID')
            }}
            theme={{
                grid: {
                  line: {
                    stroke: "#f1f1f1",
                    strokeWidth: 1,
                  }
                }
              }}
            enableLabel={false}
            tooltip={(input) => {
                return (
                <div className="px-3 py-2 text-xs border border-solid border-gray-500 bg-white rounded-lg">
                  <p className="mb-0"><span className="uppercase">{ input.id }</span> { input.indexValue }: <strong>{ input.id === 'pma' ? 'US$' : 'Rp' } { input.value.toLocaleString('id-ID') }</strong></p>
                </div>
            )}}
        />
    )
}

export default BarChart
