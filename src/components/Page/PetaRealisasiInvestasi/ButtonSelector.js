import React from 'react'
import { Card, Dropdown, Button, DropdownItem } from '@windmill/react-ui'
import { FaCaretDown } from 'react-icons/fa'

const ButtonSelector = ({clickHandler, region, children, active}) => {
    return (
        <div className="mx-3 mb-3 ">
            <Button className="bg-gradient-to-r from-bkpm-dark-blue-01 to-bkpm-green rounded-full px-4 pr-8 py-2 relative text-2xs" onClick={ clickHandler }>{ region }<FaCaretDown className="absolute right-4 top-1/2 transform -translate-y-1/2" /></Button>
            <div className="relative z-10" >
                <Dropdown isOpen={ active } onClose={() => {}} className="overflow-y-auto">
                    { children }
                </Dropdown>
            </div>
        </div>
    )
}

export default ButtonSelector
