import React from 'react'

import ResponsiveContainer from 'components/UI/ResponsiveContainer'
import dataIndonesia from 'components/store/dataIndonesia'

import SVGMap from './SVGMap'

const Map = (props) => {
    return (
        <section className="py-12">
            <ResponsiveContainer>
                <SVGMap map={ dataIndonesia } activePropinsi={ props.activeRegion } setActiveRegion={ props.setActiveRegion }/>
            </ResponsiveContainer>
        </section>
    )
}

export default Map
