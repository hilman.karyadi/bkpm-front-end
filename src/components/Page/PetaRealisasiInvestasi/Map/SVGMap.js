import React, { useState, useContext } from 'react'
import styled from 'styled-components'
import { navigate } from 'gatsby'
import MouseTooltip from 'react-sticky-mouse-tooltip'

import MapContext from 'components/ctx/map-context'

const SVGMap = (props) => {

    const MapCtx = useContext(MapContext)

    const [hoveredPropinsi, setHoveredPropinsi] = useState(null)
    const [hoveredPropinsiName, setHoveredPropinsiName] = useState(null)
    const [showMapToolTip, setShowMapToolTip] = useState(false)

    return (
        <>
            <svg
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 29 793 288"
                aria-label="Map of Indonesia"
            >
                { props.map.map( item => {
                    return (
                        <ProvincePath
                            key={ item.id }
                            id={ item.id }
                            name={ item.name }
                            d={ item.path }
                            $isHovered={ item.id === MapCtx.mapId }
                            onMouseEnter={ () => { MapCtx.setMap(item.id, item.name); setShowMapToolTip(true) } }
                            onMouseLeave={ () => { MapCtx.setMap(); setShowMapToolTip(false) } }
                            onClick={() => navigate('/daerah')/*props.setActiveRegion(item.region)*/ }
                            role="button"
                        />
                    )
                })}
            </svg>
            <MouseTooltip
                visible={ showMapToolTip }
                offsetX={15}
                offsetY={10}
                >
                <div className="bg-gray-200 rounded-md p-2 text-xs">{ MapCtx.mapName }</div>
            </MouseTooltip>
        </>
    )
}

const ProvincePath = styled.path`
    fill: ${
        e => {
            return e.$isHovered ? 'rgb(var(--light-blue-02))' : 'rgb(var(--light-blue-01))'
        }
    };
    stroke: white;
    stroke-width: 0.5px;
`

export default SVGMap