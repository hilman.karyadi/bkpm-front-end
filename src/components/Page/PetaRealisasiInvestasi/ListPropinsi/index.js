import React, { useState, useEffect } from 'react'
import axios from "axios";
import ResponsiveContainer from 'components/UI/ResponsiveContainer'
import PropinsiBadge from './PropinsiBadge'

const ListPropinsi = () => {
    const [dataRegionalDaerah, setDataRegionalDaerah] = useState(null);
    useEffect(() => {
        axios({
            method: 'GET',
            url: "https://admin.regionalinvestment.bkpm.go.id:81/backend/public/index.php/api/FrontEnd/GetDaerah",
            responseType: 'json'
        })
        .then(function (response) {
            setDataRegionalDaerah(response.data.Data.regional.daerah);
        }).catch((response_error) => {
        
        })
    },[dataRegionalDaerah]);
    return (
        <section className="py-12">
            <ResponsiveContainer>

                {/* { dataRegionalDaerah != null ?
                    dataRegionalDaerah.map((itemKategori,indexKategori) => {
                    return (<div key={indexKategori} className="mb-16">
                        <h2 className="text-bkpm-dark-blue-01 mb-8">itemKategori.region</h2>
                        <div class="grid gap-3 grid-cols-1 sm:grid-cols-2 md:grid-cols-4 xl:grid-cols-5">
                            { 
                                dataRegionalDaerah.map((itemData,indexData) => {
                                    if(itemData.region == itemKategori.region){
                                        return(<PropinsiBadge key={indexData} data={itemData} />);
                                    }
                                   
                                }) 
                            }
                        </div>
                    </div>)
                    }
                } */}

                <div className="mb-16">
                    <h2 className="text-bkpm-dark-blue-01 mb-8">Jawa</h2>
                    <div class="grid gap-3 grid-cols-1 sm:grid-cols-2 md:grid-cols-4 xl:grid-cols-5">
                        <PropinsiBadge image="/images/logo-jakarta.png" link="/daerah" title="DKI Jakarta" />
                        <PropinsiBadge image="/images/logo-banten.png" link="/daerah" title="Banten" />
                        <PropinsiBadge image="/images/logo-jawa-barat.png" link="/daerah" title="Jawa Barat" />
                        <PropinsiBadge image="/images/logo-jawa-tengah.png" link="/daerah" title="Jawa Tengah" />
                        <PropinsiBadge image="/images/logo-diy.png" link="/daerah" title="Daerah Istimewa Yogyakarta" />
                        <PropinsiBadge image="/images/logo-jawa-timur.png" link="/daerah" title="Jawa Timur" />
                    </div>
                </div> 
                <div className="mb-16">
                    <h2 className="text-bkpm-dark-blue-01 mb-8">Sumatera</h2>
                    <div class="grid gap-3 grid-cols-1 sm:grid-cols-2 md:grid-cols-4 xl:grid-cols-5">
                        <PropinsiBadge image="/images/aceh-logo.png" link="/daerah" title="Aceh" />
                        <PropinsiBadge image="/images/bangka-belitung-logo.png" link="/daerah" title="Bangka Belitung" />
                        <PropinsiBadge image="/images/bengkulu-logo.png" link="/daerah" title="Bengkulu" />
                        <PropinsiBadge image="/images/jambi-logo.png" link="/daerah" title="Jambi" />
                        <PropinsiBadge image="/images/kepulauan-riau-logo.png" link="/daerah" title="Kepulauan Riau" />
                        <PropinsiBadge image="/images/lampung-logo.png" link="/daerah" title="Lampung" />
                        <PropinsiBadge image="/images/riau-logo.png" link="/daerah" title="Riau" />
                        <PropinsiBadge image="/images/sumatera-barat-logo.png" link="/daerah" title="Sumatera Barat" />
                        <PropinsiBadge image="/images/sumatera-selatan-logo.png" link="/daerah" title="Sumatera Selatan" />
                        <PropinsiBadge image="/images/sumatera-utara-logo.png" link="/daerah" title="Sumatera Utara" />
                    </div>
                </div>
                <div className="mb-16">
                    <h2 className="text-bkpm-dark-blue-01 mb-8">Kalimantan</h2>
                    <div class="grid gap-3 grid-cols-1 sm:grid-cols-2 md:grid-cols-4 xl:grid-cols-5">
                        <PropinsiBadge image="/images/kalimantan-barat-logo.png" link="/daerah" title="Kalimantan Barat" />
                        <PropinsiBadge image="/images/kalimantan-selatan-logo.png" link="/daerah" title="Kalimantan Selatan" />
                        <PropinsiBadge image="/images/kalimantan-tengah-logo.png" link="/daerah" title="Kalimantan Tengah" />
                        <PropinsiBadge image="/images/kalimantan-timur-logo.png" link="/daerah" title="Kalimantan Timur" />
                        <PropinsiBadge image="/images/kalimantan-utara-logo.png" link="/daerah" title="Kalimantan Utara" />
                    </div>
                </div>
                <div className="mb-16">
                    <h2 className="text-bkpm-dark-blue-01 mb-8">Sulawesi</h2>
                    <div class="grid gap-3 grid-cols-1 sm:grid-cols-2 md:grid-cols-4 xl:grid-cols-5">
                        <PropinsiBadge image="/images/gorontalo-logo.png" link="/daerah" title="Gorontalo" />
                        <PropinsiBadge image="/images/sulawesi-barat-logo.png" link="/daerah" title="Sulawesi Barat" />
                        <PropinsiBadge image="/images/sulawesi-selatan-logo.png" link="/daerah" title="Sulawesi Selatan" />
                        <PropinsiBadge image="/images/sulawesi-tengah-logo.png" link="/daerah" title="Sulawesi Tengah" />
                        <PropinsiBadge image="/images/sulawesi-tenggara-logo.png" link="/daerah" title="Sulawesi Tenggara" />
                        <PropinsiBadge image="/images/sulawesi-utara-logo.png" link="/daerah" title="Sulawesi Utara" />
                    </div>
                </div>
                <div className="mb-16">
                    <h2 className="text-bkpm-dark-blue-01 mb-8">Bali dan Nusa Tenggara</h2>
                    <div class="grid gap-3 grid-cols-1 sm:grid-cols-2 md:grid-cols-4 xl:grid-cols-5">
                        <PropinsiBadge image="/images/bali-logo.png" link="/daerah" title="Bali" />
                        <PropinsiBadge image="/images/nusa-tenggara-barat-logo.png" link="/daerah" title="Nusa Tenggara Barat" />
                        <PropinsiBadge image="/images/nusa-tenggara-timur-logo.png" link="/daerah" title="Nusa Tenggara Timur" />
                    </div>
                </div>
                <div className="mb-16">
                    <h2 className="text-bkpm-dark-blue-01 mb-8">Maluku</h2>
                    <div class="grid gap-3 grid-cols-1 sm:grid-cols-2 md:grid-cols-4 xl:grid-cols-5">
                        <PropinsiBadge image="/images/maluku-logo.png" link="/daerah" title="Maluku" />
                        <PropinsiBadge image="/images/maluku-utara-logo.png" link="/daerah" title="Maluku Utara" />
                    </div>
                </div>
                <div className="mb-16">
                    <h2 className="text-bkpm-dark-blue-01 mb-8">Papua</h2>
                    <div class="grid gap-3 grid-cols-1 sm:grid-cols-2 md:grid-cols-4 xl:grid-cols-5">
                        <PropinsiBadge image="/images/papua-logo.png" link="/daerah" title="Papua" />
                        <PropinsiBadge image="/images/papua-barat-logo.png" link="/daerah" title="Papua Barat" />
                    </div>
                </div>
            </ResponsiveContainer>
        </section>
    )
}

export default ListPropinsi
