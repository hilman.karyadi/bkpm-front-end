import React from 'react'
import { navigate } from 'gatsby'

import IcPeluang from 'components/images/ic-peluang.svg'
import IcRealisasi from 'components/images/ic-realisasi.svg'

const PropinsiBadge = ({data}) => {
    return (
        <div className="bg-white rounded-lg shadow-lg p-6 text-center" role="button" onClick={() => navigate("/daerah")}>
            <img src={"/images/logo-jakarta.png"} className="mb-4 h-32 mx-auto" />
            <h5 className="text-bkpm-dark-blue-01">{ data.name }</h5>
            <div className="flex text-2xs text-center w-full justify-between">
                <div className="mr-4">
                    <img src={IcPeluang} className="mx-auto w-8 h-auto mb-2" />
                    <p>
                        <strong className="text-bkpm-dark-blue-01 font-bold">Peluang</strong><br />
                        12.074.083
                    </p>
                </div>
                <div>
                    <img src={IcRealisasi} className="mx-auto w-8 h-auto mb-2" />
                    <p>
                        <strong className="text-bkpm-dark-blue-01 font-bold">Peluang</strong><br />
                        PMA: (US$ ribu) 23.765.277<br />
                        PMDN: (Rp juta) 24.508.885
                    </p>
                </div>
            </div>
        </div>
    )
}

export default PropinsiBadge
