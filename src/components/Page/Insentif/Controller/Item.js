import React from 'react'
import styled from 'styled-components'

const Item = (props) => {
    return (
        <Button className={ `${ props.isActive ? 'active' : '' } text-bkpm-dark-blue-01 leading-loose relative px-4 py-2 font-bold mx-3 whitespace-nowrap` } onClick={ props.clickHandler }>{ props.label }</Button>
    )
}

const Button = styled.button`
    margin-bottom: -2px;
    &:before {
        content: '';
        width: 100%;
        background: transparent;
        position: absolute;
        bottom: 0;
        left: 0;
        height: 2px;
    }
    &:hover, &.active {
        &:before {
            background: rgb(var(--dark-blue-01));
        }
    }
    &:first-child {
        margin-left: 0 !important;
    }
    &:last-child {
        margin-right: 0 !important;
    }
`

export default Item
