import React from 'react'

import Item from './Item'

const Controller = (props) => {
    return (
        <div className="overflow-x-auto overflow-y-hidden mb-8 bg-white z-30">
            <div className="border-b-2 border-gray-200 border-solid flex justify-center">
                <Item label="Tax Holiday" clickHandler={() => props.setTab('taxholiday') } isActive={ props.tab === 'taxholiday' } />
                <Item label="Tax Allowance" clickHandler={() => props.setTab('taxallowance') } isActive={ props.tab === 'taxallowance' } />
                <Item label="Master List" clickHandler={() => props.setTab('masterlist') } isActive={ props.tab === 'masterlist' } />
                <Item label="Super Deduction" clickHandler={() => props.setTab('superdeduction') } isActive={ props.tab === 'superdeduction' } />
            </div>
        </div>
    )
}

export default Controller
