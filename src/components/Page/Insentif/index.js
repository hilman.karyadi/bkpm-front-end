import React from 'react'
import { StaticImage } from 'gatsby-plugin-image'

const Insentif = () => {
    return (
        <div className="py-12"><StaticImage src="../../images/page-insentive-2.jpg" className="w-full" /></div>
    )}

export default Insentif
