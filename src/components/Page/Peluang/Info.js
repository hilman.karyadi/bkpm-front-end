import React from 'react'

const InfoItem = ({ label, value }) => {
    return (
        <div className="mb-4">
            <h6 className="mb-0 text-bkpm-orange-02">{ label }</h6>
            <p className="text-sm">{ value }</p>
        </div>
    )
}

const Info = () => {
    return (
        <>
            <InfoItem label="Year" value="2022" />
            <InfoItem label="Investment Value" value="Rp28 Billion" />
            <InfoItem label="IRR" value="12.68%" />
            <InfoItem label="NPV" value="Rp43.066.000.000" />
            <InfoItem label="Payback Period" value="9.2 Year" />
            <InfoItem label="Source" value="BKPM" />
        </>
    )
}

export default Info
