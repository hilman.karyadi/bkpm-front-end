import React from 'react'

const ContactItem = ({label, value}) => {
    return (
        <tr >
            <td className="pr-12 py-2"><span className="font-bold">{ label }:</span></td>
            <td><span className="font-bold text-gray-500">{ value }</span></td>
        </tr>
    )
}

const ContactDetails = () => {
    return (
        <table>
            <ContactItem label="Kontak" value="DPMPTSP" />
            <ContactItem label="Alamat" value="Raja Ampat, Papua Barat" />
            <ContactItem label="Telepon" value="087213412455" />
            <ContactItem label="Email" value="mail@gmail.com" />
            <ContactItem label="Website" value="www.domain.com" />
        </table>
    )
}

export default ContactDetails
