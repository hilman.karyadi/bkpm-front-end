import React, { useState } from "react"
import { navigate } from "gatsby"
import Fade from "react-reveal/Fade"
import Sticky from "react-sticky-el"

import ResponsiveContainer from "components/UI/ResponsiveContainer"
import ArrowButton from "components/UI/ArrowButton"

import Controller from "./Controller"
import Content from "./Content"

const KawasanIndustri = ({ dataKawasanIndustri }) => {
  const [kawasanId, setKawasanId] = useState(1)
  const [kawasanListIndex, setKawasanListIndex] = useState(0)

  return (
    <section className="section--kawasan py-12">
      <Sticky
        boundaryElement=".section--kawasan"
        topOffset={-70}
        stickyStyle={{ transform: "translateY(70px)", zIndex: 10 }}
      >
        <div className="bg-white pt-4">
          <Controller
            kawasanId={kawasanId}
            setKawasanId={setKawasanId}
            setKawasanListIndex={setKawasanListIndex}
          />
        </div>
      </Sticky>
      <ResponsiveContainer className="relative z-0">
        <Fade spy={kawasanId}>
          <Content
            dataKawasanIndustri={dataKawasanIndustri}
            kawasanId={kawasanId}
            kawasanListIndex={kawasanListIndex}
            setKawasanListIndex={setKawasanListIndex}
          />
        </Fade>
        <div className="mt-8 text-center">
          <ArrowButton
            buttonStyle="expanded"
            buttonClick={() =>
              navigate("/kawasan-industri-kek", { state: { kawasanId } })
            }
          >
            Selengkapnya
          </ArrowButton>
        </div>
      </ResponsiveContainer>
    </section>
  )
}

export default KawasanIndustri
