import React from "react"

import Accordion from "./Accordion"
import kawasan from "components/store/kawasan"

const Content = ({
  kawasanId,
  kawasanListIndex,
  setKawasanListIndex,
  dataKawasanIndustri,
}) => {
  console.log("dataKawasanIndustri",dataKawasanIndustri)

  return (
    <>
      {kawasanId === 1 && (
        <>
          {dataKawasanIndustri &&
            dataKawasanIndustri.KawasanBlok.map((item, index) => {
              if (index < 5) {
                return (
                  <Accordion
                    title={item.name}
                    kawasanId={kawasanId}
                    active={index === kawasanListIndex}
                    key={item.index}
                    setKawasanListIndex={setKawasanListIndex}
                    index={index}
                    kawasanListIndex={kawasanListIndex}
                    details={item.details}
                  />
                )
              }
            })}
          {!dataKawasanIndustri &&
            kawasan.KawasanBlok.map((item, index) => {
              if (index < 5) {
                return (
                  <Accordion
                    title={item.name}
                    kawasanId={kawasanId}
                    active={index === kawasanListIndex}
                    key={item.index}
                    setKawasanListIndex={setKawasanListIndex}
                    index={index}
                    kawasanListIndex={kawasanListIndex}
                  />
                )
              }
            })}
        </>
      )}
      {kawasanId === 2 && (
        <>
          {dataKawasanIndustri &&
            dataKawasanIndustri.KawasanIndustri.map((item, index) => {
              if (index < 5) {
                return (
                  <Accordion
                    title={item.name}
                    kawasanId={kawasanId}
                    active={index === kawasanListIndex}
                    key={item.index}
                    setKawasanListIndex={setKawasanListIndex}
                    index={index}
                    kawasanListIndex={kawasanListIndex}
                  />
                )
              }
            })}
        </>
      )}
      {kawasanId === 3 && (
        <>
          {dataKawasanIndustri &&
            dataKawasanIndustri.kawasanEkonomiKhusus.map((item, index) => {
              if (index < 5) {
                return (
                  <Accordion
                    title={item.name}
                    kawasanId={kawasanId}
                    active={index === kawasanListIndex}
                    key={item.index}
                    setKawasanListIndex={setKawasanListIndex}
                    index={index}
                    kawasanListIndex={kawasanListIndex}
                    details={item.details}
                  />
                )
              }
            })}
          {!dataKawasanIndustri &&
            kawasan.KawasanIndustriKek.map((item, index) => {
              if (index < 5) {
                return (
                  <Accordion
                    title={item.name}
                    kawasanId={kawasanId}
                    active={index === kawasanListIndex}
                    key={item.index}
                    setKawasanListIndex={setKawasanListIndex}
                    index={index}
                    kawasanListIndex={kawasanListIndex}
                  />
                )
              }
            })}
        </>
      )}
    </>
  )
}

export default Content
