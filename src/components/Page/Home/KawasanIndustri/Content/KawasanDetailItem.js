import React from 'react'

const KawasanDetailItem = (props) => {
    return (
        <div className="mb-4">
            <p><span className="font-bold">{ props.label }</span><br />
            <span className="text-sm">{ props.children }</span></p>
        </div>
    )
}

export default KawasanDetailItem
