import React, { useRef, useState, useEffect } from 'react'
import { Link } from 'gatsby'
import { FaChevronDown } from 'react-icons/fa'
import styled from 'styled-components'

import KawasanDetailItem from './KawasanDetailItem'

const Accordion = (props) => {

    const accordionContent = useRef()
    const [accordionHeight, setAccordionHeight] = useState(null)

    useEffect(() => {
        if ( props.active ) {
            setAccordionHeight( accordionContent.current.scrollHeight)
        } else {
            setAccordionHeight(0)
        }
    })

    const imageStyle = {
        backgroundImage: props.bgImage
    }
    
    return (
        
        <div className={ `rounded-2xl overflow-hidden mb-4 ${ props.active ? 'shadow-lg' : '' }` }>
            <div className="relative pr-12 border-b border-gray-200 border-solid">
                <button onClick={ () => { props.setKawasanListIndex( props.index === props.kawasanListIndex ? null : props.index ) } } className={ `py-4 pl-6 pr-0 w-full text-left ${ props.active ? 'font-bold' : '' }` }>{ props.title }</button>
                <div className="absolute right-3 top-1/2 transform -translate-y-1/2"><FaChevronDown className="transition-transform text-2xl text-bkpm-green" style={ accordionHeight ? { transform: 'rotate(180deg)' } :'' } /></div>
            </div>
            <div
                ref={ accordionContent }
                style={ { height: accordionHeight } }
                className="overflow-hidden transition-all"
            >
                <div className="p-6">
                    <div className="grid grid-cols-1 gap-12 md:grid-cols-2">
                        <div>
                            <div className="bg-cover w-full aspect-w-5 aspect-h-3 rounded-2xl bg-gray-100" style={ imageStyle }></div>
                        </div>
                        <DetailContainer>
                            { props.details && props.details.map( item => {
                                return <KawasanDetailItem label={ item.label }>{ item.value }</KawasanDetailItem>
                            }) }
                            { !props.details && <>
                                <KawasanDetailItem label="Alamat">Jl Raya Trans Heksa, Wanajaya, Kec. Telukjambe Bar., Kabupaten Karawang, Jawa Barat 41361</KawasanDetailItem>
                                <KawasanDetailItem label="Kabupaten / Provinsi">Karawang / Jawa Barat</KawasanDetailItem>
                                <KawasanDetailItem label="Luas">205 Hektar</KawasanDetailItem>
                                <KawasanDetailItem label="Website">www.domain.com</KawasanDetailItem>
                                <KawasanDetailItem label="Bandara Terdekat">Bandara Soekarno Hatta</KawasanDetailItem>
                                <KawasanDetailItem label="Pelabuhan Terdekat">Pelabuhan Tanjung Barat</KawasanDetailItem>
                                <KawasanDetailItem label="Availability Block">6</KawasanDetailItem>
                            </> }
                            <Link to="/kawasan-industri" className="uppercase underline text-bkpm-dark-blue-01">Selengkapnya</Link>
                        </DetailContainer>
                    </div>
                </div>
            </div>
        </div>
    )
}

const DetailContainer = styled.div`
    @media (min-width: 768px) {
        column-count: 2;
        column-fill: auto;
        column-gap: 1.5rem;
        &> * {
            break-inside: avoid;
        }
    }
`
export default Accordion