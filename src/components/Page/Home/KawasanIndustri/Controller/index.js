import React from 'react'

import Item from './Item'

const Controller = props => {
    return (
        <div className="overflow-x-auto overflow-y-hidden mb-8">
            <div className="border-b-2 border-gray-200 border-solid flex justify-center">
                <Item label="Kawasan Industri dan Blok Kawasan" kawasanId={ props.kawasanId } id={1}  clickHandler={() => { props.setKawasanId(1); props.setKawasanListIndex(0) }} />
                <Item label="Kawasan Industri" kawasanId={ props.kawasanId } id={2}  clickHandler={() => { props.setKawasanId(2); props.setKawasanListIndex(0) }} />
                <Item label="Kawasan Ekonomi Khusus" kawasanId={ props.kawasanId } id={3}  clickHandler={() => { props.setKawasanId(3); props.setKawasanListIndex(0) }} />
            </div>
        </div>
    )
}

export default Controller
