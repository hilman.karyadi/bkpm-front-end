import React from 'react'
import { StaticImage } from 'gatsby-plugin-image'
import { navigate } from 'gatsby'
import { Fade } from 'react-reveal'

import ResponsiveContainer from 'components/UI/ResponsiveContainer'
import ArrowButton from 'components/UI/ArrowButton'

const InfoKawasan = () => {
    return (
        <section className="bg-gradient-to-r from-bkpm-dark-blue-02 to-bkpm-dark-blue-01">
            <ResponsiveContainer>
                <div className="flex flex-col text-white lg:flex-row lg:items-stretch">
                    <Fade bottom distance="30px">
                    <div className="lg:w-7/12 ">
                        <div className="w-full">
                            <div className="grid grid-cols-1 gap-0.5 text-center md:grid-cols-3">
                                <div className="py-3 px-4 bg-opacity-30 bg-bkpm-light-blue-01">Kawasan Industri dan Blok Kawasan</div>
                                <div className="py-3 px-4 bg-opacity-30 bg-bkpm-light-blue-01">Kawasan Industri</div>
                                <div className="py-3 px-4 bg-opacity-30 bg-bkpm-light-blue-01">Kawasan Ekonomi Khusus</div>
                            </div>
                            <div className="bg-bkpm-orange-02 p-6 shadow-inner">
                                <h3 className="font-normal mb-3">Karawang New Industry City</h3>
                                <h6 className="mb-0">Kabupaten Karawang<br />Avalailable Block : 6</h6>
                            </div>
                            <div className="px-6 py-10">
                                <h5 className="my-3">Greenland International Industrial Center</h5>
                                <hr />
                                <h5 className="my-3">Kawasan Industri Maspion</h5>
                                <hr />
                                <h5 className="my-3">Java Integrated Industrial and Port Estate</h5>
                                <hr />
                                <h5 className="my-3">Batamindo Industrial Estate</h5>
                            </div>
                            <div className="mt-4 text-center">
                                <ArrowButton buttionType="expanded" colorVar="orange" buttonClick={() => navigate('/kawasan-industri-kek')}>SELENGKAPNYA</ArrowButton>
                            </div>
                        </div>
                    </div>
                    </Fade>
                    <Fade bottom distance="30px">
                    <div className="py-12 lg:w-5/12 lg:pl-8">
                        <div className="flex flex-col md:flex-row">
                            <div className="mb-6 md:w-5/12 md:flex-grow md:flex-shrink-0">
                                <h4>KARAWANG NEW INDUSTRY CITY</h4>
                                <StaticImage src="../../images/woodwork.jpg" alt="Sample Image" width={500} aspectRatio={3/5} placeholder="blurred" />
                            </div>
                            <div className="md:w-auto md:ml-6">
                                <h5 className="text-bkpm-orange-02 mb-0">Alamat</h5>
                                <p>Jl Raya Trans Heksa, Wanajaya, Kec. Telukjambe Bar., Kabupaten Karawang, Jawa Barat 41361</p>
                                <h5 className="text-bkpm-orange-02 mb-0">Kabupaten</h5>
                                <p>Karawang/ Jawa Barat</p>
                                <h5 className="text-bkpm-orange-02 mb-0">Luas</h5>
                                <p>205 hektar</p>
                                <h5 className="text-bkpm-orange-02 mb-0">Website</h5>
                                <p>www.loremipsum.com</p>
                                <h5 className="text-bkpm-orange-02 mb-0">Bandara terdekat</h5>
                                <p>Bandara Soekarno Hatta</p>
                                <h5 className="text-bkpm-orange-02 mb-0">Pelabuhan terdekat</h5>
                                <p>Pelabuhan Tanjung Barat</p>
                                <h5 className="text-bkpm-orange-02 mb-0">Availibility Block</h5>
                                <p>6</p>
                                <div className="mt-8">
                                    <ArrowButton buttonStyle="expanded" buttonClick={() => navigate('/kawasan-industri')}>Detail</ArrowButton>
                                </div>
                            </div>
                        </div>
                    </div>
                    </Fade>
                </div>
            </ResponsiveContainer>
        </section>
    )
}

export default InfoKawasan