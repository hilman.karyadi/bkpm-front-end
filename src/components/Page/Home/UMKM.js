import React from 'react'
import { navigate } from 'gatsby'

import ResponsiveContainer from 'components/UI/ResponsiveContainer'
import UMKMSlideItem from 'components/Page/Home/UMKMSlideItem'
import ArrowButton from 'components/UI/ArrowButton'

import { Swiper, SwiperSlide } from 'swiper/react'
import 'swiper/css'
import Fade from 'react-reveal/Fade'

const UMKM = ({ dataUmkm }) => {
    return (
        <div className="bg-bkpm-light-blue-01 py-24">
            <Fade bottom distance="50px">
                <ResponsiveContainer>
                    <h4 className="text-white mb-12">Usaha Mikro, Kecil dan Menengah</h4>
                    <Swiper
                        className="umkm-slider"
                        spaceBetween={24}
                        slidesPerView={1.4}
                        breakpoints={{
                            480: {
                                slidesPerView: 2.3,
                            },
                            786: {
                                slidesPerView: 3.2,
                            },
                            1024: {
                                slidesPerView: 4.2,
                            }
                        }}
                        >
                            <div className="swiper-wrapper items-stretch">
                            {
                                dataUmkm.map((item, index) => {
                                    return (<SwiperSlide key={index}>
                                        <UMKMSlideItem title={item.bidangUsaha} area={item.namaKabupaten} category="" image={item.image} className="h-full" >
                                            <p>{item.bidangUsaha}</p>
                                        </UMKMSlideItem>
                                    </SwiperSlide>)
                                })
                            }
                            </div>
                        </Swiper>
                    <div className="mt-12 text-center"><ArrowButton buttonType="expanded" colorVar="orange" buttonClick={() => navigate('/daftar-umkm')} >Selengkapnya</ArrowButton></div>
                </ResponsiveContainer>
            </Fade>
        </div>
    )
}

export default UMKM