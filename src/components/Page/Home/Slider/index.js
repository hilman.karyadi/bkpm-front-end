import React from "react"
import { navigate } from "gatsby"
import { Swiper, SwiperSlide } from "swiper/react"
import { Pagination } from "swiper"
import "swiper/css"
import "swiper/css/pagination"

import ArrowButton from "components/UI/ArrowButton"
import ResponsiveContainer from "components/UI/ResponsiveContainer"
import bgSlidePir from "components/images/slide-01-bg.png"

import CarouselSlide from "./CarouselSlide"
import data from "./data.slider"

const Slider = ({ dataSlider }) => {
  return (
    <section className="relative">
      <Swiper
        className="hero-slider"
        modules={[Pagination]}
        spaceBetween={0}
        slidesPerView={1}
        loop={true}
        pagination={{
          el: ".custom-pagination",
          dynamicBullets: true,
        }}
      >
        {dataSlider.map((slide, index) => {
          return (
            <SwiperSlide key={index}>
              <CarouselSlide backgroundImage={slide.image}>
                <div className="text-white">
                  <h2 className="mb-4">{slide.title}</h2>
                  <p>{slide.description}</p>
                </div>
                <div className="mt-4">
                  <ArrowButton
                    buttonStyle="expanded"
                    buttonClick={() => navigate(slide.CTAlink)}
                  >
                    {slide.CTAtitle}
                  </ArrowButton>
                </div>
              </CarouselSlide>
            </SwiperSlide>
          )
        })}
        <div className=" absolute bottom-20 w-screen z-10">
          <ResponsiveContainer>
            <div className="custom-pagination"></div>
          </ResponsiveContainer>
        </div>
      </Swiper>
    </section>
  )
}

export default Slider
