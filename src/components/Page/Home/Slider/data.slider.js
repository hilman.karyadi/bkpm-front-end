import bgImage1 from "components/images/slide-investasi.jpg"
import bgImage2 from "components/images/slide-infrastruktur.jpg"
import bgImage3 from "components/images/slide-insentif.jpg"
import bgImage4 from "components/images/slide-umkm.jpg"

const data = [
  {
    bgImage: bgImage1,
    title: "Peluang Investasi",
    description:
      "Peluang Investasi berisikan hasil studi kelayakan (feasibility study) sebagai gambaran perkiraan investasi yang diperlukan dalam mengembangkan suatu daerah sesuai dengan potensi yang dimilikinya. Peluang investasi yang tersedia saat ini berjumlah 23 Proyek di 23 Kabupaten/Kota di seluruh Indonesia yang terdiri dari berbagai sektor.",
    CTAlink: "/peluang-investasi",
    CTAtitle: "Selengkapnya",
  },
  {
    bgImage: bgImage2,
    title: "Kawasan Industri",
    description:
      "Kawasan Industri yang tercatat saat ini berjumlah 63 Kawasan dimana 17 diantaranya telah memiliki block site serta 19 KEK yang tersebar diseluruh wilayah Indonesia didukung dengan penyelenggaraan infrastruktur, pemberian fasilitas dan insentif serta kemudahan berinvestasi.",
    CTAlink: "/kawasan-industri-kek",
    CTAtitle: "Selengkapnya",
  },
  {
    bgImage: bgImage3,
    title: "Kemitraan Usaha Nasional",
    description: `Berdasarkan ketentuan Bidang Usaha sesuai dengan Peraturan Presiden Nomor 49 Tahun 2021 tentang Bidang Usaha Penanaman Modal (Lampiran II) yaitu Kriteria Bidang Usaha Kemitraan dengan Koperasi dan UMKM adalah Bidang Usaha yang terbuka untuk Usaha Besar yang bermitra dengan Koperasi dan UMKM ditetapkan berdasarkan kriteria:
        A. Bidang Usaha yang banyak diusahakan oleh Koperasi dan UMKM; dan/atau
        B.Bidang Usaha yang didorong untuk masuk dalam rantai pasok Usaha Besar.`,
    CTAlink: "/daftar-umkm",
    CTAtitle: "Selengkapnya",
  },
]

export default data
