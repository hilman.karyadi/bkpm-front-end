import React from 'react'
import ResponsiveContainer from 'components/UI/ResponsiveContainer'
import tw from 'tailwind-styled-components'

const CarouselSlide = (props) => {
    return (
        <ContentContainer style={{backgroundImage: `${ !props.plainBg ? 'linear-gradient(0deg, rgba(0,0,0,0.9), rgba(0,0,0,.2)), ' : '' }url('${props.backgroundImage}')`}}>
            <ResponsiveContainer className="h-full">
                <div className="relative h-full w-full">
                    <Content>
                        { props.children }
                    </Content>
                </div>
            </ResponsiveContainer>
        </ContentContainer>
    );
}

const ContentContainer = tw.div`
    h-screen
    relative
    bg-left
    bg-cover
    bg-blend-multiply
`
const Content = tw.div`
    absolute
    left-0
    right-0
    bottom-28
    md:right-auto
    md:w-2/3
    lg:w-1/2
    xl:w-1/3
`
export default CarouselSlide;