import React from 'react'
import { navigate } from 'gatsby'

import ArrowButton from 'components/UI/ArrowButton'

const ButtonProfile = () => {
    return (
        <div className="mb-8">
            <ArrowButton buttonStyle="expanded" colorVar="green" buttonClick={() => navigate('/daerah')}>Detail Profile</ArrowButton>
        </div>
    )
}

export default ButtonProfile
