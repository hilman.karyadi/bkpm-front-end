import React from 'react'
import Fade from 'react-reveal/Fade'

import DataList from './DataList'
import ButtonContact from './ButtonContact'
import ButtonProfile from './ButtonProfile'
import ButtonProvinceSelector from './ButtonProvinceSelector'

const DataCard = (props) => {
    return (
        <div className="relative bg-white rounded-t-lg border border-solid border-gray-100 shadow-xl px-6 py-8 sm:w-3/4 sm:mx-auto md:w-full">
            <span className="block h-0.5 w-12 bg-bkpm-green rounded-full mb-2" ></span>
            <h5 className=" text-bkpm-green mb-8">Regional { props.regionName }</h5>
            <ButtonProvinceSelector activeProvince={ props.activeProvince } province={ props.map } setActiveProvince={ props.setActiveProvince }/>
            <Fade bottom spy={ props.activeProvince } distance="10px" cascade={false}>
                <DataList dataPropinsi={ props.dataPropinsi } />
                <ButtonContact />
                <ButtonProfile />
            </Fade>
        </div>
    )
}

export default DataCard
