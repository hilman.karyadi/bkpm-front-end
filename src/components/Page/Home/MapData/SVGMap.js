import React from 'react'
import styled from 'styled-components'

const SVGMap = (props) => {

    return (
        <svg
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 29 793 288"
            aria-label="Map of Indonesia"
        >
            { props.map.map( item => {
                return (
                    <ProvincePath
                        key={ item.id }
                        id={ item.id }
                        name={ item.name }
                        d={ item.path }
                        $isSelected={ item.id === props.activeProvince }
                        onClick={() => props.setActiveProvince(item.id) }
                        role="button"
                    />
                )
            })}
        </svg>
    )
}

const ProvincePath = styled.path`
    fill: ${ e => e.$isSelected ? 'rgb(var(--green))' : 'rgb(var(--light-blue-01))' };
    stroke: white;
    stroke-width: 0.5px;
    &:hover {
        fill: ${ e => e.$isSelected ? 'rgb(var(--green))' : 'rgb(var(--light-blue-02))' };
    }
`

export default SVGMap