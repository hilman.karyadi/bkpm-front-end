import React, { useState, useEffect } from 'react'

import ResponsiveContainer from 'components/UI/ResponsiveContainer'
import dataIndonesia from 'components/store/dataIndonesia'

import DataCard from './DataCard'
import SVGMap from './SVGMap'

const MapData = ({ dataRegional }) => {
    const [activeProvinceID, setActiveProvinceID] = useState(31)
    const [activeProvince, setActiveProvince] = useState('')
    const [regionName, setRegionName] = useState('')
    const [dataPropinsi, setDataPropinsi] = useState(null)

    //console.log(dataRegional)

    const updateProvince = (id) => {
        setActiveProvinceID(id);
    }

    useEffect(() => {
        const activeProvinceItem = dataIndonesia.find(item => item.id === activeProvinceID)
        setActiveProvince(activeProvinceItem.name)
        setRegionName(activeProvinceItem.region)
        const d = dataRegional.filter( item => item.id === activeProvinceItem.id)
        setDataPropinsi([...d[0].data])
    }, [activeProvinceID])

    return (
        <>
        <section className="overflow-hidden">
            <ResponsiveContainer>
                <div className="grid grid-cols-1 md:grid-cols-5 gap-8 items-center">
                    <div className="md:col-span-2">
                        <DataCard regionName={ regionName } activeProvince={ activeProvince } map={ dataIndonesia } setActiveProvince={ updateProvince } dataPropinsi={ dataPropinsi } />
                    </div>
                    <div className="hidden md:block md:col-span-3">
                        <SVGMap map={ dataIndonesia } className="map-indonesia" activeProvince={ activeProvinceID } setActiveProvince={ updateProvince } />
                    </div>
                </div>
            </ResponsiveContainer>
        </section>
        </>
    )
}

export default MapData
