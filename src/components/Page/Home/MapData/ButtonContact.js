import React, { useState } from 'react'
import Modal from 'react-modal';

import ArrowButton from 'components/UI/ArrowButton'

Modal.setAppElement('#___gatsby')

const ButtonContact = () => {

    const [contactModalIsOpen, setContactModalIsOpen] = useState(false);
    const openModal = () => setContactModalIsOpen(true)
    const closeModal = () => setContactModalIsOpen(false)

    return (
        <>
            <Modal
                isOpen={contactModalIsOpen}
                onRequestClose={closeModal}
                overlayClassName="inset-0 bg-opacity-90 bg-black z-50 fixed flex items-center justify-center "
                className="p-0 w-10/12"
                style={{content: { maxWidth: '650px'}}}
            >
                <div className="bg-white rounded-lg p-8">
                    <h5 className="text-bkpm-dark-blue-01">Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu (DPMPTSP) Provinsi Jawa Barat</h5>

                    <p className="text-bkpm-dark-blue-01">Alamat : Jl. Windu No. 26 Bandung<br />
                    Telepon : 022-73515000<br />
                    Email : dpmptsp.jabarprov.go.id</p>
                </div>
            </Modal>
            <div className="mb-4">
                <ArrowButton buttonStyle="expanded" colorVar="red" buttonClick={openModal}>DPMPTSP</ArrowButton>
            </div>
        </>
    )
}

export default ButtonContact
