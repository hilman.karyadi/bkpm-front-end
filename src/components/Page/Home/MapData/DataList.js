import React from 'react'

import IcJumlahPenduduk from 'components/images/ic-jumlah-penduduk.svg'
import IcKawasanIndustri from 'components/images/ic-kawasan-industri.svg'
import IcPDRB from 'components/images/ic-pdrb.svg'
import IcPeluang from 'components/images/ic-peluang.svg'
import IcRealisasi from 'components/images/ic-realisasi.svg'
import IcUMR from 'components/images/ic-umr.svg'

import ItemPeluang from './ItemPeluang'

const DataList = ({ dataPropinsi }) => {
    
    return (
        <div className="mb-8 grid grid-cols-1 lg:grid-cols-2">
            {dataPropinsi && dataPropinsi.map( item => {
                if (item.label.toLowerCase().includes('peluang')) {
                    return <ItemPeluang icon={IcPeluang} title={ item.label } value={`${ item.value } Proyek`} key="info-peluang" />
                }
                if (item.label.toLowerCase().includes('kawasan')) {
                    return <ItemPeluang icon={IcKawasanIndustri} title={ item.label } value={`${ item.value } Kawasan`}key="info-industri" />
                }
                if (item.label.toLowerCase().includes('umr')) {
                    return <ItemPeluang icon={IcUMR} title={ item.label } value={`Rp ${ item.value }`} key="info-umr" />
                }
                if (item.label.toLowerCase().includes('realisasi')) {
                    return <ItemPeluang icon={IcRealisasi} title={ item.label } value={[`PMA (US$ ribu): ${ item.detail.PMA }`,<br />,`PMDN (Rp juta): ${ item.detail.PMDN }`]} key="info-realisasi" />
                }
                if (item.label.toLowerCase().includes('penduduk')) {
                    return <ItemPeluang icon={IcJumlahPenduduk} title={ item.label } value={`${ item.value } jiwa`} key="info-penduduk" />
                }
                if (item.label.toLowerCase().includes('pdrb')) {
                    return <ItemPeluang icon={IcPDRB} title={ item.label } value={ item.value } key="info-pdrb" />
                }
                return null
            })}
        </div>
    )
}

export default DataList
