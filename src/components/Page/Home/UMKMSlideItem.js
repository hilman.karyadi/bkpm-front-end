import React from 'react'
import { FiMapPin } from 'react-icons/fi'
import { navigate } from 'gatsby'

const UMKMSlideItem = (props) => {
    return (
        <div className={`bg-white rounded-xl ${ props.className }`}>
            <div className="p-4">
                <h6 className="text-bkpm-dark-blue-01 text-base">{ props.title.length > 20 ? `${props.title.substring(0, 20)}...` : props.title }</h6>
                <p className ="text-2xs"><FiMapPin className="inline text-bkpm-light-blue-01" />&nbsp;&nbsp;{ props.area }<br />{ props.category }</p>
            </div>
            <img src={ props.image } alt={ props.imageAlt ? props.imageAlt : ''}  className="w-full" />
            <div className="p-4">
                <h6 className="text-bkpm-dark-blue-01 cursor-pointer" onClick={() => navigate('/detail-umkm')}>Selengkapnya</h6>
            </div>
        </div>
    )
}

export default UMKMSlideItem