import React from 'react'
import Fade from 'react-reveal/Fade'
import { Link } from 'gatsby'

import Accordion from './Accordion'

const accordionContent = [
    { 
        title: 'Tax Holiday',
        text:'Tax holiday menjadi salah satu faktor penentu untuk menarik minat investasi ke berbagai sektor. Tax holiday sendiri merupakan salah satu bentuk insentif pajak kepada pelaku usaha. Bentuknya berupa pengurangan hingga pembebasan pajak penghasilan (PPh) badan hingga dalam jangka waktu tertentu',
        tab: 'taxholiday',
        isOpen: true
    },
    { 
        title: 'Tax Allowance',
        text:'Wajib Pajak badan dalam negeri yang melakukan Penamaan modal baru (Industri padat karya) atau perluasan usaha pada bidang tertentu, tidak mendapatkan fasilitas pasal 31A UU Pajak Penghasilan (tax allowance),  dapat menerima fasilitas pengurangan penghasilan netto 60% dari jumlah penanaman modal berupa aktiva tetap berwujud termasuk',
        tab: 'taxallowance',
        isOpen: false
    },
    { 
        title: 'Master List',
        text:'Masterlist(Fasilitas Penanaman Modal) adalah fasilitas yang meliputi: 1. Pembebasan bea masuk sebagaimana diatur dalam Peraturan Menteri Keuangan Nomor 110/PMK.010/2005 tentang Tata Cara Pemberian Pembebasan dan/atau Keringan Bea Masuk dan Pembebasan dan/atau Penundaan Nilai Atas Impor Barang dalam Rangka',
        tab: 'masterlist',
        isOpen: false
    },
    { 
        title: 'Super Deduction',
        text:'Super Deduction Tax Indonesia merupakan insentif pajak yang diberikan pemerintah pada industri yang terlibat dalam program Pendidikan vokasi, meliputi kegiatan penelitian dan pengembangan untuk menghasilkan inovasi. Pemotongan pajak ini diatur dalam Peraturan Pemerintah No. 45 Tahun 2019, dimana terdapat dua poin insentif yang tercantum di dalamnya yaitu:',
        tab: 'superdeduction',
        isOpen: false
    }
]

const Insentif = ({ accordionState, setAccordionState}) => {

    const clickHandler = (i) => {
        setAccordionState( (prevAccordion) => {
            return prevAccordion.map( (item, index) => {
                let isOpen = false
                if ( index == i ) {
                    isOpen = true
                }                
                return { ...item, isOpen: isOpen}
            })
        })
    }

    return (
        <Fade bottom distance="50px">
            <div>
                <div className="bg-white text-bkpm-dark-blue-01 rounded-lg p-6 shadow-xl border border-solid border-gray-200 md:h-full">
                    <h2 className="mb-8 text-bkpm-green">Insentif</h2>
                    { accordionState.map( (content, index) => {
                        return <Accordion title={ content.title } isOpen={ content.isOpen } key={ index } clickHandler={ () => clickHandler(index) }>{ content.text} ... <Link to="/insentif" state={ { defaultTab: content.tab} }  className="font-bold uppercase">Selengkapnya</Link></Accordion>
                    })}
                </div>
            </div>
        </Fade>
    )
}

export default Insentif
