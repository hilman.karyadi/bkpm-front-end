import React from 'react'
import Fade from 'react-reveal/Fade'

const Description = () => {
    return (
        <Fade bottom distance="30px">
            <div className="my-10">
                <h4 className="text-bkpm-dark-blue-01">Aplikasi PIR</h4>
                <p>PIR merupakan sistem informasi berbasis geospasial untuk potensi dan peluang investasi di 34 Provinsi dan 514 Kabupaten / Kota di Indonesia dan merupakan bagian dari situs Badan Koordinasi Penanaman Modal (BKPM). PIR memuat informasi profil daerah antara lain data demografi, komoditas, pendapatan dan Upah Minimum Regional (UMR), serta infrastruktur pendukung.</p>
            </div>
        </Fade>
    )
}

export default Description
