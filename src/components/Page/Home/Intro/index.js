import React from 'react'

import ResponsiveContainer from 'components/UI/ResponsiveContainer'

import VideoContainer from './VideoContainer'
import Insentif from './Insentif'

const Index = ({ accordionState, setAccordionState }) => {
    
    return (
        <section className="relative z-10">
            <ResponsiveContainer className="pb-16">
                <div className="grid grid-cols-1 gap-8 -mt-12 md:grid-cols-2">
                    <VideoContainer />
                    <Insentif accordionState={ accordionState } setAccordionState={ setAccordionState }/>
                </div>
            </ResponsiveContainer>
        </section>
    )
}

export default Index
