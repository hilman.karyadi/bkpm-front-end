import React from 'react'

import ResponsiveContainer from 'components/UI/ResponsiveContainer'

import PartnerIcon from './PartnerIcon'

const Partner = () => {
    return (
        <section className="py-12" >
            <ResponsiveContainer>
                <h4 className="text-bkpm-dark-blue-01 mb-8">Didukung oleh</h4>
                <div className="flex -mx-6 flex-wrap justify-center w-full">
                    <PartnerIcon image="/images/logo-big.png" label=""  />
                    <PartnerIcon image="/images/logo-atrbpn.png" label="" />
                    <PartnerIcon image="/images/logo-bps.png" label="" />
                    <PartnerIcon image="/images/logo-esdm.png" label="" />
                    <PartnerIcon image="/images/logo-bi.png" label="" />
                    <PartnerIcon image="/images/logo-kemenper.png" label="" />
                    <PartnerIcon image="/images/logo-hkii.png" label="" />
                    <PartnerIcon image="/images/logo-kek.png" label="" />
                </div>
            </ResponsiveContainer>
        </section>
    )
}

export default Partner
