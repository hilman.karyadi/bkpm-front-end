import React from 'react'
import styled from 'styled-components'

const LogoIcon = styled.img`
    max-height: 80px;
`
const LabelContainer = styled.div`
    max-width: 150px;
    text-align: center;
`

const PartnerIcon = props => {
    return (
        <div className="w-1/2 px-6 text-center mb-6 sm:w-auto">
            <div className="mb-3"><LogoIcon src={ props.image } className="w-3/4 mx-auto sm:w-auto" /></div>
            <LabelContainer className="mx-auto"><h6>{ props.label }</h6></LabelContainer>
        </div>
    )
}

export default PartnerIcon
