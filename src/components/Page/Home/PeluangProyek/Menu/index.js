import React, { useEffect, useLayoutEffect, useState } from "react"
import styled from "styled-components"
import Fade from "react-reveal/Fade"
import Sticky from "react-sticky-el"

import BgSwirl from "components/images/bg-swirl.png"
import iconEnergi from "components/images/ic-peluang-menu-smelter.svg"
import IconIndustri from "components/images/ic-peluang-menu-industri.svg"
import IconInfrastruktur from "components/images/ic-peluang-menu-infrastruktur.svg"
import IconSmelter from "components/images/ic-peluang-menu-smelter.svg"
import IconPariwisata from "components/images/ic-peluang-menu-pariwisata.svg"

import Icon from "./Icon"
import index from "../List"

const circularRadius = 350
const padding = 50

const Menu = ({ dataPeluangInvestasi, peluangArea, setPeluangArea }) => {
  const [rotation, setRotation] = useState(88)

  const [slidePeluang, setSlidePeluang] = useState(0)

  const [content, setContent] = useState({
    deskripsi:null,
    nilaiInvestasi:null,
    totalProyek:null
  });
  
  useEffect(async () => {
    try {
      setContent({
        deskripsi:dataPeluangInvestasi[2].deskripsi,
        nilaiInvestasi:dataPeluangInvestasi[2].detail.nilaiInvestasi,
        totalProyek:dataPeluangInvestasi[2].detail.totalProyek
      });
      setPeluangArea(dataPeluangInvestasi[2].title)
    } catch (e) {
        
    }
}, [dataPeluangInvestasi]);


  /// slide auto ///
  setTimeout(function(){ 
    if(dataPeluangInvestasi != null){
      var rotation = 0;
      if(dataPeluangInvestasi[slidePeluang].title == "Industri"){
        rotation = 90;
      } else if(dataPeluangInvestasi[slidePeluang].title == "Infrastruktur"){
        rotation = 0;
      } else if(dataPeluangInvestasi[slidePeluang].title == "Smelter"){
        rotation = -35;
      } else if(dataPeluangInvestasi[slidePeluang].title == "Pariwisata"){
        rotation = -88;
      } else if(dataPeluangInvestasi[slidePeluang].title == "Energi"){
        rotation = 40;
      }

      setRotation(rotation)
      setContent({
        deskripsi:dataPeluangInvestasi[slidePeluang].deskripsi,
        nilaiInvestasi:dataPeluangInvestasi[slidePeluang].detail.nilaiInvestasi,
        totalProyek:dataPeluangInvestasi[slidePeluang].detail.totalProyek
      });
      setPeluangArea(dataPeluangInvestasi[slidePeluang].title)

      var jmlData = dataPeluangInvestasi.length-1;
      if(slidePeluang < jmlData){
        setSlidePeluang(parseInt(slidePeluang)+1)
      } else {
        setSlidePeluang(0)
      }
      console.log("slidePeluang",slidePeluang);
      console.log("dataPeluangInvestasi.length",dataPeluangInvestasi.length);
    }
    
  }, 5000);
  /// slide auto ///

  const style = {
    iconIndustri: {
      position: "absolute",
      right: "100%",
      top: "50%",
      transform: "translate(-50%,-20%)",
    },
    iconEnergi: {
      position: "absolute",
      top: "100%",
      right: "85%",
      transform: "translate(-50%,-20%)",
    },
    iconInfrastruktur: {
      position: "absolute",
      top: "100%",
      left: "10%",
      transform: "translate(50%,40%)",
    },
    iconSmelter: {
      position: "absolute",
      top: "100%",
      left: "95%",
    },
    iconPariwisata: {
      position: "absolute",
      left: "95%",
      top: "50%",
      transform: "translate(50%,-20%)",
    },
  }

  return (
    <SektorPeluangContainer className="sektor-menu-boundary py-12 px-6 rounded-xl shadow-lg my-6 lg:w-full lg:pl-8 lg:pr-0 lg:shadow-none lg:rounded-none lg:my-0">
      <Sticky
        boundaryElement=".sektor-menu-boundary"
        topOffset={-105}
        stickyStyle={{ transform: "translateY(105px)" }}
        bottomOffset={250}
      >
        <h4 className="text-bkpm-dark-blue-01 text-center">
          Peluang Investasi Sektor {peluangArea}
        </h4>
        <Fade bottom distance="30px">
          <div className="w-full min-h-full flex items-start justify-center relative">
            <Circle className="rounded-full bg-bkpm-light-blue-01 text-white relative">
              <i></i>
              <div>
                <h6>{peluangArea}</h6>
                <p>
                 {content.deskripsi}
                </p>
                <div className="inline-grid grid-cols-2 gap-4">
                  <div>
                    <p>
                      Rp { content.nilaiInvestasi }
                      <br />
                      Nilai Investasi
                    </p>
                  </div>
                  <div>
                    <p>
                      {content.totalProyek}
                      <br />
                      Total Proyek
                    </p>
                  </div>
                </div>
              </div>

              { dataPeluangInvestasi != null ?
                dataPeluangInvestasi.map((item, index) => {
                  var IconVector = iconEnergi;
                  var IconStyle = style.iconEnergi;
                  var rotation = 40;
                  if(item.title == "Energi"){
                    IconVector = iconEnergi;
                    IconStyle = style.iconEnergi;
                    rotation = 40;
                  } else if(item.title == "Industri"){
                    IconVector = IconIndustri;
                    IconStyle = style.iconIndustri;
                    rotation = 90;
                  } else if(item.title == "Infrastruktur"){
                    IconVector = IconInfrastruktur;
                    IconStyle = style.iconInfrastruktur;
                    rotation = 0;
                  } else if(item.title == "Pariwisata"){
                    IconVector = IconPariwisata;
                    IconStyle = style.iconPariwisata;
                    rotation = -88;
                  } else if(item.title == "Smelter"){
                    IconVector = IconSmelter;
                    IconStyle = style.iconSmelter;
                    rotation = -35;
                  } 

                  
                  return(<Icon
                    key={index}
                    icon={IconVector}
                    style={IconStyle}
                    label={item.title}
                    clickHandler={() => {
                      setRotation(rotation)
                      setContent({
                        deskripsi:item.deskripsi,
                        nilaiInvestasi:item.detail.nilaiInvestasi,
                        totalProyek:item.detail.totalProyek
                      });
                      setPeluangArea(item.title)
                    }}
                    />);
                }) : null
              }


              {/* <Icon
                icon={IconInfrastruktur}
                style={style.iconInfrastruktur}
                label="Infrastruktur"
                clickHandler={() => {
                  setRotation(35)
                  setPeluangArea("Infrastruktur")
                }}
              />
              <Icon
                icon={IconSmelter}
                style={style.iconSmelter}
                label="Smelter"
                clickHandler={() => {
                  setRotation(-35)
                  setPeluangArea("Smelter")
                }}
              />
              <Icon
                icon={IconPariwisata}
                style={style.iconPariwisata}
                label="Pariwisata"
                clickHandler={() => {
                  setRotation(-88)
                  setPeluangArea("Pariwisata")
                }}
              /> */}

              {/* {dataPeluangInvestasi &&
                dataPeluangInvestasi.map((item, index) => {
                  return (
                    <Icon
                      label={item.title}
                      style={style.iconIndustri}
                      clickHandler={() => {
                        setRotation(88)
                        setPeluangArea(item.title)
                      }}
                    />
                  )
                })} */}
            </Circle>
            <CircleBorder>
              <Rotator $rotation={rotation} className="relative w-full h-full">
                <div class="absolute top-full left-1/2 w-7 h-7 bg-white transform -translate-x-1/2 -translate-y-1/2 rounded-full border-2 border-solid border-bkpm-light-blue-01"></div>
              </Rotator>
            </CircleBorder>
          </div>
        </Fade>
      </Sticky>
    </SektorPeluangContainer>
  )
}

const SektorPeluangContainer = styled.div`
  background: rgb(var(--light-blue-02)) url("${BgSwirl}") left bottom no-repeat;
  background-size: cover;
`
const Circle = styled.div`
  width: ${circularRadius}px;
  height: ${circularRadius}px;
  text-align: center;
  i {
    content: "";
    height: 100%;
    width: 50%;
    float: right;
    shape-outside: radial-gradient(
      farthest-side at left,
      transparent calc(100% - 10px),
      #fff 0
    );
  }
  &::before {
    content: "";
    float: left;
    height: 100%;
    width: 50%;
    shape-outside: radial-gradient(
      farthest-side at right,
      transparent calc(100% - 10px),
      #fff 0
    );
  }
`
const CircleBorder = styled.div`
  background: url("/images/circle.svg") center no-repeat;
  background-size: contain;
  width: ${circularRadius + padding}px;
  height: ${circularRadius + padding}px;
  /*border: 3px solid rgb(var(--light-blue-01));*/
  position: absolute;
  top: 50%;
  left: 50%;
  border-radius: 9999px;
  transform: translate(-50%, -50%);
`
const Rotator = styled.div`
  transition: all linear 0.5s;
  transform: rotate(${e => e.$rotation}deg);
`
export default Menu
