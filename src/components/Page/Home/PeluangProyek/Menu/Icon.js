import React from 'react'

const Icon = (props) => {
    return (
        <div className={`text-center ${ props.className}`} style={ props.style }>
            <button onClick={ () => props.clickHandler(props.menuId) } className="w-16 h-16 group rounded-full bg-bkpm-dark-blue-01 flex items-center justify-center mx-auto mb-2 "><img src={ props.icon } className="w-10 transform -translate-y-px translate-x-px transition-all group-hover:transform group-hover:scale-125" /></button>
            <h6 className="uppercase text-bkpm-dark-blue-01">{ props.label }</h6>
        </div>
    )
}

export default Icon
