import React, { useState } from "react"

import ResponsiveContainer from "components/UI/ResponsiveContainer"

import List from "./List"
import Menu from "./Menu"

const PeluangProyek = ({
  dataPeluangProyek,
  dataPeluangInvestasi,
  dataActivePeluangArea,
}) => {
  const [peluangArea, setPeluangArea] = useState(dataActivePeluangArea)

  return (
    <section className="bg-bkpm-light-blue-02">
      <ResponsiveContainer>
        <div className="flex flex-col items-stretch lg:flex-row lg:items-stretch">
          <List
            peluangArea={peluangArea}
            dataPeluangProyek={dataPeluangProyek}
          />
          <Menu
            dataPeluangInvestasi={dataPeluangInvestasi}
            peluangArea={peluangArea}
            setPeluangArea={setPeluangArea}
          />
        </div>
      </ResponsiveContainer>
    </section>
  )
}

export default PeluangProyek
