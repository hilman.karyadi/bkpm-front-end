import React from 'react'
import { FiMapPin } from 'react-icons/fi'
import { BiCalendar } from 'react-icons/bi'
import { Link } from 'gatsby'

const PeluangProyekItem = (props) => {
    
    return (
        <div>
            <div className="grid grid-cols-1 gap-4 py-6 sm:grid-cols-2 lg:grid-cols-5">
                <div className="">
                    <img src={ props.image } className="rounded-xl" alt="Image of a beach" />
                </div>
                <div className="lg:col-span-4">
                    <h6 className="text-bkpm-dark-blue-01">{ props.title }</h6>
                    <p className="text-xs">
                        <FiMapPin className="inline text-bkpm-light-blue-01" />&nbsp;&nbsp;{ props.area }&emsp;
                        <BiCalendar className="inline text-bkpm-light-blue-01" />&nbsp;&nbsp;{ props.year }
                    </p>
                    <div className="flex items-center w-full text-xs">
                        <div className="flex-shrink flex-grow-0">
                            <p className="text-bkpm-dark-blue-01"><strong>Estimasi Investasi</strong></p>
                        </div>
                        <div className="mx-3 flex-grow">
                            <Link to="/peluang" className="bg-bkpm-light-blue-01 rounded-lg py-2 px-3 w-full text-center text-white font-bold block">{ props.amount }</Link>
                        </div>
                        <div className="flex-shrink flex-grow-0">
                            <Link to="/peluang" className="underline font-bold text-bkpm-dark-blue-01 flex-shrink">Selengkapnya</Link>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default PeluangProyekItem