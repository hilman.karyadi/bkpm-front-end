import React from "react"
import styled from "styled-components"
import Fade from "react-reveal/Fade"
import { navigate } from "gatsby"
import CustomScroll from "react-custom-scroll"

import ArrowButton from "components/UI/ArrowButton"
import BgMountain from "components/images/bg-mountain.png"

import PeluangProyekItem from "./PeluangProyekItem"

const index = ({ peluangArea, dataPeluangProyek }) => {
  return (
    <PeluangProyekContainer className="py-12 px-6 rounded-xl shadow-lg mt-12 mb-8 lg:w-5/12 lg:pr-8 lg:pl-0 lg:flex-shrink-0 lg:shadow-none lg:rounded-none lg:my-0">
      <Fade bottom distance="30px">
        <h4 className="text-bkpm-dark-blue-01">
          Peluang Investasi Sektor {peluangArea}
        </h4>
        <div className="mb-8">
          <div className="overflow-y-scroll h-96 pr-8">
            {
              dataPeluangProyek.map((item, index) => {
                if(item.sektor == peluangArea){
                  return (<PeluangProyekItem
                    key={index}
                    title={item.title}
                    area={item.area}
                    year={item.date}
                    amount={item.value}
                    image={item.image}
                  />)
                }
              })
            }
            
            {/* <PeluangProyekItem
              title="Pengembangan Fasilitas Wisata Geopark Ciletuh"
              area="Kab. Pangandaran"
              year="2019 - 2024"
              amount="Rp 32 Miliar"
              image="/images/beach-02.jpg"
            />
            <PeluangProyekItem
              title="Pengembangan Fasilitas Wisata Pantai Batu Karas"
              area="Kab. Pangandaran"
              year="2019 - 2020"
              amount="Rp 20 Miliar"
              image="/images/beach-01.jpg"
            />
            <PeluangProyekItem
              title="Pengembangan Fasilitas Wisata Geopark Ciletuh"
              area="Kab. Pangandaran"
              year="2019 - 2024"
              amount="Rp 32 Miliar"
              image="/images/beach-02.jpg"
            />
            <PeluangProyekItem
              title="Pengembangan Fasilitas Wisata Pantai Batu Karas"
              area="Kab. Pangandaran"
              year="2019 - 2020"
              amount="Rp 20 Miliar"
              image="/images/beach-01.jpg"
            /> */}
          </div>
        </div>
        <div>
          <ArrowButton
            buttonType="expanded"
            buttonClick={() => navigate("/peluang-investasi")}
          >
            Lihat Semua
          </ArrowButton>
        </div>
      </Fade>
    </PeluangProyekContainer>
  )
}

const PeluangProyekContainer = styled.div`
  background: rgb(var(--light-blue-02)) url("${BgMountain}") left center
    no-repeat;
  background-size: cover;
`

export default index
