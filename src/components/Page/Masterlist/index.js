import React from 'react'
import { StaticImage } from 'gatsby-plugin-image'

const Masterlist = () => {
    return (
        <div className="py-12"><StaticImage src="../../images/page-insentif-masterlist.jpg"  layout="constrained" /></div>
    )}

export default Masterlist
