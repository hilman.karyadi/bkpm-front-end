import React from 'react'
import { Card, CardBody } from '@windmill/react-ui'
import GoogleMapReact from 'google-map-react';
import { Fade } from 'react-reveal'

import ResponsiveContainer from 'components/UI/ResponsiveContainer'
import SVGPPeluang from 'components/images/ic-komoditi-peluang.svg'
import SVGUmkm from 'components/images/ic-komoditi-umkm.svg'
import SVGIndustri from 'components/images/ic-komoditi-industri.svg'
import SVGBandara from 'components/images/ic-komoditi-bandara.svg'
import SVGPelabuhan from 'components/images/ic-komoditi-pelabuhan.svg'
import SVGPendidikan from 'components/images/ic-komoditi-pendidikan.svg'
import SVGHotel from 'components/images/ic-komoditi-hotel.svg'
import SVGRumahSakit from 'components/images/ic-komoditi-rumah-sakit.svg'
import infrastruktur from 'components/store/infrastruktur'
import kawasan from 'components/store/kawasan'

import PetaInteraktifIcon from './PetaInteraktifIcon'

const PetaInteraktif = () => {
    return (
        <section className="py-12">
            <ResponsiveContainer>
                <hr className="h-0.5 bg-bkpm-green w-12 rounded-full border-0 mb-2" />
                <h4 className="text-bkpm-green mb-12">Profil Daerah</h4>
                <div className="flex flex-col lg:flex-row">
                    <div className="mb-8 lg:w-1/4 lg:mb-0 lg:mr-8 lg:flex-grow-0">
                        <Fade bottom distance="30px">
                            <Card className="shadow-xl">
                                <CardBody>
                                    <div className="grid gap-8 grid-cols-2 sm:grid-cols-4 lg:grid-cols-2">
                                        <PetaInteraktifIcon title="Peluang" icon={ SVGPPeluang } value="50" />
                                        <PetaInteraktifIcon title="UMKM" icon={ SVGUmkm } value="84" color="#4C8D10" />
                                        <PetaInteraktifIcon title="Kawasan Industri" icon={ SVGIndustri } value="32" color="#4C8D10" />
                                        <PetaInteraktifIcon title="Bandara" icon={ SVGBandara } value="5" color="#F5B415" />
                                        <PetaInteraktifIcon title="Pelabuhan" icon={ SVGPelabuhan } value="10" color="#FF5C00" />
                                        <PetaInteraktifIcon title="Sarana Pendidikan" icon={ SVGPendidikan } value="175" color="#5196FF" />
                                        <PetaInteraktifIcon title="Hotel" icon={ SVGHotel } value="209" color="#287504" />
                                        <PetaInteraktifIcon title="Rumah Sakit" icon={ SVGRumahSakit } value="298" color="#F09706" />
                                    </div>
                                </CardBody>
                            </Card>
                        </Fade>
                    </div>
                    <div className="lg:flex-grow lg:flex-shrink-0">
                        <Card className="shadow-xl h-screen lg:h-full">
                            <GoogleMapReact
                                bootstrapURLKeys={{ key: 'AIzaSyD-U9NkKKhshvKgZRAM4MzlPk45HeLGIZ8' }}
                                defaultCenter={{
                                    lat: 0.7893,
                                    lng: 113.9213
                                }}
                                defaultZoom={5}
                                options={{
                                    mapTypeId: "satellite"
                                }}
                            >
                                {infrastruktur.pendidikans.map( (item, index) => {
                                    if( index < 50) {
                                        return <img src={ SVGPendidikan } className="w-4 h-auto" lat={ item.latitude } lng={ item.longitude } />
                                    }
                                })}
                                {infrastruktur.rumahsakits.map( (item, index) => {
                                    if( index < 50) {
                                        return <img src={ SVGRumahSakit } className="w-4 h-auto" lat={ item.latitude } lng={ item.longitude } />
                                    }
                                })}
                                {infrastruktur.pelabuhans.map( (item, index) => {
                                    if( index < 50) {
                                        return <img src={ SVGPelabuhan } className="w-4 h-auto" lat={ item.latitude } lng={ item.longitude } />
                                    }
                                })}
                                {infrastruktur.hotel.map( (item, index) => {
                                    if( index < 50) {
                                        return <img src={ SVGHotel } className="w-4 h-auto" lat={ item.latitude } lng={ item.longitude } />
                                    }
                                })}
                                {infrastruktur.bandaras.map( (item, index) => {
                                    if( index < 50) {
                                        return <img src={ SVGBandara } className="w-4 h-auto" lat={ item.latitude } lng={ item.longitude } />
                                    }
                                })}
                            </GoogleMapReact>
                        </Card>
                    </div>
                </div>
            </ResponsiveContainer>
        </section>
    )
}

export default PetaInteraktif