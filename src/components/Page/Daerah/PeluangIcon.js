import React from 'react'
import styled from 'styled-components'

const PeluangIcon = (props) => {

    const ColoredHeading = styled.h6`
        color: ${ props.color ? props.color : 'rgb(var(--dark-blue-01))'};
    `
    return (
        <div className="flex flex-col items-center justify-between text-xs text-center">
            <div>
                <div className="mb-3">
                    <img src={ props.icon } className="h-12 max-w-full mx-auto" />
                </div>
                <p>{ props.title }</p>            
            </div>
            <div>
                <ColoredHeading className="text-xs">{ props.sektorNumber }</ColoredHeading>
            </div>
        </div>
    )
}

export default PeluangIcon