import React, { useState } from 'react'
import { Card, CardBody } from '@windmill/react-ui'
import { Fade } from 'react-reveal'
import { StaticImage } from 'gatsby-plugin-image'
import Modal from 'react-modal'
import styled from 'styled-components'
import scrollTo from 'gatsby-plugin-smoothscroll'

// component import
import ResponsiveContainer from 'components/UI/ResponsiveContainer'
import ArrowButton from 'components/UI/ArrowButton'

// page component import
import PertumbuhanPendudukChart from'./Chart/PertumbuhanPendudukChart'
import KomoditasDaerahChart from'./Chart/KomoditasDaerahChart'
import PdrbChart from'./Chart/PdrbChart'
import UmrChart from'./Chart/UmrChart'
import InvestasiChart from'./Chart/InvestasiChart'
import DataButton from './Chart/DataButton'
import { FaClosedCaptioning } from 'react-icons/fa'

const StickyBar = styled.div`
    top: 75px;
    @media (min-width:1024px) {
        top: 99px;
    }
`

Modal.setAppElement('#___gatsby')

const DetailRegional = () => {

    const [chartID, setChartID] = useState('komoditas-daerah')
    const [chartDataPopup, setChartDataPopup] = useState(false)

    const openChartData = () => setChartDataPopup(true)
    const closeChartData = () => setChartDataPopup(false)

    return (
        <>
            <Modal
                isOpen={chartDataPopup}
                onRequestClose={closeChartData}
                overlayClassName="inset-0 bg-opacity-90 bg-black z-50 fixed flex items-center justify-center "
                className="p-0 w-10/12"
                style={{content: { maxWidth: '650px'}}}
            >
                <div className="bg-white p-12 rounded-lg">
                    <StaticImage src="../../images/chart-data.png" className="w-2/3 mx-auto block" />
                </div>
            </Modal>
            <section>
                <ResponsiveContainer>
                    <hr className="h-0.5 bg-bkpm-green w-12 rounded-full border-0 mb-2" />
                    <h4 className="text-bkpm-green">Regional Detail</h4>
                </ResponsiveContainer>
                <StickyBar className="sticky border-b border-solid border-gray-200 w-full bg-white z-10 hidden lg:block">
                    <div className="flex justify-center">
                        <button onClick={ () => setChartID('komoditas-daerah') } className="text-sm bg-none py-2 px-3  border-b-2 border-solid border-transparent hover:text-bkpm-dark-blue-01 hover:border-bkpm-dark-blue-01">Komoditas Daerah</button>
                        <button onClick={ () => setChartID('pertumbuhan-penduduk') } className="text-sm bg-none py-2 px-3 border-b-2 border-solid border-transparent hover:text-bkpm-dark-blue-01 hover:border-bkpm-dark-blue-01">Pertumbuhan Penduduk</button>
                        <button onClick={ () => setChartID('pdrb') } className="text-sm bg-none py-2 px-3 border-b-2 border-solid border-transparent hover:text-bkpm-dark-blue-01 hover:border-bkpm-dark-blue-01">PDRB</button>
                        <button onClick={ () => setChartID('umr') } className="text-sm bg-none py-2 px-3 border-b-2 border-solid border-transparent hover:text-bkpm-dark-blue-01 hover:border-bkpm-dark-blue-01">UMR</button>
                        <button onClick={ () => setChartID('investasi') } className="text-sm bg-none py-2 px-3 border-b-2 border-solid border-transparent hover:text-bkpm-dark-blue-01 hover:border-bkpm-dark-blue-01">Realisasi Investasi</button>
                    </div>
                </StickyBar>
                <ResponsiveContainer>
                    { chartID === 'komoditas-daerah' &&
                    (
                        <div id="chart-komoditas-daerah" className="pt-6 pb-16">
                            <hr className="h-0.5 bg-bkpm-light-blue-01 w-12 rounded-full border-0 mb-2" />
                            <h4 className="text-bkpm-light-blue-01">Komoditas Daerah</h4>
                            <Fade bottom distance="30px">
                                <Card className="shadow-xl ring-0 ring-opacity-0 ring-transparent">
                                    <CardBody className="flex flex-col justify-center items-stretch">
                                        <div className="w-full text-center mb-12">
                                            <KomoditasDaerahChart />
                                            <div className="flex justify-between w-full">
                                                <div className="inline-grid grid-cols-3 gap-6 text-xs">
                                                    <div className="inline-flex items-center">
                                                        <div className="w-4 h-4 mr-2 flex-none" style={{ backgroundColor: '#0093ED' }}></div>
                                                        <div>Peternakan</div>
                                                    </div>
                                                    <div className="inline-flex items-center">
                                                        <div className="w-4 h-4 mr-2 flex-none" style={{ backgroundColor: '#4C8D10' }}></div>
                                                        <div>Perkebunan</div>
                                                    </div><div className="inline-flex items-center">
                                                        <div className="w-4 h-4 mr-2 flex-none" style={{ backgroundColor: '#053783' }}></div>
                                                        <div>Pertanian</div>
                                                    </div>
                                                </div>
                                                <div>
                                                    <DataButton dataTable="/images/data-komoditas.jpg" />
                                                </div>
                                            </div>
                                        </div>
                                    </CardBody>
                                </Card>
                            </Fade>
                        </div>
                    )}
                    { chartID === 'pertumbuhan-penduduk' &&
                    (
                        <div id="chart-pertumbuhan-penduduk" className="pt-6 pb-16">
                            <hr className="h-0.5 bg-bkpm-light-blue-01 w-12 rounded-full border-0 mb-2" />
                            <h4 className="text-bkpm-light-blue-01">Pertumbuhan Penduduk</h4>
                            <Fade bottom distance="30px">
                                <Card className="shadow-xl ring-0 ring-opacity-0 ring-transparent">
                                    <CardBody className="flec flex-col justify-center items-stretch">
                                        <div className="h-80 w-full">
                                            <PertumbuhanPendudukChart />
                                        </div>
                                        <div className="flex justify-end">
                                            <DataButton dataTable="/images/data-penduduk.jpg" />
                                        </div>
                                    </CardBody>
                                </Card>
                            </Fade>
                        </div>
                    )}
                    { chartID === 'pdrb' &&
                    (
                        <div id="chart-komoditas-daerah" className="pt-6 pb-16">
                            <hr className="h-0.5 bg-bkpm-light-blue-01 w-12 rounded-full border-0 mb-2" />
                            <h4 className="text-bkpm-light-blue-01">PDRB</h4>
                            <Fade bottom distance="30px">
                                <Card className="shadow-xl ring-0 ring-opacity-0 ring-transparent">
                                    <CardBody className="flex flex-col justify-center items-stretch">
                                        <div className="w-full text-center mb-12">
                                            <PdrbChart />
                                            <div className="flex justify-between w-full text-left pr-16">
                                                <div className="flex flex-wrap text-xs">
                                                    <div className="inline-flex items-start mr-6 mb-6">
                                                        <div className="w-4 h-4 mr-2 flex-none flex-none" style={{ backgroundColor: '#0093ED' }}></div>
                                                        <div>Industri Pengolahan</div>
                                                    </div>
                                                    <div className="inline-flex items-start mr-6 mb-6">
                                                        <div className="w-4 h-4 mr-2 flex-none flex-none" style={{ backgroundColor: '#053783' }}></div>
                                                        <div>Perdagangan Besar dan Eceran, dan Reparasi Mobil dan Sepeda Motor</div>
                                                    </div>
                                                    <div className="inline-flex items-start mr-6 mb-6">
                                                        <div className="w-4 h-4 mr-2 flex-none flex-none" style={{ backgroundColor: '#3D6FB4' }}></div>
                                                        <div>Kontruksi</div>
                                                    </div>
                                                    <div className="inline-flex items-start mr-6 mb-6">
                                                        <div className="w-4 h-4 mr-2 flex-none flex-none" style={{ backgroundColor: '#F38C00' }}></div>
                                                        <div>Pertanian, Kehutanan dan Perikanan</div>
                                                    </div>
                                                    <div className="inline-flex items-start mr-6 mb-6">
                                                        <div className="w-4 h-4 mr-2 flex-none flex-none" style={{ backgroundColor: '#FFE700' }}></div>
                                                        <div>Transportasi dan Pergudangan</div>
                                                    </div>
                                                    <div className="inline-flex items-start mr-6 mb-6">
                                                        <div className="w-4 h-4 mr-2 flex-none flex-none" style={{ backgroundColor: '#BB31AD' }}></div>
                                                        <div>Informasi dan Komunikasi</div>
                                                    </div>
                                                    <div className="inline-flex items-start mr-6 mb-6">
                                                        <div className="w-4 h-4 mr-2 flex-none" style={{ backgroundColor: '#4C8D10' }}></div>
                                                        <div>Jasa Pendidikan</div>
                                                    </div>
                                                </div>
                                                <div>
                                                    <DataButton dataTable="/images/data-pdrb.jpg" />
                                                </div>
                                            </div>
                                        </div>
                                    </CardBody>
                                </Card>
                            </Fade>
                        </div>
                    )}
                    { chartID === 'umr' &&
                    (
                        <div id="chart-komoditas-daerah" className="pt-6 pb-16">
                            <hr className="h-0.5 bg-bkpm-light-blue-01 w-12 rounded-full border-0 mb-2" />
                            <h4 className="text-bkpm-light-blue-01">UMR</h4>
                            <Fade bottom distance="30px">
                                <Card className="shadow-xl ring-0 ring-opacity-0 ring-transparent">
                                    <CardBody className="flex flex-col justify-center items-stretch">
                                        <div className="h-80 w-full text-center mb-12">
                                            <UmrChart />
                                        </div>
                                        <div className="flex justify-end">
                                            <DataButton dataTable="/images/data-umr.jpg" />
                                        </div>
                                    </CardBody>
                                </Card>
                            </Fade>
                        </div>
                    )}
                    { chartID === 'investasi' &&
                    (
                        <div id="chart-komoditas-daerah" className="pt-6 pb-16">
                            <hr className="h-0.5 bg-bkpm-light-blue-01 w-12 rounded-full border-0 mb-2" />
                            <h4 className="text-bkpm-light-blue-01">Realisasi Investasi</h4>
                            <Fade bottom distance="30px">
                                <Card className="shadow-xl ring-0 ring-opacity-0 ring-transparent">
                                    <CardBody className="flex flex-col justify-center items-stretch">
                                        <div className="w-full text-center mb-12">
                                            <InvestasiChart />
                                        </div>
                                        <div className="flex text-2xs justify-center mt-12">
                                            <div className="mr-12 flex items-center">
                                                <div className="h-4 w-4 mr-3 items-center" style={ { background: 'rgb(93, 98, 181)' } } ></div>
                                                <span>PMA (US$ ribu)</span>
                                            </div>
                                            <div className="flex items-center">
                                                <div className="h-4 w-4 mr-3 flex items-center" style={ { background: 'rgb(41, 195, 190)' } } ></div>
                                                <span>PMDN (Rp juta)</span>
                                            </div>
                                        </div>
                                        <div className="flex justify-end">
                                            <DataButton dataTable="/images/data-investasi.jpg" />
                                        </div>
                                    </CardBody>
                                </Card>
                            </Fade>
                        </div>
                    )}
                </ResponsiveContainer>
            </section>
        </>
    )
}

export default DetailRegional