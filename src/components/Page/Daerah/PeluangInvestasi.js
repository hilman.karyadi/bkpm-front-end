import React from 'react'
import Fade from 'react-reveal'
import { Swiper, SwiperSlide } from 'swiper/react'
import 'swiper/css'

import ResponsiveContainer from 'components/UI/ResponsiveContainer'
import IconIndustri from 'components/images/ic-peluang-menu-industri.svg'
import IconInfrastruktur from 'components/images/ic-peluang-menu-infrastruktur.svg'
import IconSmelter from 'components/images/ic-peluang-menu-smelter.svg'
import IconPariwisata from 'components/images/ic-peluang-menu-pariwisata.svg'

import PeluangInvestasiSlideItem from './PeluangInvestasiSlideItem'
import PeluangIcon from './PeluangIcon'


const PeluangInvestasi = () => (
    <section className="py-12">
        <ResponsiveContainer>
            <hr className="h-0.5 bg-bkpm-green w-12 rounded-full border-0 mb-2" />
            <h4 className="text-bkpm-green mb-12">Peluang Investasi berdasarkan Sektor</h4>
            <div className="inline-grid gap-20 grid-flow-col mb-16">
                <div className='text-center'>
                    <button className="w-16 h-16 group rounded-full bg-bkpm-dark-blue-01 flex items-center justify-center mx-auto mb-2 "><img src={ IconIndustri } className="w-10 transform -translate-y-px translate-x-px transition-all group-hover:transform group-hover:scale-125" /></button>
                    <h6 className="uppercase text-bkpm-dark-blue-01 font-bold">Industri</h6>
                </div>
                <div className='text-center'>
                    <button className="w-16 h-16 group rounded-full bg-bkpm-dark-blue-01 flex items-center justify-center mx-auto mb-2 "><img src={ IconInfrastruktur } className="w-10 transform -translate-y-px translate-x-px transition-all group-hover:transform group-hover:scale-125" /></button>
                    <h6 className="uppercase text-bkpm-dark-blue-01 font-bold">Infrastruktur</h6>
                </div>
                <div className='text-center'>
                    <button className="w-16 h-16 group rounded-full bg-bkpm-dark-blue-01 flex items-center justify-center mx-auto mb-2 "><img src={ IconSmelter } className="w-10 transform -translate-y-px translate-x-px transition-all group-hover:transform group-hover:scale-125" /></button>
                    <h6 className="uppercase text-bkpm-dark-blue-01 font-bold">Smelter</h6>
                </div>
                <div className='text-center'>
                    <button className="w-16 h-16 group rounded-full bg-bkpm-dark-blue-01 flex items-center justify-center mx-auto mb-2 "><img src={ IconPariwisata } className="w-10 transform -translate-y-px translate-x-px transition-all group-hover:transform group-hover:scale-125" /></button>
                    <h6 className="uppercase text-bkpm-dark-blue-01 font-bold">Pariwisata</h6>
                </div>
            </div>
            <Fade bottom distance="30px">
                <Swiper
                    className="umkm-slider same-height"
                    spaceBetween={24}
                    slidesPerView={1.4}
                    loop={true}
                    breakpoints={{
                        480: {
                            slidesPerView: 2.3,
                        },
                        786: {
                            slidesPerView: 3.3,
                        },
                        980: {
                            slidesPerView: 4.3,
                        }
                    }}
                >
                    <SwiperSlide>
                        <PeluangInvestasiSlideItem title="Pengembangan Fasilitas Wisata Pantai Batu Karas" area="Kab. Pangandaran" value="Rp 20 Miliar" image="/images/pantai-batu-karas.jpg" />
                    </SwiperSlide>
                    <SwiperSlide>
                        <PeluangInvestasiSlideItem title="Pengembangan Fasilitas Wisata Geopark Ciletuh" area="Sukabumi" value="Rp 30 Miliar" image="/images/geopark-ciletuh.jpg" />
                    </SwiperSlide>
                    <SwiperSlide>
                        <PeluangInvestasiSlideItem title="Pengembangan Fasilitas Wisata Ciwidey" area="Kab. Bandung" value="Rp 15 Miliar" image="/images/ciwidey.jpg" />
                    </SwiperSlide>
                    <SwiperSlide>
                        <PeluangInvestasiSlideItem title="Pengembangan Fasilitas Wisata Pantai Batu Karas" area="Kab. Pangandaran" value="Rp 20 Miliar" image="/images/pantai-batu-karas.jpg" />
                    </SwiperSlide>
                    <SwiperSlide>
                        <PeluangInvestasiSlideItem title="Pengembangan Fasilitas Wisata Geopark Ciletuh" area="Sukabumi" value="Rp 30 Miliar" image="/images/geopark-ciletuh.jpg" />
                    </SwiperSlide>
                    <SwiperSlide>
                        <PeluangInvestasiSlideItem title="Pengembangan Fasilitas Wisata Ciwidey" area="Kab. Bandung" value="Rp 15 Miliar" image="/images/ciwidey.jpg" />
                    </SwiperSlide>
                    
                </Swiper>
            </Fade>      
        </ResponsiveContainer>
    </section>
)

export default PeluangInvestasi