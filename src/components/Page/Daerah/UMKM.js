import React from 'react'

import ResponsiveContainer from 'components/UI/ResponsiveContainer'
import UMKMSlideItem from 'components/Page/Home/UMKMSlideItem'

import { Swiper, SwiperSlide } from 'swiper/react'
import 'swiper/css'
import Fade from 'react-reveal/Fade'

const UMKM = () => (
    <section className="py-24 bg-bkpm-light-blue-01">
            <ResponsiveContainer className="overflow-visible">
                <hr className="h-0.5 bg-white w-12 rounded-full border-0 mb-2" />
                <h4 className="text-white mb-12">Usaha Mikro, Kecil dan Menengah</h4>
                <Fade bottom distance="50px">
                    <Swiper
                        className="umkm-slider overflow-visible same-height"
                        spaceBetween={24}
                        slidesPerView={1.4}
                        breakpoints={{
                            480: {
                                slidesPerView: 2.3,
                            },
                            786: {
                                slidesPerView: 3.3,
                            },
                            980: {
                                slidesPerView: 4.3,
                            }
                        }}
                    >
                        <SwiperSlide>
                            <UMKMSlideItem title="Jerawood Craft" area="Kabupaten Bogor" category="Kerajinan Kayu" image="/images/kayu-jati.jpg" className="h-full shadow-xl" >
                                <p>Pemilik usaha Jerawood Craft. UMKM ini berdiri sejak 2019. Jerawood Craft merupakan usaha aneka kerajinan limbah kayu yang disulap menjadi produk bernilai jual tinggi.</p>
                            </UMKMSlideItem>
                        </SwiperSlide>
                        <SwiperSlide>
                            <UMKMSlideItem title="Puspita Batik Indigo Natural" area="DIY Yogyakarta" category="Tas, Dompet, Jaket, Kemeja, Dress, Kain" image="/images/batik.jpg" className="h-full" className="h-full shadow-xl" >
                                <p>Yogyakarta merupakan daerah yang kuat dengan industri kreatifnya. Kegiatan ini merupakan salah bentuk dukungan kami untuk terus menumbuhkan Industri Kreatif yang inovatif.z</p>
                            </UMKMSlideItem>
                        </SwiperSlide>
                        <SwiperSlide>
                            <UMKMSlideItem title="CV. Jepara Abadi" area="Kabupaten Bogor" category="Kerajinan Furnitur Kayu" image="/images/kayu-jati.jpg" className="h-full shadow-xl" >
                                <p>CV. Jepara Abadi tumbuh meramaikan industri lokal dalam bidang furnitur dan kerajinan kayu lainnya. Berdiri sejak tahun 1989.</p>
                            </UMKMSlideItem>
                        </SwiperSlide>
                        <SwiperSlide>
                            <UMKMSlideItem title="Batik Yogyakarta" area="DIY Yogyakarta" category="Tas, Dompet, Jaket, Kemeja" image="/images/batik.jpg" className="h-full shadow-xl">
                                <p>Yogyakarta merupakan daerah yang kuat dengan industri kreatifnya. Kegiatan ini merupakan salah bentuk dukungan kami untuk terus menumbuhkan Industri Kreatif </p>
                            </UMKMSlideItem>
                        </SwiperSlide>
                        <SwiperSlide>
                            <UMKMSlideItem title="Jerawood Craft" area="Kabupaten Bogor" category="Kerajinan Kayu" image="/images/kayu-jati.jpg" className="h-full shadow-xl" >
                                <p>Pemilik usaha Jerawood Craft. UMKM ini berdiri sejak 2019. Jerawood Craft merupakan usaha aneka kerajinan limbah kayu yang disulap menjadi produk bernilai jual tinggi.</p>
                            </UMKMSlideItem>
                        </SwiperSlide>
                        <SwiperSlide>
                            <UMKMSlideItem title="Puspita Batik Indigo Natural" area="DIY Yogyakarta" category="Tas, Dompet, Jaket, Kemeja, Dress, Kain" image="/images/batik.jpg" className="h-full" className="h-full shadow-xl" >
                                <p>Yogyakarta merupakan daerah yang kuat dengan industri kreatifnya. Kegiatan ini merupakan salah bentuk dukungan kami untuk terus menumbuhkan Industri Kreatif yang inovatif.z</p>
                            </UMKMSlideItem>
                        </SwiperSlide>
                        <SwiperSlide>
                            <UMKMSlideItem title="CV. Jepara Abadi" area="Kabupaten Bogor" category="Kerajinan Furnitur Kayu" image="/images/kayu-jati.jpg" className="h-full shadow-xl" >
                                <p>CV. Jepara Abadi tumbuh meramaikan industri lokal dalam bidang furnitur dan kerajinan kayu lainnya. Berdiri sejak tahun 1989.</p>
                            </UMKMSlideItem>
                        </SwiperSlide>
                        <SwiperSlide>
                            <UMKMSlideItem title="Batik Yogyakarta" area="DIY Yogyakarta" category="Tas, Dompet, Jaket, Kemeja" image="/images/batik.jpg" className="h-full shadow-xl">
                                <p>Yogyakarta merupakan daerah yang kuat dengan industri kreatifnya. Kegiatan ini merupakan salah bentuk dukungan kami untuk terus menumbuhkan Industri Kreatif </p>
                            </UMKMSlideItem>
                        </SwiperSlide>
                    </Swiper>
            </Fade>
        </ResponsiveContainer>
    </section>
)

export default UMKM