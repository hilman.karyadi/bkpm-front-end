import React, { useState } from 'react'
import Modal from 'react-modal'

import ArrowButton from 'components/UI/ArrowButton'

Modal.setAppElement('#___gatsby')

const DataButton = ({ dataTable }) => {

    const [chartDataPopup, setChartDataPopup] = useState(false)

    const openChartData = () => setChartDataPopup(true)
    const closeChartData = () => setChartDataPopup(false)

    return (
        <>
            <Modal
                isOpen={chartDataPopup}
                onRequestClose={closeChartData}
                overlayClassName="inset-0 bg-opacity-90 bg-black z-50 fixed flex items-center justify-center "
                className="p-0 w-10/12"
                style={{content: { maxWidth: '650px'}}}
            >
                <div className="bg-white p-12 rounded-lg">
                    <img src={ dataTable } className="w-2/3 mx-auto block" />
                </div>
            </Modal>
            <ArrowButton buttonStyle="expanded" buttonClick={openChartData}>Data</ArrowButton>
        </>
    )
}

export default DataButton
