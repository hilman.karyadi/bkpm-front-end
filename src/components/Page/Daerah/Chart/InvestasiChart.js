import React, { useState } from 'react'
import Fade from 'react-reveal/Fade'
//import { Card, CardBody } from '@windmill/react-ui'
import { ResponsiveBar } from '@nivo/bar'

const data = [
    {
        name: 'Sektor Konstruksi',
        pma: '0',
        pmaColor: 'rgb(93, 98, 181)',
        pmdn: '62094840',
        pmdnColor: 'rgb(41, 195, 190)',
        color: {
            pma: 'rgb(93, 98, 181)',
            pmdn: 'rgb(41, 195, 190)',
        }
    },
    {
        name: 'Sektor Perumahan, Kawasan Industri dan Perkantoran',
        pma: '5881045',
        pmaColor: 'rgb(93, 98, 181)',
        pmdn: '49284164',
        pmdnColor: 'rgb(41, 195, 190)',
        color: {
            pma: 'rgb(93, 98, 181)',
            pmdn: 'rgb(41, 195, 190)',
        }
    },
    {
        name: 'Sektor Transportasi, Gudang dan Telekomunikasi',
        pma: '2723240',
        pmaColor: 'rgb(93, 98, 181)',
        pmdn: '18654681',
        pmdnColor: 'rgb(41, 195, 190)',
        color: {
            pma: 'rgb(93, 98, 181)',
            pmdn: 'rgb(41, 195, 190)',
        }
    },
    {
        name: 'Sektor Listrik, Gas dan Air',
        pma: '866282',
        pmdn: '45452714',
        color: {
            pma: 'rgb(93, 98, 181)',
            pmdn: 'rgb(41, 195, 190)',
        }
    },
    {
        name: 'Sektor Industri, Logam Dasar, Barang Logam, Mesin dan Elektronik',
        pma: '1868179',
        pmaColor: 'rgb(93, 98, 181)',
        pmdn: '20708397',
        pmdnColor: 'rgb(41, 195, 190)',
        color: {
            pma: 'rgb(93, 98, 181)',
            pmdn: 'rgb(41, 195, 190)',
        }
    },
    {
        name: 'Sektor Industri Makanan',
        pma: '14630',
        pmaColor: 'rgb(93, 98, 181)',
        pmdn: '6298839',
        pmdnColor: 'rgb(41, 195, 190)',
        color: {
            pma: 'rgb(93, 98, 181)',
            pmdn: 'rgb(41, 195, 190)',
        }
    },
]


const DetailRegional = () => {
    const colors = {
        pma: 'rgb(93, 98, 181)',
        pmdn: 'rgb(41, 195, 190)',
    }
    const getColor = bar => colors[bar.id]
    const [year, setYear] = useState(2019)
    const [availableYear, setAvaiableYear] = useState([2018, 2019, 2020, 2021])

    const selectYearHandler = el => {
        setYear(el.target.value)
    }

    return (
        <>
            <div className="w-full flex justify-end mb-8">
                <select onChange={selectYearHandler} className="text-xs rounded-full ">
                    {availableYear.map( item => {
                        if ( year === item ) {
                            return <option value={ item } selected>{ item }</option>
                        } else {
                            return <option value={ item }>{ item }</option>

                        }
                    } )}
                </select>
            </div>
            <Fade spy={ year }>
                <div className="h-80">
                    <ResponsiveBar
                        data={ data }
                        keys={['pma','pmdn']}
                        indexBy="name"
                        margin={{ top: 0, right: 50, bottom: 10, left: 100 }}
                        layout="vertical"
                        padding={0.6}
                        groupMode="grouped"
                        valueScale={{ type: 'linear' }}
                        indexScale={{ type: 'band', round: true }}
                        valueFormat={{ enabled: true }}
                        colors={getColor}
                        axisTop={null}
                        axisRight={null}
                        axisBottom={{
                            tickSize: 5,
                            tickPadding: 5,
                            tickRotation: 0,
                            tickValues: 6,
                            format: () => false
                        }}
                        axisLeft={{
                            tickSize: 0,
                            tickPadding: 5,
                            tickRotation: 0,
                            tickValues: 4,
                            format: value => value.toLocaleString('id-ID')
                        }}
                        theme={{
                            grid: {
                            line: {
                                stroke: "#f1f1f1",
                                strokeWidth: 1,
                            }
                            }
                        }}
                        enableLabel={false}
                        tooltip={(input) => {
                            return (
                            <div className="px-3 py-2 text-xs border border-solid border-gray-500 bg-white rounded-lg">
                            <p className="mb-0"><span className="uppercase">{ input.id }</span> { input.indexValue }: <strong>{ input.id === 'pma' ? 'US$' : 'Rp' } { input.value.toLocaleString('id-ID') }</strong></p>
                            </div>
                        )}}
                    />
                </div>
            </Fade>
            <div className="grid grid-cols-6 w-full text-xs pl-36 pr-20 gap-4">
                <div>Sektor Konstruksi</div>
                <div>Sektor Perumahan, Kawasan Industri dan Perkantoran</div>
                <div>Sektor Transportasi, Gudang dan Telekomunikasi</div>
                <div>Sektor Listrik, Gas dan Air</div>
                <div>Sektor Industri, Logam Dasar, Barang Logam, Mesin dan Elektronik</div>
                <div>Sektor Industri Makanan</div>
            </div>
        </>
    )
}

export default DetailRegional