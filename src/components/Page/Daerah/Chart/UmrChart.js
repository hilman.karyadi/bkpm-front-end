import React from 'react'
import { ResponsiveLine  } from '@nivo/line'

const UmrChart = () => {

    const data = [
        {
            id: 'umr',
            color: '#000000',
            data: [
                {
                    x: "2016",
                    y: 2250000,
                },
                {
                    x: "2017",
                    y: 1475000,
                },
                {
                    x: "2018",
                    y: 1675000,
                },
                {
                    x: "2019",
                    y: 1835000,
                },
                {
                    x: "2020",
                    y: 1925000,
                },
                {
                    x: "2021",
                    y: 1995000,
                }
            ],
        }
    ]
    const getColor = bar => bar.data.color
    
    return (
        <ResponsiveLine
            data={data}
            margin={{ top: 50, right: 50, bottom: 50, left: 100 }}
            xScale={{ type: 'point' }}
            yScale={{ type: 'linear', min: 1000000, max: 3000000, stacked: true, reverse: false }}
            yFormat=" >-.2f"
            axisTop={null}
            axisRight={null}
            axisBottom={{
                orient: 'bottom',
                tickSize: 5,
                tickPadding: 5,
                tickRotation: 0,
                legend: 'Tahun',
                legendOffset: 36,
                legendPosition: 'middle'
            }}
            axisLeft={{
                orient: 'left',
                tickSize: 5,
                tickPadding: 5,
                tickRotation: 0,
                legendOffset: -40,
                format: value => `Rp ${ value.toLocaleString('id-ID')}`
            }}
            pointSize={10}
            pointColor={{ theme: 'background' }}
            pointBorderWidth={2}
            pointBorderColor={{ from: 'serieColor' }}
            pointLabelYOffset={-12}
            useMesh={true}
            enableSlices="x"
            sliceTooltip={input => {
                console.log(input)
                return (
                <div className="px-3 py-2 text-xs border border-solid border-gray-500 bg-white rounded-lg">
                  {input.data.komoditi}: Rp { Number(input.data.value).toLocaleString() }
                </div>
              )}}
        />
    )
}

export default UmrChart