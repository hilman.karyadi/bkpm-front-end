import React, { useState } from 'react'
import Fade from 'react-reveal/Fade'
import { ResponsiveBar } from '@nivo/bar'

const KomoditasDaerahChart = () => {

    const [year, setYear] = useState(2019)
    const [availableYear, setAvaiableYear] = useState([2018, 2019, 2020, 2021])

    const selectYearHandler = el => {
        setYear(el.target.value)
    }

    const data = [
        {
            komoditi: "Komoditi Nanas",
            value: 228600.5,
            color: '#053783'
        },
        {
            komoditi: "Komoditi Mangga",
            value: 418521.7,
            color: '#4C8D10'
        },
        {
            komoditi: "Komoditi Sapi",
            value: 532840,
            color: '#0093ED'
        },
        {
            komoditi: "Komoditi Jagung",
            value: 959933,
            color: '#053783'
        },
        {
            komoditi: "Komoditi Pisang",
            value: 1220174,
            color: '#4C8D10'
        },
        {
            komoditi: "Komoditi Padi",
            value: 9084957,
            color: '#053783'
        },
        {
            komoditi: "Komoditi Domba",
            value: 12014083,
            color: '#0093ED'
        },
    ]
    const colors = {
    "Komoditi Domba": '#0093ED',
    "Komoditi Padi": '#053783',
    "Komoditi Pisang": '#4C8D10',
    "Komoditi Jagung": '#053783',
    "Komoditi Sapi": '#0093ED',
    "Komoditi Mangga": '#4C8D10',
    "Komoditi Nanas": '#4C8D10',}
    const getColor = bar => bar.data.color
    
    return (
        <>
            <div className="w-full flex justify-end mb-8">
                <select onChange={selectYearHandler} className="text-xs rounded-full ">
                    {availableYear.map( item => {
                        if ( year === item ) {
                            return <option value={ item } selected>{ item }</option>
                        } else {
                            return <option value={ item }>{ item }</option>

                        }
                    } )}
                </select>
            </div>
            <Fade spy={ year }>
                <div className="h-80">
                    <ResponsiveBar
                        data={data}
                        keys={["value"]}
                        indexBy="komoditi"
                        margin={{ top: 0, right: 50, bottom: 50, left: 100 }}
                        layout="horizontal"
                        padding={0.6}
                        groupMode="grouped"
                        valueScale={{ type: 'linear' }}
                        indexScale={{ type: 'band', round: true }}
                        valueFormat={{ enabled: true }}
                        colors={getColor}
                        axisTop={null}
                        axisRight={null}
                        axisBottom={{
                            tickSize: 5,
                            tickPadding: 5,
                            tickRotation: 0,
                            format: value => value.toLocaleString('id-ID'),
                            tickValues: 6,
                        }}
                        axisLeft={{
                            tickSize: 0,
                            tickPadding: 5,
                            tickRotation: 0,
                        }}
                        theme={{
                            grid: {
                            line: {
                                stroke: "#f1f1f1",
                                strokeWidth: 1,
                            }
                            }
                        }}
                        enableLabel={false}
                        tooltip={(input) => {
                            return (
                            <div className="px-3 py-2 text-xs border border-solid border-gray-500 bg-white rounded-lg">
                            {input.data.komoditi}: { Number(input.data.value).toLocaleString() } Ton
                            </div>
                        )}}
                    />
                </div>
            </Fade>
        </>
    )
}

export default KomoditasDaerahChart