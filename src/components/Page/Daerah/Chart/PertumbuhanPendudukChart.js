import React from 'react'
//import { Card, CardBody } from '@windmill/react-ui'
import { ResponsiveBar } from '@nivo/bar'

const DetailRegional = () => {

    const data = [
        {
            year: 2017,
            pria: 24335337,
            wanita: 23702496,
            color: '#cacaca'
        },
        {
            year: 2018,
            pria: 24652609,
            wanita: 24031252,
            color: 'green'
        },
        {
            year: 2019,
            pria: 24962701,
            wanita: 24354011,
            color: 'blue'
        },
        {
            year: 2020,
            pria: 24508885,
            wanita: 23765277,
            color: 'yellow'
        }
    ]
    const colors = { 'pria': '#FF5C00', 'wanita': '#053783' }
    const getColor = bar => colors[bar.id]
    
    return (
        <ResponsiveBar
            data={data}
            keys={[ 'pria', 'wanita' ]}
            indexBy="year"
            margin={{ top: 0, right: 50, bottom: 50, left: 80 }}
            padding={0.6}
            groupMode="grouped"
            valueScale={{ type: 'linear' }}
            indexScale={{ type: 'band', round: true }}
            valueFormat={{ enabled: true }}
            colors={getColor}
            axisTop={null}
            axisRight={null}
            axisBottom={{
                tickSize: 5,
                tickPadding: 5,
                tickRotation: 0,
            }}
            axisLeft={{
                tickSize: 0,
                tickPadding: 5,
                tickRotation: 0,
                tickValues: 4,
                format: value => value.toLocaleString('id-ID')
            }}
            theme={{
                grid: {
                  line: {
                    stroke: "#f1f1f1",
                    strokeWidth: 1,
                  }
                }
              }}
            enableLabel={false}
            tooltip={(input) => {
                console.log(input)
                return (
                <div className="px-3 py-2 text-xs border border-solid border-gray-500 bg-white rounded-lg">
                  {input.id} : { Number(input.value).toLocaleString() } Jiwa
                </div>
              )}}
            legends={[
                {
                    dataFrom: 'keys',
                    anchor: 'bottom',
                    direction: 'row',
                    justify: false,
                    translateX: 0,
                    translateY: 50,
                    itemsSpacing: 2,
                    itemWidth: 100,
                    itemHeight: 20,
                    itemDirection: 'left-to-right',
                    itemOpacity: 0.85,
                    symbolSize: 8,
                    effects: [
                        {
                            on: 'hover',
                            style: {
                                itemOpacity: 1
                            }
                        }
                    ]
                }
            ]}
        />
    )
}

export default DetailRegional