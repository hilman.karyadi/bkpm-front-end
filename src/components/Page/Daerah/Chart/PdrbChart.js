import React, { useState } from 'react'
import Fade from 'react-reveal/Fade'
import { ResponsiveBar } from '@nivo/bar'

const PdrbChart = () => {

    const [year, setYear] = useState(2019)
    const [availableYear, setAvaiableYear] = useState([2018, 2019, 2020, 2021])

    const selectYearHandler = el => {
        setYear(el.target.value)
    }

    const data = [
        {
            komoditi: "Industri Pengolahan",
            value: 645367746,
            color: '#0093ED'
        },
        {
            komoditi: "Perdagangan Besar dan Eceran, dan Reparasi Mobil dan Sepeda Motor",
            value: 445367746,
            color: '#053783'
        },
        {
            komoditi: "Kontruksi",
            value: 435367746,
            color: '#3D6FB4'
        },
        {
            komoditi: "Pertanian, Kehutanan dan Perikanan",
            value: 425367746,
            color: '#F38C00'
        },
        {
            komoditi: "Transportasi dan Pergudangan",
            value: 405367746,
            color: '#FFE700'
        },
        {
            komoditi: "Informasi dan Komunikasi",
            value: 305367746,
            color: '#BB31AD'
        },
        {
            komoditi: "Jasa Pendidikan",
            value: 305367746,
            color: '#4C8D10'
        },
    ]
    const getColor = bar => bar.data.color
    
    return (
        <>
            <div className="w-full flex justify-end mb-8">
                <select onChange={selectYearHandler} className="text-xs rounded-full ">
                    {availableYear.map( item => {
                        if ( year === item ) {
                            return <option value={ item } selected>{ item }</option>
                        } else {
                            return <option value={ item }>{ item }</option>

                        }
                    } )}
                </select>
            </div>
            <Fade spy={ year }>
                <div className="flex">
                    <div className="flex flex-col justify-around text-2xs flex-auto text-left pt-3 pb-16">
                        {[...data].reverse().map( item => <span className="whitespace-normal">{ item.komoditi }</span> )}
                    </div>
                    <div className="h-80 w-10/12 flex-none">
                        <ResponsiveBar
                            data={data}
                            keys={["value"]}
                            indexBy="komoditi"
                            margin={{ top: 0, right: 50, bottom: 50, left: 20 }}
                            layout="horizontal"
                            padding={0.6}
                            groupMode="grouped"
                            valueScale={{ type: 'linear' }}
                            indexScale={{ type: 'band', round: true }}
                            valueFormat={{ enabled: true }}
                            colors={getColor}
                            axisTop={null}
                            axisRight={null}
                            axisBottom={{
                                tickSize: 5,
                                tickPadding: 5,
                                tickRotation: 0,
                                format: value => `Rp ${ value.toLocaleString('id-ID')}`,
                                tickValues: 6,
                            }}
                            axisLeft={null}
                            theme={{
                                grid: {
                                line: {
                                    stroke: "#f1f1f1",
                                    strokeWidth: 1,
                                }
                                }
                            }}
                            enableLabel={false}
                            tooltip={(input) => {
                                return (
                                <div className="px-3 py-2 text-xs border border-solid border-gray-500 bg-white rounded-lg">
                                {input.data.komoditi}: Rp { Number(input.data.value).toLocaleString() }
                                </div>
                            )}}
                        />
                    </div>
                </div>
            </Fade>
        </>
    )
}

export default PdrbChart