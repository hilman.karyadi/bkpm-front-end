import React, { useState } from 'react'
import styled from 'styled-components'
import { Card, Dropdown, Button, DropdownItem } from '@windmill/react-ui'
import { StaticImage } from 'gatsby-plugin-image'
import { FaCaretDown } from 'react-icons/fa'

import ResponsiveContainer from 'components/UI/ResponsiveContainer'
import PetaDaerahJabar from 'components/images/peta-daerah-jabar.svg'
import ImageDaerahJabar from 'components/images/bg-hero-daerah.jpg'
import RegionButtons from 'components/Page/PetaRealisasiInvestasi/RegionButtons'

const HeroImage = styled.div`
    clip-path: polygon(0 0, 100% 0, 100% 100%, 100px 100%);
    z-index: 3;
    background-image: url('${ ImageDaerahJabar }');
`

const Hero = () => {
    const [region, setRegion] = useState(null)
    const [menuDaerahIsOpen, setMenuDaerahIsOpen] = useState(false)
    const [menuKabupatenIsOpen, setMenuKabupatenIsOpen] = useState(false)
    return (
        <section className="min-h-96 relative bg-gradient-to-b from-bkpm-dark-blue-01 to-bkpm-dark-blue-02 text-white pt-32 pb-16 md:h-auto">
            <div className="absolute top-4 left-0 w-full z-10">
                <RegionButtons region={region} setRegion={setRegion}/>
            </div>
            <HeroImage className="bg-black absolute top-0 right-0 left-1/2 bottom-0 hidden md:block md:bg-cover md:bg-center"></HeroImage>
            <ResponsiveContainer>
                <div className="md:w-1/2">
                    <div className="flex flex-wrap items-center mb-8 sm:flex-row sm:flex-nowrap">
                        <div className="w-1/2 pr-8 sm:w-28">
                            <Card className="p-2">
                                <StaticImage src="../../images/logo-daerah-jabar.png" placeholder="blur" alt="Logo Daerah Jawa Barat" className="max-w-full" />
                            </Card>
                        </div>
                        <div className="mb-6 sm:mb-0">
                            <h2 className="mb-0">Provinsi Jawa Barat</h2>
                        </div>
                    </div>
                    <div className="mb-8 flex flex-wrap ">
                        <div className="w-full md:w-auto">
                            <Button className="bg-gradient-to-r from-bkpm-dark-blue-01 to-bkpm-green rounded-full px-12 relative" onClick={ () => setMenuKabupatenIsOpen((prevState) => !prevState)}>Kabupaten / Kota<FaCaretDown className="absolute right-4 top-1/2 transform -translate-y-1/2" /></Button>
                            <div className="relative z-10" >
                                <Dropdown isOpen={menuKabupatenIsOpen} onClose={() => {}} >
                                    <DropdownItem className="whitespace-nowrap text-xs">Kota Bandung</DropdownItem>
                                    <DropdownItem className="whitespace-nowrap text-xs">Kabupaten Bandung</DropdownItem>
                                    <DropdownItem className="whitespace-nowrap text-xs">Kabupaten Bandung Barat</DropdownItem>
                                    <DropdownItem className="whitespace-nowrap text-xs">Kabupaten Bekasi</DropdownItem>
                                    <DropdownItem className="whitespace-nowrap text-xs">Kabupaten Bogor</DropdownItem>
                                </Dropdown>
                            </div>
                        </div>
                    </div>
                    <h5>Area: 35,378 km<sup>2</sup></h5>
                </div>
            </ResponsiveContainer>
        </section>
    )
}

export default Hero