import React, { useState } from 'react'

const MapContext = React.createContext({
    mapId: null,
    mapName: null,
    setMap: () => {}
})

export const MapContextProvider = (props) => {

    const [mapId,setMapId] = useState(null)
    const [mapName,setMapName] = useState(null)
    const setMap = (id = null, name = null) => {
        setMapId(id)
        setMapName(name)
    }

    return (
        <MapContext.Provider value={{mapId, mapName, setMap}}>
            { props.children }
        </MapContext.Provider>
    )
}

export default MapContext