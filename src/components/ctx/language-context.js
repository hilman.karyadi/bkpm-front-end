import React, { useState } from 'react'

const LanguageContext = React.createContext({
    language: 1,
    toggleLanguage: () => {}
})

export const LanguageContextProvider = (props) => {

    const [language,setLanguage] = useState(1)
    const toggleLanguage = (langId = null) => {
        if ( langId !== null ) {
            setLanguage(langId);
        } else {
            const newLanguage = language * -1
            setLanguage(newLanguage);
        }
    }

    return (
        <LanguageContext.Provider value={{language: language, toggleLanguage: toggleLanguage}}>
            { props.children }
        </LanguageContext.Provider>
    )
}

export default LanguageContext