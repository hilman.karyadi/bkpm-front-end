import React, { useState } from 'react'
import PropTypes from 'prop-types'

import ResponsiveContainer from 'components/UI/ResponsiveContainer'

import Logo from './Logo'
import LogoKementrian from './LogoKementrian'
import MobileMenuToggleBtn from './MobileMenuToggleBtn'
import MainMenu from './Menu/MainMenu'
import MobileMenu from './Menu/MobileMenu'
import LanguageSelector from './Menu/LanguageSelector'

const Header = (props) => {

    const [toggleMobileMenu, setToggleMobileMenu] = useState(false)

    const toggleBtnHandler = () => {
        setToggleMobileMenu( prevstate => !prevstate )
    }

    return (
        <>
            <MobileMenu mobileMenuState={ toggleMobileMenu } />
            <header className="bg-white fixed inset-x-0 top-0 py-4 z-40 border-b border-solid border-gray-200 lg:py-2">
                <ResponsiveContainer>
                    <div className="flex justify-between items-center">
                        <Logo className="w-24 lg:w-20 xl:w-32" />
                        <MainMenu className="hidden lg:block lg:mx-8" pageGroup={ props.pageGroup} />
                        <div className="hidden w-12 mr-8 lg:block xl:w-16">
                            <LanguageSelector />
                        </div>
                        <LogoKementrian className="hidden lg:flex lg:flex-col lg:items-end lg:w-48 xl:w-60" />
                        <MobileMenuToggleBtn className="lg:hidden" clickHandler={toggleBtnHandler} mobileMenuState={toggleMobileMenu} />
                    </div>
                </ResponsiveContainer>
            </header>
        </>
    );
}

Header.propTypes = {
    pageGroup: PropTypes.string
}
 
export default Header;