import React from 'react'

import ImgLogoKementrianInvestasi from 'components/images/logo-kementrian-investasi-2.inline.svg'
import ArrowButton from 'components/UI/ArrowButton'

const LogoKementrian = (props) => {
    return (
        <div { ...props }>
            <a href="https://www.bkpm.go.id/" target="_blank" className="w-full block" rel="noreferrer"><ImgLogoKementrianInvestasi className="w-full" alt="Ministry of Investment / Badan Koordinasi Penanaman Modal"/></a>
        </div>
    )
}

export default LogoKementrian
