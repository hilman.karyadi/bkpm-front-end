import React from "react"
import { Link } from "gatsby"
import PropTypes from 'prop-types'

import MenuItem from './MenuItem'
 
const MainMenu = (props) => {

  const { pageGroup, ...containerProps } = props

  return (
    <div { ...containerProps } >
      <ul className="inline-grid grid-flow-col gap-6 lg:text-xs xl:text-sm" >
          <MenuItem className={ `${props.pageGroup === 'daerah' ? 'active' : ''}` } menuLabel="Daerah" menuLink="/peta-realisasi-investasi" />
          <MenuItem className={ `${props.pageGroup === 'peluang-daerah' ? 'active' : ''}` } menuLabel="Peluang Investasi" menuLink="/peluang-daerah" />
          <MenuItem className={ `${props.pageGroup === 'infrastruktur' ? 'active' : ''}` } menuLabel="Infrastruktur" menuLink="#">
            <ul>
              <li><Link to="/infrastruktur">Infrastruktur</Link></li>
              <li><Link to="/kawasan-industri-kek">Kawasan Industri &amp; KEK</Link></li>
            </ul>
          </MenuItem>
          <MenuItem className={ `${props.pageGroup === 'insentif' ? 'active' : ''}` } menuLabel="Insentif" menuLink="#">
            <ul>
              <li><Link to="/insentif" state={{ defaultTab: 'taxholiday'}}>Tax Holiday</Link></li>
              <li><Link to="/insentif" state={{ defaultTab: 'taxallowance'}}>Tax Allowance</Link></li>
              <li><Link to="/insentif" state={{ defaultTab: 'masterlist'}}>Master List</Link></li>
              <li><Link to="/insentif" state={{ defaultTab: 'superdeduction'}}>Super Deduction</Link></li>
            </ul>
          </MenuItem>
          <MenuItem className={ `${props.pageGroup === 'umkm' ? 'active' : ''}` } menuLabel="Kemitraan Usaha Nasional" menuLink="/daftar-umkm" />
      </ul>
    </div>
  )
}

MainMenu.propTypes = {
  pageGroup: PropTypes.string
}

export default MainMenu
