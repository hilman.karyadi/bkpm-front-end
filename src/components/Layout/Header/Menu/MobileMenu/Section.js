import React from 'react'

const Section = ({ children }) => <div className="mb-8 text-center">{ children }</div>

export default Section