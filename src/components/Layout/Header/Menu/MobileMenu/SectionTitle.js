import React from 'react'

const SectionTitle = ({ children }) => <h5 className="mb-4 text-bkpm-dark-blue-01" >{ children }</h5>

export default SectionTitle