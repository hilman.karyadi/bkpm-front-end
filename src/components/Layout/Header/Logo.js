import React from 'react'
import { Link } from 'gatsby'

import ImgLogo from 'components/images/logo.inline.svg'

const LogoContainer = (props) => {
    return (
        <div { ...props }>
            <Link to="/" ><ImgLogo alt="Potensi Investasi Regional" /></Link>
        </div>
    )
}

export default LogoContainer
