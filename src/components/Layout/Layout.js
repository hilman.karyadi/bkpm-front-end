import React from 'react'
import PropTypes from 'prop-types'
import 'styles/global.css'
import { Windmill } from '@windmill/react-ui'
import { Helmet } from 'react-helmet'

import styled from 'styled-components'
import Header from './Header'
import Footer from './Footer'
import { LanguageContextProvider } from 'components/ctx/language-context'
import Theme from 'components/theme.js'

const Layout = (props) => {

    const pageTitle = `BKPM PIR${ props.pageTitle ? ` - ${ props.pageTitle }` : '' }`

    return (
        <Windmill theme={Theme}>
            <Helmet>
                <title>{ pageTitle }</title>
                <link rel="apple-touch-icon" sizes="180x180" href="/images/apple-touch-icon.png" />
                <link rel="icon" type="image/png" sizes="32x32" href="/images/favicon-32x32.png" />
                <link rel="icon" type="image/png" sizes="16x16" href="/images/favicon-16x16.png" />
                <link rel="manifest" href="/images/site.webmanifest" />
            </Helmet>
            <LanguageContextProvider>
                <Header pageGroup={ props.pageGroup } />
                <Main id={ props.pageID }>
                    { props.children }
                </Main>
                <Footer />
            </LanguageContextProvider>
        </Windmill>
    )
    
}

Layout.propTypes = {
    pageTitle: PropTypes.string,
    pageID: PropTypes.string,
    pageGroup: PropTypes.string
}

// Component styling
const Main = styled.main`
    margin-top: 75px;
    @media (min-width: 1024px) {
        margin-top: 99px;
    }
`

export default Layout