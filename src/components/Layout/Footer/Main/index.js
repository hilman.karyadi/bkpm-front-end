import React from 'react'

import MenuTitle from '../MenuTitle'
import Logo from './Logo'
import Contact from './Contact'
import MenuCol from './MenuCol'
import MenuLink from './MenuLink'
import LoginButton from './LoginButton'

const FooterMain = () => {
    return (
        <div className="flex flex-col md:flex-row md:w-full">
            <div className="mb-12 text-center md:mb-0 md:mr-16 md:text-left">
                <Logo />
                <Contact />
                <LoginButton />
            </div>
            <div className="flex flex-col md:flex-row md:flex-grow md:justify-end">
                <MenuCol>
                    <MenuTitle>Infrastruktur</MenuTitle>
                    <MenuLink menuLink="/infrastruktur">Infrastruktur</MenuLink>
                    <MenuLink menuLink="/kawasan-industri-kek">Kawasan Industri dan KEK</MenuLink>
                </MenuCol>
                <MenuCol>
                    <MenuTitle>Insentif Investasi</MenuTitle>
                    <MenuLink menuLink="/insentif" state={ {defaultTab: 'taxholiday'} }>Tax Holiday</MenuLink>
                    <MenuLink menuLink="/insentif" state={ {defaultTab: 'taxallowance'} }>Tax Allowance</MenuLink>
                    <MenuLink menuLink="/insentif" state={ {defaultTab: 'masterlist'} }>Master List</MenuLink>
                    <MenuLink menuLink="/insentif" state={ {defaultTab: 'superdeduction'} }>Super Deduction</MenuLink>
                </MenuCol>
                <MenuCol>
                    <MenuTitle>Pelayan Investasi Online</MenuTitle>
                    <MenuLink menuLink="#">Online Single Submission</MenuLink>
                    <MenuLink menuLink="#">National Single Window For Investment</MenuLink>
                </MenuCol>
            </div>
        </div>
    );
}

export default FooterMain;