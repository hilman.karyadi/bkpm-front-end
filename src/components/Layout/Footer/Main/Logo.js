import React from 'react'

import LogoWhite from 'components/images/logo-white.inline.svg'

const Logo = () => <div className="w-24 mb-8 mx-auto md:ml-0 lg:w-20 xl:w-32"><LogoWhite /></div>

export default Logo
