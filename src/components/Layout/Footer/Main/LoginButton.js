import React from 'react'

import ArrowButton from 'components/UI/ArrowButton'

const LoginButton = () => <div className="mt-8"><ArrowButton buttonClick={() => window.open("https://admin.regionalinvestment.bkpm.go.id:81/backend/public/index.php/login","_self") } buttonStyle="expanded" >Login Admin</ArrowButton></div>

export default LoginButton
