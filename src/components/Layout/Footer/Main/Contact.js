import React from 'react'

import ContactDetails from './ContactDetails'

const Contact = () => {
    return (
        <>
            <ContactDetails label="Phone"><a href="tel:+62215252008" className="hover:text-bkpm-light-blue-01">+6221 525 2008</a></ContactDetails>
            <ContactDetails label="Email"><a href="mailto:sipid@bkpm.go.id" className="hover:text-bkpm-light-blue-01">sipid@bkpm.go.id</a></ContactDetails>
        </>
    )
}

export default Contact
