import React from 'react'

const MenuCol = ({ children }) => <div className="mb-12 text-center md:mb-0 md:mr-16 md:text-left md:justify-between xl:mr-24 ">{ children }</div>

export default MenuCol
