import React from 'react'

const MenuTitle = ({ children }) => <h6 className="text-bkpm-light-blue-01 mb-4 font-bold whitespace-nowrap">{ children }</h6>
 
export default MenuTitle