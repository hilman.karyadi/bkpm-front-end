import React from 'react'

import ResponsiveContainer from 'components/UI/ResponsiveContainer'
import Main from './Main'
import Aside from './Aside'

const Footer = () => {
    return (
        <footer className="bg-gradient-to-b from-bkpm-dark-blue-01 to-bkpm-dark-blue-02 py-12 text-gray-50">
            <ResponsiveContainer>
                <Main />
                <Aside />
            </ResponsiveContainer>
        </footer>
    )
}

export default Footer