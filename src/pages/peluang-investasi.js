import React from "react"
import styled from "styled-components"
import GoogleMapReact from "google-map-react"

import Layout from "components/Layout/Layout"
import ResponsiveContainer from "components/UI/ResponsiveContainer"

import Search from "components/Page/PeluangInvestasi/Search"
import PeluangInvestasi from "components/Page/PeluangInvestasi/PeluangInvestasi"

const MapContainer = styled.section`
  height: calc(100vh - 220px);
`

const PeluangInvestasix = () => {
  return (
    <Layout pageID="peluang-investasi" pageGroup="peluang-daerah">
      <MapContainer>
        <GoogleMapReact
          bootstrapURLKeys={{ key: "AIzaSyD-U9NkKKhshvKgZRAM4MzlPk45HeLGIZ8" }}
          defaultCenter={{
            lat: 0.7893,
            lng: 113.9213,
          }}
          defaultZoom={5}
          options={{
            mapTypeId: "satellite",
          }}
        ></GoogleMapReact>
      </MapContainer>
      <Search />
      <PeluangInvestasi />
      <section className="bg-bkpm-light-blue-01 text-white">
        <ResponsiveContainer className="py-16">
          <h1 className="text-5xl text-center">Tahun 2020</h1>
          <div className="flex space-x-16 justify-center text-center">
            <div className="">
              <h5 className="text-2xl font-bold mb-0">23</h5>
              <span className="uppercase">Proyek</span>
            </div>
            <div className="">
              <h5 className="text-2xl font-bold mb-0">Rp. 103 Triliun</h5>
              <span className="uppercase">Total Nilai Proyek</span>
            </div>
            <div className="">
              <h5 className="text-2xl font-bold mb-0">23</h5>
              <span className="uppercase">Kabupaten</span>
            </div>
            <div className="">
              <h5 className="text-2xl font-bold mb-0">4</h5>
              <span className="uppercase">Sektor</span>
            </div>
          </div>
        </ResponsiveContainer>
      </section>
    </Layout>
  )
}

export default PeluangInvestasix
