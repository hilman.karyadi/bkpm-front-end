import React from 'react'
import GoogleMapReact from 'google-map-react';
import { FaPlane } from 'react-icons/fa'
import styled from 'styled-components'

import Layout from 'components/Layout/Layout'
import ResponsiveContainer from 'components/UI/ResponsiveContainer'

const DetailItem = (props) => {
    return (
        <div className="mb-4">
            <p><span className="font-bold text-bkpm-orange-02">{ props.label }</span><br />
            <span className="text-sm">{ props.children }</span></p>
        </div>
    )
}

const DetailBandara = () => {
    return (
        <Layout pageID="detail-bandara" pageGroup="infrastruktur" pageTitle="Bandara Cut Nyak Dien">
            <section className="py-12">
                <ResponsiveContainer>
                    <hr className="h-0.5 bg-bkpm-dark-blue-01 w-12 rounded-full border-0 mb-2" />
                    <h4 className="text-bkpm-dark-blue-01 mb-12">Bandara Cut Nyak Dhien</h4>
                    <div className="grid grid-cols-1 gap-6 md:grid-cols-2">
                        <div>
                            <div className="h-40 md:h-full">
                                <GoogleMapReact
                                    bootstrapURLKeys={{ key: 'AIzaSyD-U9NkKKhshvKgZRAM4MzlPk45HeLGIZ8' }}
                                    defaultCenter={{
                                        lat: 4.047331,
                                        lng: 96.2484215
                                    }}
                                    defaultZoom={15}
                                >
                                    <FaPlane className="text-2xl text-bkpm-dark-blue-01"
                                        lat={4.047331} 
                                        lng={96.2484215} 
                                    />
                                </GoogleMapReact>
                            </div>
                        </div>
                        <InfoContainer className="p-8 bg-bkpm-dark-blue-01 text-white">
                            <DetailItem label="Nama">CUT NYAK DHIEN</DetailItem>
                            <DetailItem label="Keterangan">Bandar Udara Cut Nyak Dhien Nagan Raya adalah bandar udara yang terletak di Nagan Raya, Aceh. Bandar udara ini memiliki ukuran landasan pacu 2.400 x 23 m meter menjadi 7,920 by 85 meter bulan Agustus 2014.</DetailItem>
                            <DetailItem label="Jarak Ibukota Provinsi">198 Km</DetailItem>
                            <DetailItem label="Alamat">Jalan Kuala Pesisir, Kuala Tuha, Kuala Pesisir, Kubang Gajah, Kuala Pesisir, Kabupaten Nagan Raya, Aceh 23681</DetailItem>
                            <DetailItem label="Maskapai">Wing Air, Susi Air</DetailItem>
                            <DetailItem label="Kode IATA">MEQ</DetailItem>
                        </InfoContainer>
                    </div>
                </ResponsiveContainer>
            </section>
        </Layout>
    )
}

const InfoContainer = styled.div`
    @media (min-width: 768px) {
        column-count: 2;
        column-gap: 2rem;
        & > * {
            break-inside: false;
        }
    }
`

export default DetailBandara
