import React, { useState, useEffect } from 'react'

import Layout from 'components/Layout/Layout'
import ResponsiveContainer from 'components/UI/ResponsiveContainer'

import Chart from 'components/Page/KawasanIndustriKek/Chart'
import PetaKawasan from 'components/Page/KawasanIndustriKek/PetaKawasan'

const KawasanIndustriKek = ({ location }) => {
    
    const [kawasanId, setKawasanId] = useState(1)
    const [keyword, setKeyword] = useState('')
    
    useEffect( () => {
        console.log(location)
        if ( location.state ) {
            if ( location.state.keyword ) {
                setKeyword(location.state.keyword)
            }
            if ( location.state.kawasanId ) {
                setKawasanId(location.state.kawasanId)
            }
        }
    },[])

    return (
        <Layout pageID="kawasan-industri-kek" pageGroup="infrastruktur">
            <section>
                <ResponsiveContainer className="py-12">
                    <hr className="h-0.5 bg-bkpm-dark-blue-01 w-12 rounded-full border-0 mb-2" />
                    <h4 className="text-bkpm-dark-blue-01 mb-12">Kawasan Industri &amp; KEK</h4>
                    <PetaKawasan kawasanId={ kawasanId } setKawasanId={ setKawasanId } keyword={ keyword } setKeyword={ setKeyword }/>
                </ResponsiveContainer>
            </section>
        </Layout>
    )
}

export default KawasanIndustriKek
