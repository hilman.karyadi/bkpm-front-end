import React, { useState, useContext, useEffect } from 'react'
import GoogleMapReact from 'google-map-react';
import styled from 'styled-components'

import Layout from 'components/Layout/Layout'
import PagedDisplay from 'components/UI/PagedDisplay'
import ResponsiveContainer from 'components/UI/ResponsiveContainer'

import Search from 'components/Page/DaftarUMKM/Search'
import UmkmItem from 'components/Page/DaftarUMKM/UMKMItem'
import dataUmkm from 'components/store/dataUmkm'

import { UmkmContextProvider } from 'components/Page/DaftarUMKM/Context'

const MapContainer = styled.section`
    height: calc( 100vh - 220px )
`

const DaftarUMKM = () => {

    const [searchTerm, setSearchTerm] = useState('')
    const [listItems, setListItems] = useState([...dataUmkm.map( item => <UmkmItem title={ item.name} area={`${item.kotaKabupaten.name}, ${item.propinsi.name} `} category={ item.bidangUsaha.description } image={item.image} className="h-full" />)])
    const [currentPage, setCurrentPage] = useState(1)

    const searchHandler = () => {
        setCurrentPage(1)
        setListItems([...dataUmkm.filter( item => item.name.toLowerCase().includes(searchTerm.toLowerCase()) ).map( item => <UmkmItem title={ item.name} area={`${item.kotaKabupaten.name}, ${item.propinsi.name} `} category={ item.bidangUsaha.description } image={item.image} className="h-full" />)])
    }

    return (
        <Layout pageID="daftar-umkm" pageGroup="umkm">
            <UmkmContextProvider>
                <MapContainer className="h-screen">
                    <GoogleMapReact
                        bootstrapURLKeys={{ key: 'AIzaSyD-U9NkKKhshvKgZRAM4MzlPk45HeLGIZ8' }}
                        defaultCenter={{
                            lat: 0.7893,
                            lng: 113.9213
                        }}
                        defaultZoom={5}
                        options={{
                            mapTypeId: "satellite"
                        }}
                    >
                    </GoogleMapReact>
                </MapContainer>
                <Search searchTerm={ searchTerm } setSearchTerm={ setSearchTerm } searchHandler={ searchHandler }/>
                <section className="pb-12">
                    <ResponsiveContainer >
                        <PagedDisplay
                            list={[...listItems]}
                            itemsPerPage={ 12 }
                            itemsPerRow={ 4 }
                            currentPage={ currentPage }
                            setCurrentPage={ setCurrentPage }
                        />
                    </ResponsiveContainer>
                </section>
            </UmkmContextProvider>
        </Layout>
    )
}

export default DaftarUMKM
