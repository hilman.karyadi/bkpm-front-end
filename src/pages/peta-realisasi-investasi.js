import React, { useState } from 'react'
import { DropdownItem } from '@windmill/react-ui'
import { navigate } from 'gatsby'

import Layout from 'components/Layout/Layout'
import { MapContextProvider } from 'components/ctx/map-context'

import Map from 'components/Page/PetaRealisasiInvestasi/Map'
import Chart from 'components/Page/PetaRealisasiInvestasi/Chart'
import ListPropinsi from 'components/Page/PetaRealisasiInvestasi/ListPropinsi'
import RegionButtons from 'components/Page/PetaRealisasiInvestasi/RegionButtons'
import ResponsiveContainer from 'components/UI/ResponsiveContainer'

const PetaRegional = () => {

    const [region, setRegion] = useState(null)
    const [chartData, setChartData] = useState([
        {
            id: 'id-jk',
            name: 'DKI Jakarta',
            pma: '4122995',
            pmaColor: 'rgb(93, 98, 181)',
            pmdn: '62094840',
            pmdnColor: 'rgb(41, 195, 190)',
            color: {
                pma: 'rgb(93, 98, 181)',
                pmdn: 'rgb(41, 195, 190)',
            }
        },
        {
            id: 'id-jb',
            name: 'Jawa Barat',
            pma: '5881045',
            pmaColor: 'rgb(93, 98, 181)',
            pmdn: '49284164',
            pmdnColor: 'rgb(41, 195, 190)',
            color: {
                pma: 'rgb(93, 98, 181)',
                pmdn: 'rgb(41, 195, 190)',
            }
        },
        {
            id: 'id-jt',
            name: 'Jawa Tengah',
            pma: '2723240',
            pmaColor: 'rgb(93, 98, 181)',
            pmdn: '18654681',
            pmdnColor: 'rgb(41, 195, 190)',
            color: {
                pma: 'rgb(93, 98, 181)',
                pmdn: 'rgb(41, 195, 190)',
            }
        },
        {
            id: 'id-ji',
            name: 'Jawa Timur',
            pma: '866282',
            pmdn: '45452714',
            color: {
                pma: 'rgb(93, 98, 181)',
                pmdn: 'rgb(41, 195, 190)',
            }
        },
        {
            id: 'id-bt',
            name: 'Banten',
            pma: '1868179',
            pmaColor: 'rgb(93, 98, 181)',
            pmdn: '20708397',
            pmdnColor: 'rgb(41, 195, 190)',
            color: {
                pma: 'rgb(93, 98, 181)',
                pmdn: 'rgb(41, 195, 190)',
            }
        },
        {
            id: 'id-yo',
            name: 'Yogyakarta',
            pma: '14630',
            pmaColor: 'rgb(93, 98, 181)',
            pmdn: '6298839',
            pmdnColor: 'rgb(41, 195, 190)',
            color: {
                pma: 'rgb(93, 98, 181)',
                pmdn: 'rgb(41, 195, 190)',
            }
        },
    ])

    return (
        <Layout pageID="peta-realisasi-investasi" pageGroup="daerah" pageTitle="Peta Realisasi Investasi">
            <MapContextProvider>
                <section>
                    <ResponsiveContainer className="py-12">
                        <h1 className="text-center uppercase text-bkpm-light-blue-01 font-bold">Peta Profile</h1>
                        <RegionButtons region={ region } setRegion={ setRegion }/>
                    </ResponsiveContainer>
                </section>
                <Map />
                <ListPropinsi currentRegion={ region } />
            </MapContextProvider>
        </Layout>
    )
}

export default PetaRegional
