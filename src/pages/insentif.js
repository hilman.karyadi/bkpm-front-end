import React, { useState, useEffect} from 'react'
import { useQueryParam, StringParam  } from "use-query-params";
import { StaticImage } from 'gatsby-plugin-image'
import Fade from 'react-reveal/Fade'

import Layout from 'components/Layout/Layout'
import ResponsiveContainer from 'components/UI/ResponsiveContainer'
import Controller from 'components/Page/Insentif/Controller'
import TaxHoliday from 'components/Page/Insentif/Tabs/TaxHoliday'
import TaxAllowance from 'components/Page/Insentif/Tabs/TaxAllowance'
import MasterList from 'components/Page/Insentif/Tabs/MasterList'
import SuperDeduction from 'components/Page/Insentif/Tabs/SuperDeduction'
import Sticky from 'react-sticky-el'

const Insentif = ({ location }) => {

    let defaultTab = 'taxholiday'
    if ( location.state ) {
        defaultTab = location.state.defaultTab
    }

    const [tab, setTab] = useState(defaultTab);

    return (
        <Layout pageID="insentif" pageGroup="insentif" pageTitle="Insentif">
            <section className="py-12">
                <ResponsiveContainer className="sticky-container">
                    <div className="relative z-20">
                        <Sticky boundaryElement=".sticky-container" topOffset={-90} stickyStyle={{ transform: 'translateY(90px)' }} >
                            <Controller tab={ tab } setTab={ setTab } />
                        </Sticky>
                    </div>
                    <div className="relative z-10">
                        <Fade spy={ tab }>
                            <>
                                { tab === 'taxholiday' && <TaxHoliday /> }
                                { tab === 'taxallowance' && <TaxAllowance /> }
                                { tab === 'masterlist' && <MasterList /> }
                                { tab === 'superdeduction' && <SuperDeduction /> }
                            </>
                        </Fade>
                    </div>
                </ResponsiveContainer>
            </section>
        </Layout>
    )
}

export default Insentif
