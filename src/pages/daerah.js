import React, { useState } from 'react'

import Layout from 'components/Layout/Layout'
import 'components/Page/Daerah/style.css'

import DetailaRegional from 'components/Page/Daerah/DetailRegional'
import Umkm from 'components/Page/Daerah/UMKM'
import PeluangInvestasi from 'components/Page/Daerah/PeluangInvestasi'
import KawasanIndustri from'components/Page/Home/KawasanIndustri'
import PetaInteraktif from 'components/Page/Daerah/PetaInteraktif'
import Hero from 'components/Page/Daerah/Hero'
import RegionButtons from 'components/Page/PetaRealisasiInvestasi/RegionButtons'

const DaerahPage = () => {
    return (
        <Layout pageID="daerah" pageGroup="daerah">
            <Hero />
            <PetaInteraktif />
            <PeluangInvestasi />
            <KawasanIndustri />
            <Umkm />
            <DetailaRegional />
        </Layout>
    )
}

export default DaerahPage