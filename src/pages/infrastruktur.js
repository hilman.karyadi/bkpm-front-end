import React, { useState, useEffect } from "react"
import GoogleMapReact from "google-map-react"
import { Card, CardBody } from "@windmill/react-ui"

import Layout from "components/Layout/Layout"
import ResponsiveContainer from "components/UI/ResponsiveContainer"
import infrastruktur from "components/store/infrastruktur"
import SVGBandara from "components/images/ic-komoditi-bandara.svg"
import SVGPelabuhan from "components/images/ic-komoditi-pelabuhan.svg"
import SVGPendidikan from "components/images/ic-komoditi-pendidikan.svg"
import SVGHotel from "components/images/ic-komoditi-hotel.svg"
import SVGRumahSakit from "components/images/ic-komoditi-rumah-sakit.svg"

import TabelInfrastruktur from "components/Page/Infrastruktur/TabelInfrastruktur"
import axios from "axios"
import Accordion from "../components/Page/Infrastruktur/TabelInfrastruktur/Content/Accordion";
import Peta from "components/UI/Peta"

const Infrastruktur = () => {
  const [infrastrukturId, setInfrastrukturId] = useState(1)
  const [dataBandara, setDataBandara] = useState(null)
  const [dataPelabuhan, setDataPelabuhan] = useState(null)
  const content = details => <Accordion details={details} />

  const featureLayer = {
    layerBandaraId:"https://regionalinvestment.bkpm.go.id/gis/rest/services/bkpm_pir/bandara_id/MapServer/0",
    layerHotelId:"https://regionalinvestment.bkpm.go.id/gis/rest/services/bkpm_pir/hotel_id/MapServer/0",
    layerKawasanIndustriId:"https://regionalinvestment.bkpm.go.id/gis/rest/services/bkpm_pir/kawasan_industri_id/MapServer/0",
    layerPelabuhanId:"https://regionalinvestment.bkpm.go.id/gis/rest/services/bkpm_pir/pelabuhan_id/MapServer/0",
    layerPendidikanId:"https://regionalinvestment.bkpm.go.id/gis/rest/services/bkpm_pir/pendidikan_id/MapServer/0",
    layerRumahSakitId:"https://regionalinvestment.bkpm.go.id/gis/rest/services/bkpm_pir/rumah_sakit_id/MapServer/0",

    layerBandaraEn:"https://regionalinvestment.bkpm.go.id/gis/rest/services/bkpm_pir/bandara_en/MapServer/0",
    layerHotelEn:"https://regionalinvestment.bkpm.go.id/gis/rest/services/bkpm_pir/hotel_en/MapServer/0",
    layerKawasanIndustriEn:"https://regionalinvestment.bkpm.go.id/gis/rest/services/bkpm_pir/kawasan_industri_en/MapServer/0",
    layerPelabuhanEn:"https://regionalinvestment.bkpm.go.id/gis/rest/services/bkpm_pir/pelabuhan_en/MapServer/0",
    layerPendidikanEn:"https://regionalinvestment.bkpm.go.id/gis/rest/services/bkpm_pir/pendidikan_en/MapServer/0",
    layerRumahSakitEn:"https://regionalinvestment.bkpm.go.id/gis/rest/services/bkpm_pir/rumah_sakit_en/MapServer/0",
  }

  useEffect(async () => {
    const url_local = "http://localhost:8001/api/FrontEnd/GetInfrastrukturs"
    const url_server =
      "https://admin.regionalinvestment.bkpm.go.id:81/backend/public/index.php/api/FrontEnd/GetInfrastrukturs"
    const reqHome = axios.get(url_server)
    const result = await axios.all([reqHome])

    setDataBandara([
      ...result[0].data.Data.bandaras.map((item, index) => {
        return {
          id: item.idBandara,
          name: item.namaBandara,
          lng: item.longitude,
          ltd: item.latitude,
          details: [...item.details],
          content: content({
            name: item.namaBandara,
            details: [...item.details],
          }),
        }
      }),
    ])

    
  }, [])

  return (
    <Layout
      pageID="infrastruktur"
      pageGroup="infrastruktur"
      pageTitle="Infrastruktur"
    >
      <section className="py-12">
        <ResponsiveContainer>
          <hr className="h-0.5 bg-bkpm-dark-blue-01 w-12 rounded-full border-0 mb-2" />
          <h4 className="text-bkpm-dark-blue-01 mb-12">
            Infrastruktur Nasional
          </h4>
          <div className="grid gap-16 grid-cols-1 md:grid-cols-2 lg:grid-cols-5">
            <div className="lg:col-span-2">
              <TabelInfrastruktur
                infrastrukturId={infrastrukturId}
                setInfrastrukturId={setInfrastrukturId}
                dataBandara={dataBandara}
              />
            </div>
            <div className="lg:col-span-3">
              <Card className="shadow-xl h-screen">
                {infrastrukturId === 5 && (
                  <GoogleMapReact
                    bootstrapURLKeys={{
                      key: "AIzaSyD-U9NkKKhshvKgZRAM4MzlPk45HeLGIZ8",
                    }}
                    defaultCenter={{
                      lat: 0.7893,
                      lng: 113.9213,
                    }}
                    defaultZoom={5}
                  >
                    {infrastruktur.rumahsakits.map((item, index) => {
                      if (index < 300)
                        return (
                          <img
                            src={SVGRumahSakit}
                            className="w-4 h-auto"
                            lat={item.latitude}
                            lng={item.longitude}
                          />
                        )
                    })}
                  </GoogleMapReact>
                )}
                {infrastrukturId === 4 && (
                  <GoogleMapReact
                    bootstrapURLKeys={{
                      key: "AIzaSyD-U9NkKKhshvKgZRAM4MzlPk45HeLGIZ8",
                    }}
                    defaultCenter={{
                      lat: 0.7893,
                      lng: 113.9213,
                    }}
                    defaultZoom={5}
                  >
                    {infrastruktur.pendidikans.map((item, index) => {
                      if (index < 300)
                        return (
                          <img
                            src={SVGPendidikan}
                            className="w-4 h-auto"
                            lat={item.latitude}
                            lng={item.longitude}
                          />
                        )
                    })}
                  </GoogleMapReact>
                )}
                {infrastrukturId === 3 && (
                  <GoogleMapReact
                    bootstrapURLKeys={{
                      key: "AIzaSyD-U9NkKKhshvKgZRAM4MzlPk45HeLGIZ8",
                    }}
                    defaultCenter={{
                      lat: 0.7893,
                      lng: 113.9213,
                    }}
                    defaultZoom={5}
                  >
                    {infrastruktur.hotel.map((item, index) => {
                      if (index < 300)
                        return (
                          <img
                            src={SVGHotel}
                            className="w-4 h-auto"
                            lat={item.latitude}
                            lng={item.longitude}
                          />
                        )
                    })}
                  </GoogleMapReact>
                )}
                {infrastrukturId === 2 && (
                  <Peta dataLayers={[featureLayer.layerPelabuhanId]} />
                )}
                {infrastrukturId === 1 && (
                  <GoogleMapReact
                    bootstrapURLKeys={{
                      key: "AIzaSyD-U9NkKKhshvKgZRAM4MzlPk45HeLGIZ8",
                    }}
                    defaultCenter={{
                      lat: 0.7893,
                      lng: 113.9213,
                    }}
                    defaultZoom={5}
                  >
                    {infrastruktur.bandaras.map((item, index) => {
                      if (index < 300)
                        return (
                          <img
                            src={SVGBandara}
                            className="w-4 h-auto"
                            lat={item.latitude}
                            lng={item.longitude}
                          />
                        )
                    })}
                  </GoogleMapReact>
                )}
              </Card>
            </div>
          </div>
        </ResponsiveContainer>
      </section>
    </Layout>
  )
}

export default Infrastruktur
