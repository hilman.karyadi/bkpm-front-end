import Chart from 'components/Page/KawasanIndustri/Chart'
import React from 'react'
import GoogleMapReact from 'google-map-react'
import { StaticImage } from 'gatsby-plugin-image'
import { Player } from 'video-react'
import 'video-react/dist/video-react.css'
import { Swiper, SwiperSlide } from 'swiper/react'
import 'swiper/css'
import { FaMapMarkerAlt } from 'react-icons/fa'

import Layout from 'components/Layout/Layout'
import ResponsiveContainer, { ResponsiveContainerFluid } from 'components/UI/ResponsiveContainer'
import Info from 'components/Page/KawasanIndustri/Info'
import ContactDetails from 'components/Page/Peluang/ContactDetails'
import IcTourism from 'components/images/ic-tourism.inline.svg'

const Card = ({ children, className, ...allProps}) => {
    return (
        <div className={`rounded-xl p-8 shadow-lg bg-white overflow-hidden ${ className ? className : ''}`} {...allProps}>{ children }</div>
    )
}

const Title = ({ children, barClass, textClass }) => {
    return (
        <>
            <hr className={`h-0.5 w-12 rounded-full border-0 mb-2 ${ barClass ? barClass : 'bg-bkpm-dark-blue-01'}`} />
            <h4 className={`mb-4 ${ textClass ? textClass : 'text-bkpm-dark-blue-01'}`}>{ children }</h4>
        </>
    )
}

const Peluang = () => {
    return (
        <Layout pageID="detail-umkm" pageTitle="Detail UMKM" pageGroup="umkm">
            <div className="bg-gray-100">
                <section className="mb-6 h-96">
                    <GoogleMapReact
                        bootstrapURLKeys={{ key: 'AIzaSyD-U9NkKKhshvKgZRAM4MzlPk45HeLGIZ8' }}
                        defaultCenter={{
                            lat: 0.5,
                            lng: 110
                        }}
                        defaultZoom={12}
                    >
                        <div className="w-3 h-3 rounded-full bg-red-700"  lat={0.5} lng={110}></div>
                    </GoogleMapReact>
                </section>
                <section className="pb-12">
                    <ResponsiveContainerFluid className="grid grid-cols-1 gap-6 md:grid-cols-6">
                        <div className="md:col-span-2 md:order-2">
                            <div className="grid grid-cols-1 gap-6">
                                <Card className="bg-bkpm-dark-blue-01 text-white" >
                                    <Title barClass="bg-white" textClass="text-white">Informasi</Title>
                                    <Info />
                                </Card>
                                <Card>
                                    <Title >Lokasi / Kontak</Title>
                                    <div className="h-60 -mx-8 mb-4">
                                        <GoogleMapReact
                                            bootstrapURLKeys={{ key: 'AIzaSyD-U9NkKKhshvKgZRAM4MzlPk45HeLGIZ8' }}
                                            defaultCenter={{
                                                lat: 0.5,
                                                lng: 110
                                            }}
                                            defaultZoom={12}
                                        >
                                            <div className="w-3 h-3 rounded-full bg-red-700"  lat={0.5} lng={110}></div>
                                        </GoogleMapReact>
                                    </div>                            
                                    <ContactDetails />
                                </Card>
                            </div>
                        </div>
                        <div className="md:col-span-4 md:order-1">
                            <div className="grid grid-cols-1 gap-6">                                
                                <div>
                                    <Title >Jerawood Craft</Title>
                                </div>
                                <Card>
                                    <Title>Infografis</Title>
                                    <Swiper
                                        spaceBetween={20}
                                        slidesPerView={3.5}
                                        loop={true}
                                    >
                                        <SwiperSlide>
                                            <StaticImage src='../components/images/infografik-01.png' width="300" className="rounded-lg" />
                                        </SwiperSlide>
                                        <SwiperSlide>
                                            <StaticImage src='../components/images/infografik-02.png' width="300" className="rounded-lg" />
                                        </SwiperSlide>
                                        <SwiperSlide>
                                            <StaticImage src='../components/images/infografik-03.png' width="300" className="rounded-lg" />
                                        </SwiperSlide>
                                        <SwiperSlide>
                                            <StaticImage src='../components/images/infografik-04.png' width="300" className="rounded-lg" />
                                        </SwiperSlide>
                                        <SwiperSlide>
                                            <StaticImage src='../components/images/infografik-05.png' width="300" className="rounded-lg" />
                                        </SwiperSlide>
                                    </Swiper>
                                </Card>
                                <Card>
                                    <Title>Galeri</Title>
                                    <Swiper
                                        spaceBetween={20}
                                        slidesPerView={2.5}
                                        loop={true}
                                    >
                                        <SwiperSlide>
                                            <StaticImage src='../components/images/industri-01.jpg' width="700" aspectRatio="1.7" className="rounded-lg" />
                                        </SwiperSlide>
                                        <SwiperSlide>
                                            <StaticImage src='../components/images/industri-02.jpg' width="700" aspectRatio="1.7" className="rounded-lg" />
                                        </SwiperSlide>
                                        <SwiperSlide>
                                            <StaticImage src='../components/images/industri-03.jpg' width="700" aspectRatio="1.7" className="rounded-lg" />
                                        </SwiperSlide>
                                    </Swiper>
                                </Card>
                                <Card>
                                    <Title>Video</Title>
                                    <Player fluid={ true } autoPlay={ false } width="100%">
                                        <source src="/pir-video.mp4" />
                                    </Player>
                                </Card>
                            </div>
                        </div>
                    </ResponsiveContainerFluid>
                </section>
            </div>
        </Layout>
    )
}

export default Peluang