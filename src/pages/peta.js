import React, { useState, useEffect } from "react"
import { setDefaultOptions, loadModules } from "esri-loader";

import Layout from "components/Layout/Layout"
import Petaku from "components/UI/Peta"
import axios from "axios";

const Peta = () => {
  
  
  return (
    <Layout
      pageID="peta"
      pageGroup="peta"
      pageTitle="peta"
    >
      <Petaku />
     </Layout>
  )
}


export default Peta;