import React, { useState, useEffect } from "react"
import axios from "axios"

import Layout from "components/Layout/Layout"

import Slider from "components/Page/Home/Slider"
import Intro from "components/Page/Home/Intro"
import MapData from "components/Page/Home/MapData"
import Umkm from "components/Page/Home/UMKM"
import PeluangProyek from "components/Page/Home/PeluangProyek"
import Partner from "components/Page/Home/Partner"
import KawasanIndustri from "components/Page/Home/KawasanIndustri"
import "components/Page/Home/style.css"

const Home = () => {
  const [accordionState, setAccordionState] = useState(null)
  const [dataKawasanIndustri, setDataKawasanIndustri] = useState(null)
  const [dataRegional, setDataRegional] = useState(null)
  const [dataSlider, setDataSlider] = useState(null)
  const [dataPeluangProyek, setDataPeluangProyek] = useState(null)
  const [dataPeluangInvestasi, setDataPeluangInvestasi] = useState(null)
  const [dataUmkm, setDataUmkm] = useState(null)
  const [dataActivePeluangArea, setActivePeluangArea] = useState(null)

  useEffect(async () => {
    const url_local = "http://localhost:8001/api/FrontEnd/GetHome"
    const url_server =
      "https://admin.regionalinvestment.bkpm.go.id:81/backend/public/index.php/api/FrontEnd/GetHome"
    const reqHome = axios.get(url_server)
    const result = await axios.all([reqHome])

    setAccordionState([
      ...result[0].data.Data.insentifs.map((item, index) => {
        return {
          title: item.title,
          text: item.deskripsi,
          tab: item.title.toLowerCase().replace(" ", ""),
          isOpen: index === 0 ? true : false,
        }
      }),
    ])

    setActivePeluangArea(result[0].data.Data.peluangInvestasi[0].title)
    setDataKawasanIndustri({ ...result[0].data.Data.kawasan })
    setDataRegional([...result[0].data.Data.regional.mapData])
    setDataSlider([...result[0].data.Data.sliders])
    setDataPeluangProyek([...result[0].data.Data.peluangProyek])
    setDataPeluangInvestasi([...result[0].data.Data.peluangInvestasi])
    setDataUmkm([...result[0].data.Data.dataUmkm])
  }, [])

  return (
    <Layout pageID="home">
      {/* <Slider /> */}
      {dataSlider && <Slider dataSlider={dataSlider} />}
      {accordionState && (
        <Intro
          accordionState={accordionState}
          setAccordionState={setAccordionState}
        />
      )}
      {dataRegional && <MapData dataRegional={dataRegional} />}
      {dataPeluangProyek && (
        <PeluangProyek
          dataPeluangProyek={dataPeluangProyek}
          dataPeluangInvestasi={dataPeluangInvestasi}
          dataActivePeluangArea={dataActivePeluangArea}
        />
      )}
      {dataKawasanIndustri && (
        <KawasanIndustri
          dataKawasanIndustri={dataKawasanIndustri}
          dataPeluangInvestasi={dataPeluangInvestasi}
        />
      )}
      {dataUmkm && (
        <Umkm dataUmkm={dataUmkm} />)
      }
      <Partner />
    </Layout>
  )
}

export default Home
