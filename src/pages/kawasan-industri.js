import Chart from 'components/Page/KawasanIndustri/Chart'
import React from 'react'
import GoogleMapReact from 'google-map-react'
import { StaticImage } from 'gatsby-plugin-image'
import { Player } from 'video-react'
import 'video-react/dist/video-react.css'
import { Swiper, SwiperSlide } from 'swiper/react'
import 'swiper/css'
import { FaMapMarkerAlt } from 'react-icons/fa'

import Layout from 'components/Layout/Layout'
import ResponsiveContainer, { ResponsiveContainerFluid } from 'components/UI/ResponsiveContainer'
import Info from 'components/Page/KawasanIndustri/Info'
import ContactDetails from 'components/Page/Peluang/ContactDetails'
import IcTourism from 'components/images/ic-tourism.inline.svg'

const Card = ({ children, className, ...allProps}) => {
    return (
        <div className={`rounded-xl p-8 shadow-lg bg-white overflow-hidden ${ className ? className : ''}`} {...allProps}>{ children }</div>
    )
}

const Title = ({ children, barClass, textClass }) => {
    return (
        <>
            <hr className={`h-0.5 w-12 rounded-full border-0 mb-2 ${ barClass ? barClass : 'bg-bkpm-dark-blue-01'}`} />
            <h4 className={`mb-4 ${ textClass ? textClass : 'text-bkpm-dark-blue-01'}`}>{ children }</h4>
        </>
    )
}

const Peluang = () => {
    return (
        <Layout pageID="kawasan-industri" pageTitle="Kawasan Industri" pageGroup="infrastruktur">
            <div className="bg-gray-100">
                <section className="mb-6">
                    <StaticImage src="../components/images/map-kawasan.png" className="w-full" quality="100"/>
                </section>
                <section className="pb-6">
                    <ResponsiveContainer>
                        <Card>
                            <div className="h-80 w-auto">
                                <Chart />
                            </div>
                        </Card>
                    </ResponsiveContainer>
                </section>
                <section>
                    <ResponsiveContainerFluid className="grid grid-cols-1 gap-6 md:grid-cols-6">
                        <div className="md:col-span-2 md:order-2">
                            <div className="grid grid-cols-1 gap-6">
                                <Card className="bg-bkpm-dark-blue-01 text-white" >
                                    <Title barClass="bg-white" textClass="text-white">Informasi</Title>
                                    <Info />
                                </Card>
                                <Card>
                                    <Title >Lokasi / Kontak</Title>
                                    <div className="h-60 -mx-8 mb-4">
                                        <GoogleMapReact
                                            bootstrapURLKeys={{ key: 'AIzaSyD-U9NkKKhshvKgZRAM4MzlPk45HeLGIZ8' }}
                                            defaultCenter={{
                                                lat: 0.5,
                                                lng: 110
                                            }}
                                            defaultZoom={12}
                                        >
                                            <div className="w-3 h-3 rounded-full bg-red-700"  lat={0.5} lng={110}></div>
                                        </GoogleMapReact>
                                    </div>                            
                                    <ContactDetails />
                                </Card>
                            </div>
                        </div>
                        <div className="md:col-span-4 md:order-1">
                            <div className="grid grid-cols-1 gap-6">
                                <Card>
                                    <Title>Karawang New Industri City</Title>
                                    <p><FaMapMarkerAlt className="inline-block text-bkpm-light-blue-01" />&nbsp;&nbsp;Jawa Barat</p>
                                    <hr className="h-0 border-b border-gray-200 border-solid mt-8 mb-4" />
                                    <h5 className="text-bkpm-dark-blue-01">Deskripsi</h5>
                                    <div className="flex justify-between items-center mb-8">
                                        <div className="flex items-center">
                                            <IcTourism className="mr-2" />
                                            <span className="text-sm">Kawasan Industri</span>
                                        </div>
                                        <div className="border border-gray-500 rounded-full px-2 py-1 text-xs">Viewed - 105</div>
                                    </div>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Elit id vitae ac arcu amet, urna, fusce suspendisse pretium. Bibendum habitant amet scelerisque orci elit id fringilla pellentesque. Amet, volutpat interdum luctus est accumsan dignissim donec ultrices. Cras nec orci cras eleifend eu, velit. Etiam feugiat ac justo, eget euismod lectus erat id. Justo, aliquet amet iaculis enim etiam eget sit. Eget euismod semper vel blandit volutpat proin. Enim nisl hac in quis sed nisl pharetra venenatis ac. Vitae in elit aliquam vestibulum, mattis. Sagittis, mauris dolor fermentum elementum phasellus quam. Venenatis ornare amet congue tortor tincidunt vulputate sed. At massa odio tristique tortor. Fermentum neque, diam urna suscipit maecenas at suspendisse.</p>
                                    <p>Vel aliquam nulla posuere risus ac integer gravida gravida. Mattis pellentesque fames turpis sed et amet lacinia. Sed nisl augue vestibulum praesent gravida convallis. Adipiscing feugiat phasellus egestas eget suscipit. Pellentesque nibh tincidunt varius vitae sit adipiscing enim praesent tellus.</p>
                                    <p>Lectus lectus pretium molestie bibendum gravida nullam adipiscing orci. Ut enim gravida felis dictum ut commodo pretium. Diam, tellus lacus, id purus. Fringilla auctor et pellentesque dictum sem in nascetur. Vivamus elit aliquam pharetra enim proin euismod. Leo lectus dictum amet nisl, ornare aliquam. Egestas vehicula proin a sed id non blandit ullamcorper. Egestas morbi urna augue facilisi lorem diam odio consequat faucibus. Suspendisse senectus pharetra sagittis ullamcorper tristique gravida natoque euismod.</p>
                                </Card>
                                <Card>
                                    <Title>Infografis</Title>
                                    <Swiper
                                        spaceBetween={20}
                                        slidesPerView={3.5}
                                        loop={true}
                                    >
                                        <SwiperSlide>
                                            <StaticImage src='../components/images/infografik-01.png' width="300" className="rounded-lg" />
                                        </SwiperSlide>
                                        <SwiperSlide>
                                            <StaticImage src='../components/images/infografik-02.png' width="300" className="rounded-lg" />
                                        </SwiperSlide>
                                        <SwiperSlide>
                                            <StaticImage src='../components/images/infografik-03.png' width="300" className="rounded-lg" />
                                        </SwiperSlide>
                                        <SwiperSlide>
                                            <StaticImage src='../components/images/infografik-04.png' width="300" className="rounded-lg" />
                                        </SwiperSlide>
                                        <SwiperSlide>
                                            <StaticImage src='../components/images/infografik-05.png' width="300" className="rounded-lg" />
                                        </SwiperSlide>
                                    </Swiper>
                                </Card>
                                <Card>
                                    <Title>Galeri</Title>
                                    <Swiper
                                        spaceBetween={20}
                                        slidesPerView={2.5}
                                        loop={true}
                                    >
                                        <SwiperSlide>
                                            <StaticImage src='../components/images/industri-01.jpg' width="700" aspectRatio="1.7" className="rounded-lg" />
                                        </SwiperSlide>
                                        <SwiperSlide>
                                            <StaticImage src='../components/images/industri-02.jpg' width="700" aspectRatio="1.7" className="rounded-lg" />
                                        </SwiperSlide>
                                        <SwiperSlide>
                                            <StaticImage src='../components/images/industri-03.jpg' width="700" aspectRatio="1.7" className="rounded-lg" />
                                        </SwiperSlide>
                                    </Swiper>
                                </Card>
                                <Card>
                                    <Title>Video</Title>
                                    <Player fluid={ true } autoPlay={ false } width="100%">
                                        <source src="/pir-video.mp4" />
                                    </Player>
                                </Card>
                            </div>
                        </div>
                    </ResponsiveContainerFluid>
                </section>
            </div>
        </Layout>
    )
}

export default Peluang
