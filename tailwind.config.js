const windmill = require('@windmill/react-ui/config')

module.exports = windmill({
  purge: ['./src/**/*.{js,jsx,ts,tsx}','node_modules/windmill-react-ui/lib/default.js'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        bkpm: {
          'dark-blue-01':  ({ opacityVariable, opacityValue }) => {
            if (opacityValue !== undefined) {
              return `rgba(var(--dark-blue-01), ${opacityValue})`
            }
            if (opacityVariable !== undefined) {
              return `rgba(var(--dark-blue-01), var(${opacityVariable}, 1))`
            }
            return `rgb(var(--dark-blue-01))`
          },
          'dark-blue-02': ({ opacityVariable, opacityValue }) => {
            if (opacityValue !== undefined) {
              return `rgba(var(--dark-blue-02), ${opacityValue})`
            }
            if (opacityVariable !== undefined) {
              return `rgba(var(--dark-blue-02), var(${opacityVariable}, 1))`
            }
            return `rgb(var(--dark-blue-02))`
          },
          'light-blue-01': ({ opacityVariable, opacityValue }) => {
            if (opacityValue !== undefined) {
              return `rgba(var(--light-blue-01), ${opacityValue})`
            }
            if (opacityVariable !== undefined) {
              return `rgba(var(--light-blue-01), var(${opacityVariable}, 1))`
            }
            return `rgb(var(--light-blue-01))`
          },
          'light-blue-02': ({ opacityVariable, opacityValue }) => {
            if (opacityValue !== undefined) {
              return `rgba(var(--light-blue-02), ${opacityValue})`
            }
            if (opacityVariable !== undefined) {
              return `rgba(var(--light-blue-02), var(${opacityVariable}, 1))`
            }
            return `rgb(var(--light-blue-02))`
          },
          'red': ({ opacityVariable, opacityValue }) => {
            if (opacityValue !== undefined) {
              return `rgba(var(--red), ${opacityValue})`
            }
            if (opacityVariable !== undefined) {
              return `rgba(var(--red), var(${opacityVariable}, 1))`
            }
            return `rgb(var(--red))`
          },
          'green': ({ opacityVariable, opacityValue }) => {
            if (opacityValue !== undefined) {
              return `rgba(var(--green), ${opacityValue})`
            }
            if (opacityVariable !== undefined) {
              return `rgba(var(--green), var(${opacityVariable}, 1))`
            }
            return `rgb(var(--green))`
          },
          'orange': ({ opacityVariable, opacityValue }) => {
            if (opacityValue !== undefined) {
              return `rgba(var(--orange), ${opacityValue})`
            }
            if (opacityVariable !== undefined) {
              return `rgba(var(--orange), var(${opacityVariable}, 1))`
            }
            return `rgb(var(--orange))`
          },
          'orange-02': ({ opacityVariable, opacityValue }) => {
            if (opacityValue !== undefined) {
              return `rgba(var(--orange-02), ${opacityValue})`
            }
            if (opacityVariable !== undefined) {
              return `rgba(var(--orange-02), var(${opacityVariable}, 1))`
            }
            return `rgb(var(--orange-02))`
          },
        },
        bkpmv2: {
          'light-blue': ({ opacityVariable, opacityValue }) => {
            if (opacityValue !== undefined) {
              return `rgba(var(--v2-light-blue), ${opacityValue})`
            }
            if (opacityVariable !== undefined) {
              return `rgba(var(--v2-light-blue), var(${opacityVariable}, 1))`
            }
            return `rgb(var(--v2-light-blue))`
          },
          'lighter-blue': ({ opacityVariable, opacityValue }) => {
            if (opacityValue !== undefined) {
              return `rgba(var(--v2-lighter-blue), ${opacityValue})`
            }
            if (opacityVariable !== undefined) {
              return `rgba(var(--v2-lighter-blue), var(${opacityVariable}, 1))`
            }
            return `rgb(var(--v2-lighter-blue))`
          },
          'dark-blue': ({ opacityVariable, opacityValue }) => {
            if (opacityValue !== undefined) {
              return `rgba(var(--v2-dark-blue), ${opacityValue})`
            }
            if (opacityVariable !== undefined) {
              return `rgba(var(--v2-dark-blue), var(${opacityVariable}, 1))`
            }
            return `rgb(var(--v2-dark-blue))`
          },
          'darker-blue': ({ opacityVariable, opacityValue }) => {
            if (opacityValue !== undefined) {
              return `rgba(var(--v2-darker-blue), ${opacityValue})`
            }
            if (opacityVariable !== undefined) {
              return `rgba(var(--v2-darker-blue), var(${opacityVariable}, 1))`
            }
            return `rgb(var(--v2-darker-blue))`
          },
          'green': ({ opacityVariable, opacityValue }) => {
            if (opacityValue !== undefined) {
              return `rgba(var(--v2-green), ${opacityValue})`
            }
            if (opacityVariable !== undefined) {
              return `rgba(var(--v2-green), var(${opacityVariable}, 1))`
            }
            return `rgb(var(--v2-green))`
          }
        }
      },
      fontSize: {
        '2xs': ['0.6rem', {
          lineHeight: '1.3em',
        }],
      },
    },
  },
  variants: {
    extend: {
      filter: ['hover'],
      scale: ['group-hover']
    },
    aspectRatio: ['responsive']
  },
  plugins: [
    require('@tailwindcss/aspect-ratio')
  ],
})
